// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "Registration/RegistrationPipeline_config.hpp"
#include "haniUtilities.hpp"

#include <iostream>

#include <opencv2/core/core.hpp>        // OpenCV serialization
#include <boost/filesystem.hpp>         // checking files and folders

namespace hjh {
namespace registration {

// *****************************************************************************
// *********************     D E F I N I T I O N S   ***************************
// *****************************************************************************


//==============================================================================
//==============================================================================

DownsamplingParameters::DownsamplingParameters()
    : percentage( 0.1 )
    , octree_level( 8 )
    , voxel_size( 0.01 ) // 1 cm
{
    // empty
}

//==============================================================================
//==============================================================================

DownsamplingParameters::DownsamplingParameters( double percentage_
                                                , int octree_level_
                                                , double voxel_size_ )
    : percentage( percentage_ )
    , octree_level( octree_level_ )
    , voxel_size( voxel_size_ )
{
    // empty
}

//==============================================================================
//==============================================================================

DownsamplingParameters::~DownsamplingParameters()
{
    // empty
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<( std::ostream& os, const DownsamplingParameters& obj )
{
    os << "   percentage : " << obj.percentage
       << " octree_level : " << obj.octree_level
       << "   voxel_size : " << obj.voxel_size
          ;
    return os;
}

//==============================================================================
//==============================================================================

void
DownsamplingParameters::write( cv::FileStorage& fs ) const
{
    fs << "{"
       << "percentage" << percentage
       << "octree_level" << octree_level
       << "voxel_size" << voxel_size
       << "}";
}

//==============================================================================
//==============================================================================

void
DownsamplingParameters::read( const cv::FileNode& node )
{
    node["percentage"] >> percentage;
    node["octree_level"] >> octree_level;
    node["voxel_size"] >> voxel_size;
}

//==============================================================================
//==============================================================================

void
DownsamplingParameters::set_percentage( double percentage_ )
{
    percentage = percentage_;
}

//==============================================================================
//==============================================================================

double
DownsamplingParameters::get_percentage() const
{
    return percentage;
}

//==============================================================================
//==============================================================================

void
DownsamplingParameters::set_octree_level( int octree_level_ )
{
    octree_level = octree_level_;
}

//==============================================================================
//==============================================================================

int
DownsamplingParameters::get_octree_level() const
{
   return octree_level;
}

//==============================================================================
//==============================================================================

void
DownsamplingParameters::set_voxel_size( double voxel_size_ )
{
    voxel_size = voxel_size_;
}

//==============================================================================
//==============================================================================

double
DownsamplingParameters::get_voxel_size() const
{
    return voxel_size;
}


//==============================================================================
//==============================================================================

NormalComputingParameters::NormalComputingParameters()
    : search_radius( 0.03 )
{
    // empty
}

//==============================================================================
//==============================================================================

NormalComputingParameters::NormalComputingParameters( double search_radious )
    : search_radius( search_radious )
{
    // empty
}

//==============================================================================
//==============================================================================

NormalComputingParameters::~NormalComputingParameters()
{
    // empty
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<( std::ostream& os, const NormalComputingParameters& obj )
{
    os << "search_radius : " << obj.search_radius
          ;
    return os;
}

//==============================================================================
//==============================================================================

void
NormalComputingParameters::write(cv::FileStorage& fs) const
{
    fs << "{"
       << "search_radius" << search_radius
       << "}";
}

//==============================================================================
//==============================================================================

void
NormalComputingParameters::read( const cv::FileNode& node )
{
    node["search_radius"] >> search_radius;
}

//==============================================================================
//==============================================================================

void
NormalComputingParameters::set_search_radius( double radius )
{
    search_radius = radius;
}

//==============================================================================
//==============================================================================

double
NormalComputingParameters::get_search_radius() const
{
    return search_radius;
}


//==============================================================================
//==============================================================================

ICP_config::ICP_config()
    : euclidean_fitness_epsilon( -std::numeric_limits<double>::max() )
    , corr_dist_threshold( std::sqrt(std::numeric_limits<double>::max()) )
    , max_iterations( 10 )
    , ransac_iterations( 0 )
    , inlier_threshold( 0.05 )
    , transformation_epsilon( 0.0 )
{
    // empty
}


//==============================================================================
//==============================================================================

ICP_config::~ICP_config()
{
    // empty
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<( std::ostream& os, const ICP_config& obj )
{
    os
       << "euclidean_fitness_epsilon : " << obj.euclidean_fitness_epsilon
       << "\n      corr_dist_threshold : " << obj.corr_dist_threshold
       << "\n           max_iterations : " << obj.max_iterations
       << "\n        ransac_iterations : " << obj.ransac_iterations
       << "\n         inlier_threshold : " << obj.inlier_threshold
       << "\n   transformation_epsilon : " << obj.transformation_epsilon
       ;
    return os;
}

//==============================================================================
//==============================================================================

void
ICP_config::write(cv::FileStorage& fs) const
{
    fs << "{"
       << "euclidean_fitness_epsilon" << euclidean_fitness_epsilon
       << "corr_dist_threshold" << corr_dist_threshold
       << "max_iterations" << max_iterations
       << "ransac_iterations" << ransac_iterations
       << "inlier_threshold" << inlier_threshold
       << "transformation_epsilon" << transformation_epsilon
       << "}";
}

//==============================================================================
//==============================================================================

void
ICP_config::read( const cv::FileNode& node )
{
    node["euclidean_fitness_epsilon"] >> euclidean_fitness_epsilon;
    node["corr_dist_threshold"] >> corr_dist_threshold;
    node["max_iterations"] >> max_iterations;
    node["ransac_iterations"] >> ransac_iterations;
    node["inlier_threshold"] >> inlier_threshold;
    node["transformation_epsilon"] >> transformation_epsilon;
}

//==============================================================================
//==============================================================================

RegistrationPipelineConfig::RegistrationPipelineConfig()
    : depth_image_scale( 1.0 )
    , target_intrinsics_color()
    , target_intrinsics_depth()
    , source_intrinsics_color()
    , source_intrinsics_depth()
    , feature_detector_2D_type( "BRISK" )
    , feature_descriptor_2D_type( "BRIEF" )
    , descriptor_matcher_2D_type( "BruteForce" )
    , knn_number_of_matches( 1 )
    , matching_filter_2D_type( "CrossCheck" )
    , inliers_max_threshold( 3 )
    , inliers_finding_method( "source_to_target" )
    , flag_full_affine( true )
    , ransac_reprojection_threshold_2D( 3. )
    , homography_2D_method( "RANSAC")
    , ransac_max_distance_threshold_2D( 3. )
    , confidence_probability( 0.99 )
    , fundamental_matrix_2D_method( "RANSAC" )
    , correspondence_base( "homography" )
    , registration_distance_threshold( 1.0f )
    , correspondences_rejection_distant_threshold( 1.0 )
    , correspondences_rejection_median_factor( 2.0 )
    , correspondences_rejection_similarity_threshold( 0.75f )
    , correspondences_rejection_polygon_cardinality( 3 )
    , correspondences_rejection_poly_iterartion( 10000 )
    , correspondences_rejection_SAC_inlier_threshold( 0.05 )
    , correspondences_rejection_SAC_max_iterations( 1000 )
    , correspondences_rejection_SAC_flag_refine( false )
    , correspondences_rejection_overlap_ratio( 0.5f )
    , correspondences_rejection_min_correspondences( 0 )
    , correspondences_rejection_min_ratio( 0.05 )
    , correspondences_rejection_max_ratio( 0.95 )
    , transformation_estimation_3D_method( "SVD" )
    , transformation_estimation_3D_correspondences_to_use( "no_rejection" )
    , icp_type( "ICP" )
    , flag_loop_closure( false )
    , loop_closure_type( "SimpleBackToBack" )
{
    // empty
}

//==============================================================================
//==============================================================================

RegistrationPipelineConfig::~RegistrationPipelineConfig()
{
    // empty
}

//==============================================================================
//==============================================================================

int
RegistrationPipelineConfig::write_config_file( std::string filename ) const
{
    // check the validity of filename
    if (!boost::filesystem::portable_name(filename))
    {
        std::stringstream error_msg;
        error_msg << "invalid filename \"" << filename
                  << "\" (default is being used).";
#ifdef _USE_ULOGGER_
        if (show_log) UWARN(error_msg.str().c_str());
#endif
        // BADAN ToDo do somthing with this error message

        filename = default_filename_registration_pipeline_config;
    }


    // create an opencv serialization module
    cv::FileStorage fs( filename, cv::FileStorage::WRITE );

    // time of creation
    time_t rawtime; time(&rawtime);

    // writing fields...
    fs << "Built_Date" << now_string();
    fs << "show_log" << show_log;

    fs << "target_intrinsics_color" << target_intrinsics_color;
    fs << "target_intrinsics_depth" << target_intrinsics_depth;
    fs << "source_intrinsics_color" << source_intrinsics_color;
    fs << "source_intrinsics_depth" << source_intrinsics_depth;

    fs << "depth_image_scale" << depth_image_scale;

    fs << "feature_detector_2D_type" << feature_detector_2D_type;
    fs << "feature_descriptor_2D_type" << feature_descriptor_2D_type;
    fs << "descriptor_matcher_2D_type" << descriptor_matcher_2D_type;
    fs << "knn_number_of_matches" << knn_number_of_matches;
    fs << "matching_filter_2D_type" << matching_filter_2D_type;
    fs << "inliers_max_threshold" << inliers_max_threshold;
    fs << "inliers_finding_method" << inliers_finding_method;
    fs << "flag_full_affine" << flag_full_affine;
    fs << "ransac_reprojection_threshold_2D" << ransac_reprojection_threshold_2D;
    fs << "homography_2D_method" << homography_2D_method;
    fs << "ransac_max_distance_threshold_2D" << ransac_max_distance_threshold_2D;
    fs << "confidence_probability" << confidence_probability;
    fs << "fundamental_matrix_2D_method" << fundamental_matrix_2D_method;
    fs << "correspondence_base" << correspondence_base;
    fs << "registration_distance_threshold" << registration_distance_threshold;
    fs << "correspondences_rejection_distant_threshold" << correspondences_rejection_distant_threshold;
    fs << "correspondences_rejection_median_factor" << correspondences_rejection_median_factor;
    fs << "correspondences_rejection_similarity_threshold" << correspondences_rejection_similarity_threshold;
    fs << "correspondences_rejection_polygon_cardinality" << correspondences_rejection_polygon_cardinality;
    fs << "correspondences_rejection_poly_iterartion" << correspondences_rejection_poly_iterartion;
    fs << "correspondences_rejection_SAC_inlier_threshold" << correspondences_rejection_SAC_inlier_threshold;
    fs << "correspondences_rejection_SAC_max_iterations" << correspondences_rejection_SAC_max_iterations;
    fs << "correspondences_rejection_SAC_flag_refine" << correspondences_rejection_SAC_flag_refine;
    fs << "correspondences_rejection_overlap_ratio" << correspondences_rejection_overlap_ratio;
    fs << "correspondences_rejection_min_correspondences" << (int)correspondences_rejection_min_correspondences;
    fs << "correspondences_rejection_min_ratio" << correspondences_rejection_min_ratio;
    fs << "correspondences_rejection_max_ratio" << correspondences_rejection_max_ratio;
    fs << "transformation_estimation_3D_method" << transformation_estimation_3D_method;
    fs << "transformation_estimation_3D_correspondences_to_use" << transformation_estimation_3D_correspondences_to_use;
    fs << "icp_type" << icp_type;
    fs << "icp_settings" << icp_settings;
    fs << "flag_loop_closure" << flag_loop_closure;
    fs << "loop_closure_type" << loop_closure_type;


    // closing the serialization module
    fs.release();

    std::stringstream log;
    log << "configuration file \"" << filename << "\" created.";
#ifdef _USE_ULOGGER_
    if (show_log) UDEBUG(log.str().c_str());
#endif

    // BADAN ToDo do somthing with this message

    return 0;
}

//==============================================================================
//==============================================================================

int
RegistrationPipelineConfig::read_config_file( std::string filename )
{
    // check the validity/existance of filename
    if (!boost::filesystem::exists(filename))
    {
        std::stringstream error_msg;
        error_msg << "file \"" << filename << "\" does NOT exist.";
#ifdef _USE_ULOGGER_
        if (show_log) UERROR(error_msg.str().c_str());
#endif
        // BADAN ToDo do somthing with this error message

        return -1;
    }
    std::stringstream log;
    log << "configuration file \"" << filename << "\" read.";
#ifdef _USE_ULOGGER_
    if (show_log) UDEBUG(log.str().c_str());
#endif

    // create an opencv serialization module
    cv::FileStorage fs( filename, cv::FileStorage::READ );

    // reading fields...
    int itemp;

    fs ["show_log"] >> itemp; set_log( itemp );

    fs ["target_intrinsics_color"] >> target_intrinsics_color;
    fs ["target_intrinsics_depth"] >> target_intrinsics_depth;
    fs ["source_intrinsics_color"] >> source_intrinsics_color;
    fs ["source_intrinsics_depth"] >> source_intrinsics_depth;

    fs ["depth_image_scale"] >> depth_image_scale;
    if (0==depth_image_scale) depth_image_scale=1.0;

    fs ["feature_detector_2D_type"] >> feature_detector_2D_type;
    fs ["feature_descriptor_2D_type"] >> feature_descriptor_2D_type;
    fs ["descriptor_matcher_2D_type"] >> descriptor_matcher_2D_type;
    fs ["knn_number_of_matches"] >> knn_number_of_matches;
    fs ["matching_filter_2D_type"] >> matching_filter_2D_type;
    fs ["inliers_max_threshold"] >> inliers_max_threshold;
    fs ["inliers_finding_method"] >> inliers_finding_method;
    fs ["flag_full_affine"] >> flag_full_affine;
    fs ["ransac_reprojection_threshold_2D"] >> ransac_reprojection_threshold_2D;
    fs ["homography_2D_method"] >> homography_2D_method;
    fs ["ransac_max_distance_threshold_2D"] >> ransac_max_distance_threshold_2D;
    fs ["confidence_probability"] >> confidence_probability;
    fs ["fundamental_matrix_2D_method"] >> fundamental_matrix_2D_method;
    fs ["correspondence_base"] >> correspondence_base;
    fs ["registration_distance_threshold"] >> registration_distance_threshold;
    fs ["correspondences_rejection_distant_threshold"] >> correspondences_rejection_distant_threshold;
    fs ["correspondences_rejection_median_factor"] >> correspondences_rejection_median_factor;
    fs ["correspondences_rejection_similarity_threshold"] >>  correspondences_rejection_similarity_threshold;
    fs ["correspondences_rejection_polygon_cardinality"] >> correspondences_rejection_polygon_cardinality;
    fs ["correspondences_rejection_poly_iterartion"] >> correspondences_rejection_poly_iterartion;
    fs ["correspondences_rejection_SAC_inlier_threshold"] >> correspondences_rejection_SAC_inlier_threshold;
    fs ["correspondences_rejection_SAC_max_iterations"] >> correspondences_rejection_SAC_max_iterations;
    fs ["correspondences_rejection_SAC_flag_refine"] >> correspondences_rejection_SAC_flag_refine;
    fs ["correspondences_rejection_overlap_ratio"] >> correspondences_rejection_overlap_ratio;
    fs ["correspondences_rejection_min_correspondences"] >> itemp; set_correspondences_rejection_min_correspondences( (int)itemp );
    fs ["correspondences_rejection_min_ratio"] >> correspondences_rejection_min_ratio;
    fs ["correspondences_rejection_max_ratio"] >> correspondences_rejection_max_ratio;
    fs ["transformation_estimation_3D_method"] >> transformation_estimation_3D_method;
    fs ["transformation_estimation_3D_correspondences_to_use"] >> transformation_estimation_3D_correspondences_to_use;
    fs ["icp_type"] >> icp_type;
    fs ["icp_settings"] >> icp_settings;
    fs ["flag_loop_closure"] >> flag_loop_closure;
    fs ["loop_closure_type"] >> loop_closure_type;



    // closing the serialization module
    fs.release();

    // after reading from file, it should be ready by now!
    ready = true;

    return 0;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::display() const
{
#ifndef _USE_ULOGGER_
    std::cout << "RegistrationPipelineConfig:" << std::endl
              << "                                 show log flag : " << (show_log ? "YES" : "NO") << std::endl
              << "                       target_intrinsics_color : " << target_intrinsics_color << std::endl
              << "                       target_intrinsics_depth : " << target_intrinsics_depth << std::endl
              << "                       source_intrinsics_color : " << source_intrinsics_color << std::endl
              << "                       source_intrinsics_depth : " << source_intrinsics_depth << std::endl
              << "                             depth_image_scale : " << depth_image_scale << std::endl
              << "                      feature_detector_2D_type : " << feature_detector_2D_type << std::endl
              << "                    feature_descriptor_2D_type : " << feature_descriptor_2D_type << std::endl
              << "                    descriptor_matcher_2D_type : " << descriptor_matcher_2D_type << std::endl
              << "                         knn_number_of_matches : " << knn_number_of_matches << std::endl
              << "                       matching_filter_2D_type : " << matching_filter_2D_type << std::endl
              << "                         inliers_max_threshold : " << inliers_max_threshold << std::endl
              << "                        inliers_finding_method : " << inliers_finding_method << std::endl
              << "                              flag_full_affine : " << (flag_full_affine ? "YES" : "NO") << std::endl
              << "              ransac_reprojection_threshold_2D : " << ransac_reprojection_threshold_2D << std::endl
              << "                          homography_2D_method : " << homography_2D_method << std::endl
              << "              ransac_max_distance_threshold_2D : " << ransac_max_distance_threshold_2D << std::endl
              << "                        confidence_probability : " << confidence_probability << std::endl
              << "                  fundamental_matrix_2D_method : " << fundamental_matrix_2D_method << std::endl
              << "                           correspondence_base : " << correspondence_base << std::endl
              << "               registration_distance_threshold : " << registration_distance_threshold << std::endl
              << "   correspondences_rejection_distant_threshold : " << correspondences_rejection_distant_threshold << std::endl
              << "       correspondences_rejection_median_factor : " << correspondences_rejection_median_factor << std::endl
              << "correspondences_rejection_similarity_threshold : " << correspondences_rejection_similarity_threshold << std::endl
              << " correspondences_rejection_polygon_cardinality : " << correspondences_rejection_polygon_cardinality << std::endl
              << "     correspondences_rejection_poly_iterartion : " << correspondences_rejection_poly_iterartion << std::endl
              << "correspondences_rejection_SAC_inlier_threshold : " << correspondences_rejection_SAC_inlier_threshold << std::endl
              << "  correspondences_rejection_SAC_max_iterations : " << correspondences_rejection_SAC_max_iterations << std::endl
              << "     correspondences_rejection_SAC_flag_refine : " << (correspondences_rejection_SAC_flag_refine ? "YES" : "NO") << std::endl
              << "       correspondences_rejection_overlap_ratio : " << correspondences_rejection_overlap_ratio << std::endl
              << " correspondences_rejection_min_correspondences : " << correspondences_rejection_min_correspondences << std::endl
              << "           correspondences_rejection_min_ratio : " << correspondences_rejection_min_ratio << std::endl
              << "           correspondences_rejection_max_ratio : " << correspondences_rejection_max_ratio << std::endl
              << "           transformation_estimation_3D_method : " << transformation_estimation_3D_method << std::endl
              << "  transformation_estimation_3D_correspondences : " << transformation_estimation_3D_correspondences_to_use << std::endl
              << "                                      icp_type : " << icp_type << std::endl
              << "--- ICP settings-----------------------------\n" << icp_settings << std::endl
              << "---------------------------------------------" << std::endl
              << "                          flag_loop_closure : " << (flag_loop_closure ? "YES" : "NO") << std::endl
              << "                          loop_closure_type : " << loop_closure_type << std::endl
              ;
#else
    std::stringstream log;
    log << "LoggerConfig:" << std::endl
        << "                                 show log flag : " << (show_log ? "YES" : "NO") << std::endl
        << "                              intrinsics_color : " << intrinsics_color << std::endl
        << "                              intrinsics_depth : " << intrinsics_depth << std::endl
        << "                             depth_image_scale : " << depth_image_scale << std::endl
        << "                      feature_detector_2D_type : " << feature_detector_2D_type << std::endl
        << "                    feature_descriptor_2D_type : " << feature_descriptor_2D_type << std::endl
        << "                    descriptor_matcher_2D_type : " << descriptor_matcher_2D_type << std::endl
        << "                         knn_number_of_matches : " << knn_number_of_matches << std::endl
        << "                       matching_filter_2D_type : " << matching_filter_2D_type << std::endl
        << "                         inliers_max_threshold : " << inliers_max_threshold << std::endl
        << "                        inliers_finding_method : " << inliers_finding_method << std::endl
        << "                              flag_full_affine : " << (flag_full_affine ? "YES" : "NO") << std::endl
        << "              ransac_reprojection_threshold_2D : " << ransac_reprojection_threshold_2D << std::endl
        << "                          homography_2D_method : " << homography_2D_method << std::endl
        << "              ransac_max_distance_threshold_2D : " << ransac_max_distance_threshold_2D << std::endl
        << "                        confidence_probability : " << confidence_probability << std::endl
        << "                  fundamental_matrix_2D_method : " << fundamental_matrix_2D_method << std::endl
        << "                           correspondence_base : " << correspondence_base << std::endl
        << "               registration_distance_threshold : " << registration_distance_threshold << std::endl
        << "   correspondences_rejection_distant_threshold : " << correspondences_rejection_distant_threshold << std::endl
        << "       correspondences_rejection_median_factor : " << correspondences_rejection_median_factor << std::endl
        << "correspondences_rejection_similarity_threshold : " << correspondences_rejection_similarity_threshold << std::endl
        << " correspondences_rejection_polygon_cardinality : " << correspondences_rejection_polygon_cardinality << std::endl
        << "     correspondences_rejection_poly_iterartion : " << correspondences_rejection_poly_iterartion << std::endl
        << "correspondences_rejection_SAC_inlier_threshold : " << correspondences_rejection_SAC_inlier_threshold << std::endl
        << "  correspondences_rejection_SAC_max_iterations : " << correspondences_rejection_SAC_max_iterations << std::endl
        << "     correspondences_rejection_SAC_flag_refine : " << (correspondences_rejection_SAC_flag_refine ? "YES" : "NO") << std::endl
        << "       correspondences_rejection_overlap_ratio : " << correspondences_rejection_overlap_ratio << std::endl
        << " correspondences_rejection_min_correspondences : " << correspondences_rejection_min_correspondences << std::endl
        << "           correspondences_rejection_min_ratio : " << correspondences_rejection_min_ratio << std::endl
        << "           correspondences_rejection_max_ratio : " << correspondences_rejection_max_ratio << std::endl
        << "           transformation_estimation_3D_method : " << transformation_estimation_3D_method << std::endl
        << "  transformation_estimation_3D_correspondences : " << transformation_estimation_3D_correspondences_to_use << std::endl
        << "                                      icp_type : " << icp_type << std::endl
        << "--- ICP settings-----------------------------\n" << icp_settings << std::endl
        << "---------------------------------------------" << std::endl
        << "                          flag_loop_closure : " << (flag_loop_closure ? "YES" : "NO") << std::endl
        << "                          loop_closure_type : " << loop_closure_type << std::endl
        ;
      UINFO(log.str().c_str());
#endif
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_depth_image_scale() const
{
    return depth_image_scale;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_depth_image_scale( double scale )
{
    if (0==scale) return;

    depth_image_scale = scale;
}

//==============================================================================
//==============================================================================

hjh::Intrinsics
RegistrationPipelineConfig::get_target_intrinsics_color() const
{
    return target_intrinsics_color;
}

//==============================================================================
//==============================================================================

hjh::Intrinsics
RegistrationPipelineConfig::get_target_intrinsics_depth() const
{
    return target_intrinsics_depth;
}

//==============================================================================
//==============================================================================

hjh::Intrinsics
RegistrationPipelineConfig::get_source_intrinsics_color() const
{
    return source_intrinsics_color;
}

//==============================================================================
//==============================================================================

hjh::Intrinsics
RegistrationPipelineConfig::get_source_intrinsics_depth() const
{
    return source_intrinsics_depth;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_target_intrinsics_color( hjh::Intrinsics intr_ )
{
    target_intrinsics_color = intr_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_target_intrinsics_depth( hjh::Intrinsics intr_ )
{
    target_intrinsics_depth = intr_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_source_intrinsics_color( hjh::Intrinsics intr_ )
{
    source_intrinsics_color = intr_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_source_intrinsics_depth( hjh::Intrinsics intr_ )
{
    source_intrinsics_depth = intr_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_feature_detector_2D_type( std::string type_ )
{
    feature_detector_2D_type = type_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_feature_detector_2D_type() const
{
    return feature_detector_2D_type;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_feature_descriptor_2D_type( std::string type_ )
{
    feature_descriptor_2D_type = type_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_feature_descriptor_2D_type() const
{
    return feature_descriptor_2D_type;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_descriptor_matcher_2D_type( std::string type_ )
{
    descriptor_matcher_2D_type = type_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_descriptor_matcher_2D_type() const
{
    return descriptor_matcher_2D_type;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_knn_number_of_matches( int knn_ )
{
    if ( knn_>0 )
    {
        knn_number_of_matches = knn_;
    }
}

//==============================================================================
//==============================================================================

int
RegistrationPipelineConfig::get_knn_number_of_matches() const
{
    return knn_number_of_matches;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_matching_filter_2D_type( std::string type_ )
{
    matching_filter_2D_type = type_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_matching_filter_2D_type() const
{
    return matching_filter_2D_type;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_inliers_max_threshold() const
{
    return inliers_max_threshold;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_inliers_max_threshold( double threshold_ )
{
    inliers_max_threshold = threshold_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_inliers_finding_method() const
{
    return inliers_finding_method;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_inliers_finding_method( std::string method_ )
{
    inliers_finding_method = method_;
}

//==============================================================================
//==============================================================================

bool
RegistrationPipelineConfig::get_flag_full_affine() const
{
    return flag_full_affine;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_flag_full_affine( bool flag_ )
{
    flag_full_affine = flag_;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_ransac_reprojection_threshold_2D() const
{
    return ransac_reprojection_threshold_2D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_ransac_reprojection_threshold_2D( double threshold_ )
{
    ransac_reprojection_threshold_2D = threshold_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_homography_2D_method() const
{
    return homography_2D_method;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_homography_2D_method( std::string method_ )
{
    homography_2D_method = method_;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_ransac_max_distance_threshold_2D() const
{
    return ransac_max_distance_threshold_2D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_ransac_max_distance_threshold_2D( double threshold_ )
{
    ransac_max_distance_threshold_2D = threshold_;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_confidence_probability() const
{
    return confidence_probability;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_confidence_probability( double confidence_probability_ )
{
    confidence_probability = confidence_probability_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_fundamental_matrix_2D_method() const
{
    return fundamental_matrix_2D_method;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_fundamental_matrix_2D_method( std::string method_ )
{
    fundamental_matrix_2D_method = method_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_correspondence_base() const
{
    return correspondence_base;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondence_base( std::string base_ )
{
    correspondence_base = base_;
}

//==============================================================================
//==============================================================================

float
RegistrationPipelineConfig::get_registration_distance_threshold() const
{
    return registration_distance_threshold;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_registration_distance_threshold( float threshold_ )
{
    registration_distance_threshold = threshold_;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_correspondences_rejection_distant_threshold() const
{
    return correspondences_rejection_distant_threshold;
}


//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_distant_threshold(
        double threshold_ )
{
    correspondences_rejection_distant_threshold = threshold_;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_correspondences_rejection_median_factor() const
{
    return correspondences_rejection_median_factor;
}


//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_median_factor(
        double factor_ )
{
    correspondences_rejection_median_factor = factor_;
}

//==============================================================================
//==============================================================================

float
RegistrationPipelineConfig::get_correspondences_rejection_similarity_threshold() const
{
    return correspondences_rejection_similarity_threshold;
}

//==============================================================================
//==============================================================================

int
RegistrationPipelineConfig::get_correspondences_rejection_polygon_cardinality() const
{
    return correspondences_rejection_polygon_cardinality;
}

//==============================================================================
//==============================================================================

int
RegistrationPipelineConfig::get_correspondences_rejection_poly_iterartion() const
{
    return correspondences_rejection_poly_iterartion;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_similarity_threshold( float threshold_ )
{
    correspondences_rejection_similarity_threshold = threshold_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_polygon_cardinality( int cardinality_ )
{
    correspondences_rejection_polygon_cardinality = cardinality_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_poly_iterartion( int iteration_ )
{
    correspondences_rejection_poly_iterartion = iteration_;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_correspondences_rejection_SAC_inlier_threshold() const
{
    return correspondences_rejection_SAC_inlier_threshold;
}

//==============================================================================
//==============================================================================

int
RegistrationPipelineConfig::get_correspondences_rejection_SAC_max_iterations() const
{
    return correspondences_rejection_SAC_max_iterations;
}

//==============================================================================
//==============================================================================

bool
RegistrationPipelineConfig::get_correspondences_rejection_SAC_flag_refine() const
{
    correspondences_rejection_SAC_flag_refine;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_SAC_inlier_threshold(
        double threshold_ )
{
    correspondences_rejection_SAC_inlier_threshold = threshold_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_SAC_max_iterations(
        int iterations_ )
{
    correspondences_rejection_SAC_max_iterations = iterations_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_SAC_flag_refine(
        bool flag_ )
{
    correspondences_rejection_SAC_flag_refine = flag_;
}

//==============================================================================
//==============================================================================

float
RegistrationPipelineConfig::get_correspondences_rejection_overlap_ratio() const
{
    return correspondences_rejection_overlap_ratio;
}

//==============================================================================
//==============================================================================

unsigned int
RegistrationPipelineConfig::get_correspondences_rejection_min_correspondences() const
{
    return correspondences_rejection_min_correspondences;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_overlap_ratio( float ratio_ )
{
    correspondences_rejection_overlap_ratio = ratio_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_min_correspondences(
        unsigned int correspondences_ )
{
    correspondences_rejection_min_correspondences = correspondences_;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_correspondences_rejection_min_ratio() const
{
    return correspondences_rejection_min_ratio;
}

//==============================================================================
//==============================================================================

double
RegistrationPipelineConfig::get_correspondences_rejection_max_ratio() const
{
    return correspondences_rejection_max_ratio;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_min_ratio( double ratio_ )
{
    correspondences_rejection_min_ratio = ratio_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_correspondences_rejection_max_ratio( double ratio_ )
{
    correspondences_rejection_max_ratio = ratio_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_transformation_estimation_3D_method() const
{
    return transformation_estimation_3D_method;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_transformation_estimation_3D_method( std::string type_)
{
    transformation_estimation_3D_method = type_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_transformation_estimation_3D_correspondences_to_use() const
{
    return transformation_estimation_3D_correspondences_to_use;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_transformation_estimation_3D_correspondences_to_use( std::string method_ )
{
    transformation_estimation_3D_correspondences_to_use = method_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_icp_type() const
{
    return icp_type;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_icp_type( std::string type_ )
{
    icp_type = type_;
}

//==============================================================================
//==============================================================================

bool
RegistrationPipelineConfig::get_flag_loop_closure() const
{
    return flag_loop_closure;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_flag_loop_closure( bool flag_ )
{
    flag_loop_closure = flag_;
}

//==============================================================================
//==============================================================================

std::string
RegistrationPipelineConfig::get_loop_closure_type() const
{
    return loop_closure_type;
}

//==============================================================================
//==============================================================================

void
RegistrationPipelineConfig::set_loop_closure_type( std::string type_ )
{
    loop_closure_type = type_;
}

//==============================================================================
//==============================================================================


} // end of namespace registration
} // end of namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
