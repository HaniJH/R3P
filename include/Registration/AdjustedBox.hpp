
#ifndef _ADJUSTED_BOX_HPP_20150810
#define _ADJUSTED_BOX_HPP_20150810

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "haniDefinitions.hpp"

namespace hjh {
namespace registration {

// *****************************************************************************
// *********************     E N U E R A T I O N S   ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************         C L A S S E S       ***************************
// *****************************************************************************

/*!
 * \brief The AdjustedBox class contains the transformation data for a box to be
 * adjusted into its corresponding FARO-based 3D model.
 */
class AdjustedBox
{
public:

    //--------------------------------------------------------------------------
    AdjustedBox();
    //!< Default constructor.

    AdjustedBox( const AdjustedBox &obj );
    //!< Copy constructor.

    /*!
     * \brief AdjustedBox is the constructor.
     * \param[in]   box_number_     the box number.
     * \param[in]   box_ID_         the box ID.
     * \param[in]   transformation_ the transformation.
     */
    AdjustedBox(
            int box_number_
            , std::string box_ID_
            , hjh::Pose transformation_
            );

    virtual
    ~AdjustedBox();
    //!< Destructor.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    virtual
    inline
    AdjustedBox&
    operator=( const AdjustedBox &obj );
    //!< Overloaded '=' (assignemt) operator.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    typedef boost::shared_ptr<AdjustedBox> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const AdjustedBox> ConstPtr;
    //!< Const shared pointer.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    virtual
    inline
    int
    get_box_number() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    std::string
    get_box_ID() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    hjh::Pose
    get_transformation() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    void
    set_box_number( int number_ );
    //!< Sets the corresponding data member.

    virtual
    inline
    void
    set_box_ID( std::string id_ );
    //!< Sets the corresponding data member.

    virtual
    inline
    void
    set_transformation( hjh::Pose trans_ );
    //!< Sets the corresponding data member.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    friend std::ostream&
    operator<<(std::ostream& os, const AdjustedBox& obj);
    //!< Overloaded output-stream.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
private:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler.
    //--------------------------------------------------------------------------

protected:

    //--------------------------------------------------------------------------
    /*!
     * \brief box_number indicates the current box order in a Box-Chain structure.
     */
    int box_number;

    /*!
     * \brief box_ID the box ID.
     */
    std::string box_ID;

    /*!
     * \brief transformation determines the required transformation of the current
     * box (in a Box-Chain) to adjust it into its corresponding FARO-based 3D model.
     * \note it is presented as \ref hjh::Pose format.
     */
    hjh::Pose transformation;

    //--------------------------------------------------------------------------
};


// *****************************************************************************
// *****************************************************************************

} // end of namespace registration
} // end of namespace hjh

#include "Registration/AdjustedBox_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
