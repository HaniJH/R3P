
#ifndef _REGISTRATION_PIPELINE_CONFIG_IMP_HPP_20150814_
#define _REGISTRATION_PIPELINE_CONFIG_IMP_HPP_20150814_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "boost_serialization.hpp"

namespace hjh {
namespace registration {

// forward declaration
class ICP_config;

//==============================================================================
//==============================================================================

template<class Archive>
void
ICP_config::serialize( Archive & ar, const unsigned int version )
{
    ar & BOOST_SERIALIZATION_NVP( euclidean_fitness_epsilon );
    ar & BOOST_SERIALIZATION_NVP( corr_dist_threshold );
    ar & BOOST_SERIALIZATION_NVP( max_iterations );
    ar & BOOST_SERIALIZATION_NVP( ransac_iterations );
    ar & BOOST_SERIALIZATION_NVP( inlier_threshold );
    ar & BOOST_SERIALIZATION_NVP( transformation_epsilon );
}

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace registration
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
