#ifndef _BOX_PYRAMID_PAIR_IMP_HPP_20150727
#define _BOX_PYRAMID_PAIR_IMP_HPP_20150727

#include "boost_serialization.hpp"

namespace hjh {
namespace registration {

// forward declaration
class BoxPyramidPair;

// *****************************************************************************
// *****************************************************************************

template<class Archive>
void
BoxPyramidPair::serialize( Archive & ar, const unsigned int version )
{
    ar & BOOST_SERIALIZATION_NVP( flag_valid );
    ar & BOOST_SERIALIZATION_NVP( box );
    ar & BOOST_SERIALIZATION_NVP( pyramid );
    ar & BOOST_SERIALIZATION_NVP( score );
    ar & BOOST_SERIALIZATION_NVP( number_of_matches );
    ar & BOOST_SERIALIZATION_NVP( transform );
    ar & BOOST_SERIALIZATION_NVP( transform_ICP );
    ar & BOOST_SERIALIZATION_NVP( comparison_mode );
}

// *****************************************************************************
// *****************************************************************************

} // end of namespace registration
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
