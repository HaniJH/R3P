// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "Registration/RegistrationPipeline.hpp"

#include "haniUtilities.hpp"

#include <sstream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <pcl/registration/transformation_estimation.h>
#include <pcl/registration/transformation_estimation_dual_quaternion.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/registration/transformation_estimation_lm.h>
#include <pcl/registration/transformation_estimation_svd_scale.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
//#include <pcl/registration/gicp6d.h> // XYZRGB
#include <pcl/registration/icp_nl.h>


namespace hjh {
namespace registration {

// *****************************************************************************
// *********************         G L O B A L S       ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************      P R O T O T Y P E S    ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************     D E F I N I T I O N S   ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

RegistrationPipeline::RegistrationPipeline()
    : has_logger( false )
    , initialized( false )
    , number_of_homography_based_inliers_2D( 0 )
{
    transformation_3D = Eigen::MatrixXf(4,4).setIdentity();

    icp_transformation_3D = Eigen::MatrixXf(4,4).setIdentity();
}

//==============================================================================
//==============================================================================

RegistrationPipeline::RegistrationPipeline(  log::Logger::Ptr external_logger_ )
    : has_logger( false )
    , initialized( false )
    , number_of_homography_based_inliers_2D( 0 )
{
    transformation_3D = Eigen::MatrixXf(4,4).setIdentity();

    icp_transformation_3D = Eigen::MatrixXf(4,4).setIdentity();

    if ( external_logger_ )
    {
        logger_ptr = external_logger_;
        has_logger = true;

        std::stringstream ss;

        ss.str("");
        ss << "[RegistrationPipeline::RegistrationPipeline] created successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::initialize()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::initialize] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //--------------------------------------------------------------------------
    //      E n a b l i n g   n o n - f r e e   ( i f   r e q u i r e d ) // BADAN ToDo
    //--------------------------------------------------------------------------

    if (
            config.get_feature_detector_2D_type() == "SIFT"
            || config.get_feature_detector_2D_type() == "SURF"
            || config.get_feature_descriptor_2D_type() == "SIFT"
            || config.get_feature_descriptor_2D_type() == "SURF"
       )
    {
        bool result = cv::initModule_nonfree();

        if (has_logger)
        {
            if ( result )
            {
                ss.str("");
                ss << "[RegistrationPipeline::initialize] OpenCV non-free modules initialized successfully.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
            }
            else
            {
                ss.str("");
                ss << "[RegistrationPipeline::initialize] OpenCV non-free initialization failed.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
        }
    }


    //--------------------------------------------------------------------------
    //      F e a t u r e   D e t e c t o r   2 D
    //--------------------------------------------------------------------------

    detector_2D_ptr = cv::FeatureDetector::create( config.get_feature_detector_2D_type() );

    if( detector_2D_ptr.empty() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::initialize] Can not create a detector of given type '"
               << config.get_feature_detector_2D_type() << "'.";
             logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return -1;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::initialize] 'detector-2D' initialized as '"
           << config.get_feature_detector_2D_type() << "'.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }


    //--------------------------------------------------------------------------
    //      F e a t u r e   D e s c r i p t o r   2 D
    //--------------------------------------------------------------------------

    descriptor_2D_ptr = cv::DescriptorExtractor::create( config.get_feature_descriptor_2D_type() );

    if( descriptor_2D_ptr.empty() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::initialize] Can not create a descriptor of given type '"
               << config.get_feature_descriptor_2D_type() << "'.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return -1;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::initialize] 'descriptor-2D' initialized as '"
           << config.get_feature_descriptor_2D_type() << "'.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }


    //--------------------------------------------------------------------------
    //      F e a t u r e   M a t c h e r   2 D
    //--------------------------------------------------------------------------

    matcher_2D_ptr = cv::DescriptorMatcher::create( config.get_descriptor_matcher_2D_type() );

    if( matcher_2D_ptr.empty() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::initialize] Can not create a matcher of given type '"
               << config.get_descriptor_matcher_2D_type() << "'.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return -1;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::initialize] 'matcher-2D' initialized as '"
           << config.get_descriptor_matcher_2D_type() << "'.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //--------------------------------------------------------------------------
    //      T r a n s f o r m a t i o n   E s t i m a t o r   3 D
    //--------------------------------------------------------------------------

    init_transformation_estimator_3D();


    //--------------------------------------------------------------------------
    //      I C P
    //--------------------------------------------------------------------------

    init_icp_ptr();


    //--------------------------------------------------------------------------
    //      d o n e !   :)
    //--------------------------------------------------------------------------

    initialized = true;


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::initialize] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
    return 0;
}

//==============================================================================
//==============================================================================

bool
RegistrationPipeline::is_initialized() const
{
    return initialized;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::swap_source_and_target()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::swap_source_and_target] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //--------------------------------------------------------------------------
    // images ------------------------------------------------------------------
    //--------------------------------------------------------------------------

    cv::Mat temp;

    temp = target_color;
    target_color = source_color;
    source_color = temp;

    temp = target_depth;
    target_depth = source_depth;
    source_depth = temp;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::swap_source_and_target] images exchanged.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
    }

    //--------------------------------------------------------------------------
    // pointclouds -------------------------------------------------------------
    //--------------------------------------------------------------------------

    target_cloud_ptr.swap( source_cloud_ptr );
    target_cloud_depth_ptr.swap( source_cloud_depth_ptr );
    target_cloud_keypoints_ptr.swap( source_cloud_keypoints_ptr );

    //--------------------------------------------------------------------------
    // keypoints-2D ------------------------------------------------------------
    //--------------------------------------------------------------------------

    target_keypoints_2D.swap( source_keypoints_2D );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::swap_source_and_target] keypoints-2D exchanged.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
    }

    //--------------------------------------------------------------------------
    // descriptors-2D ----------------------------------------------------------
    //--------------------------------------------------------------------------

    temp = target_descriptors_2D;
    target_descriptors_2D = source_descriptors_2D;
    source_descriptors_2D = temp;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::swap_source_and_target] descriptors-2D exchanged.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
    }


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::swap_source_and_target] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

RegistrationPipeline::~RegistrationPipeline()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::~RegistrationPipeline] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    detector_2D_ptr.release();
    descriptor_2D_ptr.release();
    matcher_2D_ptr.release();

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::~RegistrationPipeline] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

//--------------------------------------------------------------------------
//      i n p u t   d a t a   2 D / 3 D
//--------------------------------------------------------------------------

cv::Mat
RegistrationPipeline::get_target_color() const
{
    return target_color;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_target_depth() const
{
    return target_depth;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_source_color() const
{
    return source_color;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_source_depth() const
{
    return source_depth;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::set_target_color( const cv::Mat& img_ )
{
    std::stringstream ss;

    if (img_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::set_target_color] empty image.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    target_color = img_;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::set_target_color] new target_color set.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::set_target_depth( const cv::Mat& img_ )
{
    std::stringstream ss;

    if (img_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::set_target_depth] empty image.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    target_depth = img_;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::set_target_depth] new target_depth set.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::set_source_color( const cv::Mat& img_ )
{
    std::stringstream ss;

    if (img_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::set_source_color] empty image.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    source_color = img_;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::set_source_color] new source_color set.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
 }

//==============================================================================
//==============================================================================

pcl::PointCloud<pcl::PointXYZ>::Ptr
RegistrationPipeline::get_target_cloud_ptr() const
{
    return target_cloud_ptr;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::set_target_cloud_ptr( const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_ptr )
{
    std::stringstream ss;

    if (!cloud_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::set_target_cloud_ptr] empty cloud_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    target_cloud_ptr = cloud_ptr;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::set_target_cloud_ptr] new target_cloud_ptr set.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

pcl::PointCloud<pcl::PointXYZ>::Ptr
RegistrationPipeline::get_source_cloud_ptr() const
{
    return source_cloud_ptr;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::set_source_cloud_ptr( const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_ptr )
{
    std::stringstream ss;

    if (!cloud_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::set_source_cloud_ptr] empty cloud_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    source_cloud_ptr = cloud_ptr;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::set_source_cloud_ptr] new source_cloud_ptr set.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::set_source_depth( const cv::Mat& img_ )
{
    std::stringstream ss;

    if (img_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::set_source_depth] empty image.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    source_depth = img_;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::set_source_depth] new source_depth set.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

//--------------------------------------------------------------------------
//      F e a t u r e   D e t e c t o r   2 D
//--------------------------------------------------------------------------

std::vector<cv::KeyPoint>
RegistrationPipeline::get_target_keypoints_2D() const
{
    return target_keypoints_2D;
}

//==============================================================================
//==============================================================================

std::vector<cv::KeyPoint>
RegistrationPipeline::get_source_keypoints_2D() const
{
    return source_keypoints_2D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::detect_target_keypoints_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::detect_target_keypoints_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (!detector_2D_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::extract_target_keypoints] empty detector_ptr (uninitialized).";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::extract_target_keypoints] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // clear the target vector and detect the keypints
    target_keypoints_2D.clear();
    detector_2D_ptr->detect( target_color, target_keypoints_2D );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::extract_target_keypoints] done successfully ("
           << target_keypoints_2D.size() << " key points).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::detect_source_keypoints_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::detect_source_keypoints_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (!detector_2D_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::extract_source_keypoints] empty detector_ptr (uninitialized).";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::extract_source_keypoints] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // clear the source vector and detect the keypints
    source_keypoints_2D.clear();
    detector_2D_ptr->detect( source_color, source_keypoints_2D );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::extract_source_keypoints] done successfully ("
           << source_keypoints_2D.size() << " key points).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

std::vector<cv::KeyPoint>
RegistrationPipeline::detect_keypoints_2D( const cv::Mat& image_ ) const
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::detect_keypoints_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    std::vector<cv::KeyPoint> keypoints_;

    if (!detector_2D_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::extract_keypoints] empty detector_ptr (uninitialized).";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return keypoints_;
    }

    if (image_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::extract_keypoints] empty image_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return keypoints_;
    }

    detector_2D_ptr->detect( image_, keypoints_ );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::extract_keypoints] done successfully ("
           << keypoints_.size() << " key points).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return keypoints_;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::draw_target_keypoints_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_target_keypoints_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_target_keypoints] empty target_keypoints.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_target_keypoints] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    cv::drawKeypoints( target_color, target_keypoints_2D, target_color );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_target_keypoints] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::draw_source_keypoints_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_source_keypoints_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_source_keypoints] empty source_keypoints.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_source_keypoints] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    cv::drawKeypoints( source_color, source_keypoints_2D, source_color );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_source_keypoints] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::draw_keypoints_2D( const std::vector<cv::KeyPoint> &keypoints_, cv::Mat image_ ) const
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_keypoints_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (keypoints_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_keypoints] empty keypoints_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (image_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_keypoints] empty image_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    cv::drawKeypoints( image_, keypoints_, image_ );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_keypoints] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

//--------------------------------------------------------------------------
//      F e a t u r e   D e s c r i p t o r   2 D
//--------------------------------------------------------------------------

cv::Mat
RegistrationPipeline::get_target_descriptors_2D() const
{
    return target_descriptors_2D;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_source_descriptors_2D() const
{
    return source_descriptors_2D;
}


//==============================================================================
//==============================================================================

void
RegistrationPipeline::compute_target_descriptors_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::compute_target_descriptors_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::compute_target_descriptors] empty target_keypoints.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::compute_target_descriptors] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    target_descriptors_2D.release();
    descriptor_2D_ptr->compute( target_color, target_keypoints_2D, target_descriptors_2D );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::compute_target_descriptors] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::compute_source_descriptors_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::compute_source_descriptors_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::compute_source_descriptors] empty source_keypoints.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::compute_source_descriptors] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    source_descriptors_2D.release();
    descriptor_2D_ptr->compute( source_color, source_keypoints_2D, source_descriptors_2D );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::compute_source_descriptors] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::compute_descriptors_2D(const cv::Mat& image_
                           , std::vector<cv::KeyPoint> &keypoints_ ) const
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::compute_descriptors_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    cv::Mat descriptors_;

    if (keypoints_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::compute_descriptors_2D] empty keypoints_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (image_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::compute_descriptors_2D] empty image_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return descriptors_;
    }

    descriptor_2D_ptr->compute( image_, keypoints_, descriptors_ );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::compute_descriptors_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return descriptors_;
}

//==============================================================================
//==============================================================================

std::vector<cv::DMatch>
RegistrationPipeline::get_filtered_matches_2D() const
{
    return filtered_matches_2D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::match_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::match_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_descriptors_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D] empty target_descriptors.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (source_descriptors_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D] empty source_descriptors.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if ( "CrossCheck" == config.get_matching_filter_2D_type() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D] Cross-Check matching.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        filtered_matches_2D.clear();

        std::vector<std::vector<cv::DMatch> > matches12, matches21;

        matcher_2D_ptr->knnMatch( target_descriptors_2D, source_descriptors_2D
                                     , matches12, config.get_knn_number_of_matches() );
        matcher_2D_ptr->knnMatch( source_descriptors_2D, target_descriptors_2D
                                     , matches21, config.get_knn_number_of_matches() );

        for( size_t m = 0; m < matches12.size(); m++ )
        {
            bool findCrossCheck = false;
            for( size_t fk = 0; fk < matches12[m].size(); fk++ )
            {
                cv::DMatch forward = matches12[m][fk];

                for( size_t bk = 0; bk < matches21[forward.trainIdx].size(); bk++ )
                {
                    cv::DMatch backward = matches21[forward.trainIdx][bk];
                    if( backward.trainIdx == forward.queryIdx )
                    {
                        filtered_matches_2D.push_back(forward);
                        findCrossCheck = true;
                        break;
                    }
                }
                if( findCrossCheck ) break;
            }
        }
    }
    else if ( "Simple" == config.get_matching_filter_2D_type() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D] simple matching.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        matcher_2D_ptr->match( target_descriptors_2D, source_descriptors_2D
                               , filtered_matches_2D );
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D] Unknown matching filter.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::match_2D] done successfully ("
           << filtered_matches_2D.size() << " matches).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

std::vector<cv::DMatch>
RegistrationPipeline::match_2D_standalone( cv::Mat target_des_
                                           , cv::Mat source_des_ )
{
    std::vector<cv::DMatch> fmatched;

    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::match_2D_standalone] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_des_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D_standalone] empty target_des_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return fmatched;
    }

    if (source_des_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D_standalone] empty source_des_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return fmatched;
    }

    if ( "CrossCheck" == config.get_matching_filter_2D_type() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D_standalone] Cross-Check matching.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        fmatched.clear();

        std::vector<std::vector<cv::DMatch> > matches12, matches21;

        matcher_2D_ptr->knnMatch( target_des_, source_des_, matches12, config.get_knn_number_of_matches() );
        matcher_2D_ptr->knnMatch( source_des_, target_des_, matches21, config.get_knn_number_of_matches() );

        for( size_t m = 0; m < matches12.size(); m++ )
        {
            bool findCrossCheck = false;
            for( size_t fk = 0; fk < matches12[m].size(); fk++ )
            {
                cv::DMatch forward = matches12[m][fk];

                for( size_t bk = 0; bk < matches21[forward.trainIdx].size(); bk++ )
                {
                    cv::DMatch backward = matches21[forward.trainIdx][bk];
                    if( backward.trainIdx == forward.queryIdx )
                    {
                        fmatched.push_back(forward);
                        findCrossCheck = true;
                        break;
                    }
                }
                if( findCrossCheck ) break;
            }
        }
    }
    else if ( "Simple" == config.get_matching_filter_2D_type() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D_standalone] simple matching.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        matcher_2D_ptr->match( target_des_, source_des_, fmatched );
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::match_2D_standalone] Unknown matching filter.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return fmatched;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::match_2D_standalone] done successfully ("
           << fmatched.size() << " matches).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return fmatched;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_ptr() const
{
    return correspondences_ptr;
}

//==============================================================================
//==============================================================================

std::vector<cv::Point>
RegistrationPipeline::get_target_keypoints_2D_pixels() const
{
    return target_keypoints_2D_pixels;
}

//==============================================================================
//==============================================================================

std::vector<cv::Point>
RegistrationPipeline::get_source_keypoints_2D_pixels() const
{
    return source_keypoints_2D_pixels;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::estimate_correspondences()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::estimate_correspondences] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_ptr) correspondences_ptr.reset();
    correspondences_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);
    target_keypoints_2D_pixels.clear();
    source_keypoints_2D_pixels.clear();

    if (target_depth.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_correspondences] empty target_depth.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (source_depth.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_correspondences] empty source_depth.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::estimate_correspondences] based on \""
           << config.get_correspondence_base() << "\".";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }


    if ( "homography" == config.get_correspondence_base() )
    {
        if (homography_based_inliers_mask_2D.empty())
        {
            if (has_logger)
            {
                ss.str("");
                ss << "[RegistrationPipeline::estimate_correspondences] empty homography_based_inliers_mask_2D.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }
    }
    if ( "fundamental_matrix" == config.get_correspondence_base() )
    {
        if ( fundamental_matrix_based_inliers_mask_2D.empty() )
        {
            if (has_logger)
            {
                ss.str("");
                ss << "[RegistrationPipeline::estimate_correspondences] empty fundamental_matrix_based_inliers_mask_2D.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }
    }
    else if ( "affine" == config.get_correspondence_base() )
    {
        if ( affine_based_inliers_mask_2D.empty() )
        {
            if (has_logger)
            {
                ss.str("");
                ss << "[RegistrationPipeline::estimate_correspondences] empty affine_based_inliers_mask_2D.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }
    }
    else if ( "perspective" == config.get_correspondence_base() )
    {
        if ( perspective_based_inliers_mask_2D.empty())
        {
            if (has_logger)
            {
                ss.str("");
                ss << "[RegistrationPipeline::estimate_correspondences] empty perspective_based_inliers_mask_2D.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }
    }
    else if ( "rigid_transform" == config.get_correspondence_base() )
    {
        if ( rigid_transformation_based_inliers_mask_2D.empty())
        {
            if (has_logger)
            {
                ss.str("");
                ss << "[RegistrationPipeline::estimate_correspondences] empty rigid_transformation_based_inliers_mask_2D.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }
    }
    else if ( "rigid_transform_direct" == config.get_correspondence_base() )
    {
        if ( rigid_transformation_direct_based_inliers_mask_2D.empty())
        {
            if (has_logger)
            {
                ss.str("");
                ss << "[RegistrationPipeline::estimate_correspondences] empty rigid_transformation_direct_based_inliers_mask_2D.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_correspondences] no inlier-detection process is applied.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_correspondences] empty target_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_correspondences] empty source_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_correspondences] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }


    // loop to estimate correspondences_ptr
    int index = 0;
    int i=0;
    for ( std::vector<cv::DMatch>::const_iterator it = filtered_matches_2D.begin();
         it != filtered_matches_2D.end(); it++, ++i )
    {
        if ( "homography" == config.get_correspondence_base()
            && !homography_based_inliers_mask_2D[i] ) continue;

        if ( "fundamental_matrix" == config.get_correspondence_base()
            && !fundamental_matrix_based_inliers_mask_2D[i] ) continue;

        if ( "affine" == config.get_correspondence_base()
            && !affine_based_inliers_mask_2D[i] ) continue;

        if ( "perspective" == config.get_correspondence_base()
            && !perspective_based_inliers_mask_2D[i] ) continue;

        if ( "rigid_transform" == config.get_correspondence_base()
            && !rigid_transformation_based_inliers_mask_2D[i] ) continue;

        if ( "rigid_transform_direct" == config.get_correspondence_base()
            && !rigid_transformation_direct_based_inliers_mask_2D[i] ) continue;


        cv::KeyPoint target_point = target_keypoints_2D[it->queryIdx];
        cv::KeyPoint source_point = source_keypoints_2D[it->trainIdx];

        int target_x = static_cast<int>( target_point.pt.x+0.5 );
        int target_y = static_cast<int>( target_point.pt.y+0.5 );
        int source_x = static_cast<int>( source_point.pt.x+0.5 );
        int source_y = static_cast<int>( source_point.pt.y+0.5 );

        if ( 0==target_depth.at<uint16_t>(target_y, target_x)
                || 0==source_depth.at<uint16_t>(source_y, source_x) ) continue;

        target_keypoints_2D_pixels.push_back( cv::Point(target_x, target_y) );
        source_keypoints_2D_pixels.push_back( cv::Point(source_x, source_y) );

        correspondences_ptr->push_back( pcl::Correspondence( index, index, it->distance) );

        index++;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::estimate_correspondences] done successfully ("
           << correspondences_ptr->size() << " correspondences).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_correspondent_inlier_keypoints() const
{
    std::string method_info( "draw_correspondent_inlier_keypoints" );

    // the empty unified image
    cv::Mat img;

    std::stringstream ss;

    // started
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //--------------------------------------------------------------------------
    // check the availability of the required data
    //--------------------------------------------------------------------------
    // color
    if ( target_color.empty() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return img;
    }
    if ( source_color.empty() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return img;
    }
    //--------------------------------------------------------------------------
    // depth
    if ( target_depth.empty() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty target_depth.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return img;
    }
    if ( source_depth.empty() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty source_depth.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return img;
    }
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    //  required steps:
    //      - creaating the unified image (size)
    //      - masked color images
    //      - put the images in the unified image
    //      - draw the inliers lines
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    //  - creaating the unified image (size)
    //      it assumes the images have the same size.
    //--------------------------------------------------------------------------

    img = cv::Mat::zeros( target_color.rows, target_color.cols*2, target_color.type() ) ;

    //--------------------------------------------------------------------------
    //  - masked color images
    //--------------------------------------------------------------------------

    cv::Vec3b zero_pixel;
    zero_pixel[0] = zero_pixel[1] = zero_pixel[2] = 0;


    cv::Mat target_masked;
    target_color.copyTo( target_masked );

    for ( int r=0; r<target_depth.rows; ++r )
        for ( int c=0; c<target_depth.cols; ++c )
            if ( 0 == target_depth.at<uint16_t>(r, c) )
                target_masked.at<cv::Vec3b>(r, c) = zero_pixel;

    cv::Mat source_masked;
    source_color.copyTo( source_masked );

    for ( int r=0; r<source_depth.rows; ++r )
        for ( int c=0; c<source_depth.cols; ++c )
            if ( 0 == source_depth.at<uint16_t>(r, c) )
                source_masked.at<cv::Vec3b>(r, c) = zero_pixel;

    //--------------------------------------------------------------------------
    //  - put the images in the unified image
    //--------------------------------------------------------------------------

    cv::Rect r1( 0, 0,   target_masked.cols, target_masked.rows );
    cv::Rect r2( target_masked.cols, 0,   target_masked.cols, target_masked.rows );
    (target_masked(r1)).copyTo(img(r1));
    (source_masked(r1)).copyTo(img(r2));

    //--------------------------------------------------------------------------
    //  - draw the inliers lines
    //--------------------------------------------------------------------------

    // circles and lines
    std::vector<cv::Point>::const_iterator it_target, it_source;
    for ( it_target = target_keypoints_2D_pixels.begin(),
          it_source = source_keypoints_2D_pixels.begin();

          it_target != target_keypoints_2D_pixels.end(),
          it_source != source_keypoints_2D_pixels.end();

          ++it_target, ++it_source )
    {
        // target circle
        cv::Point pt = *it_target;
        cv::circle( img, pt, 2, cv::Scalar(80,180,0,0), 1, CV_AA );

        // source circle
        cv::Point ps = *it_source;
        ps.x += target_masked.cols;
        cv::circle( img, ps, 2, cv::Scalar(0,180,80,0), 1, CV_AA );

        // lines in between
        cv::line( img, pt, ps, cv::Scalar(100,220,100,0), 1, CV_AA );
    }


    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " ... finished successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return img;
}


//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_ptr() const
{
    return correspondences_3D_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_remaining_ptr() const
{
    return correspondences_remaining_ptr;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::determine_correspondences()
{
    std::string method_info( "[RegistrationPipeline::determine_correspondences]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_ptr) correspondences_3D_ptr.reset();
    correspondences_3D_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);

    if (correspondences_3D_reciprocal_ptr) correspondences_3D_reciprocal_ptr.reset();
    correspondences_3D_reciprocal_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required clouds
    if ( !target_cloud_keypoints_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty target_cloud_keypoints_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if ( !source_cloud_keypoints_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty source_cloud_keypoints_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    // perform the job
    pcl::registration::CorrespondenceEstimation<pcl::PointXYZ, pcl::PointXYZ> corr_est;
    corr_est.setInputSource( source_cloud_keypoints_ptr );
    corr_est.setInputTarget( target_cloud_keypoints_ptr );
    corr_est.determineCorrespondences( *correspondences_3D_ptr
                                       , config.get_registration_distance_threshold() );
    corr_est.determineReciprocalCorrespondences( *correspondences_3D_reciprocal_ptr
                                       , config.get_registration_distance_threshold() );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_reciprocal_ptr;


    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << (int)correspondences_3D_ptr->size() << " correspondences and "
           << (int)correspondences_3D_reciprocal_ptr->size()
           << " reciprocal-correspondences"
           << ").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_reciprocal_ptr() const
{
    return correspondences_3D_reciprocal_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_distance_ptr() const
{
    return correspondences_3D_distance_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_features_ptr() const
{
    return correspondences_3D_features_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_median_distance_ptr() const
{
    return correspondences_3D_median_distance_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_one_to_one_ptr() const
{
    return correspondences_3D_one_to_one_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_organized_boundary_ptr() const
{
    return correspondences_3D_organized_boundary_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_poly_ptr() const
{
    return correspondences_3D_poly_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_sample_consensus_ptr() const
{
    return correspondences_3D_sample_consensus_ptr;
}

//==============================================================================
//==============================================================================

Eigen::Matrix4f
RegistrationPipeline::get_sample_consensus_best_transformation() const
{
    return sample_consensus_best_transformation;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_surface_normal_ptr() const
{
    return correspondences_3D_surface_normal_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_trimmed_ptr() const
{
    return correspondences_3D_trimmed_ptr;
}

//==============================================================================
//==============================================================================

pcl::CorrespondencesPtr
RegistrationPipeline::get_correspondences_3D_var_trimmed_ptr() const
{
    return correspondences_3D_var_trimmed_ptr;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_based_on_config()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_based_on_config]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //--------------------------------------------------------------------------
    // apply the appropriate rejection algorithm based on config data
    //--------------------------------------------------------------------------
    //
    //  no_rejection
    //
    if ( "no_rejection" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on '2D/3D' correspondences (nothings to do).";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }
    }
    //--------------------------------------------------------------------------
    //
    //  general
    //
    else if ( "general" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'general 3D' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        determine_correspondences();
    }
    //--------------------------------------------------------------------------
    //
    //  reciprocal
    //
    else if ( "reciprocal" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'reciprocal' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        determine_correspondences();
    }
    //--------------------------------------------------------------------------
    //
    //  distance
    //
    else if ( "distance" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'distance' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_distance();
    }
    //--------------------------------------------------------------------------
    //
    //  features
    //
    else if ( "features" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'features' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_features();
    }
    //--------------------------------------------------------------------------
    //
    //  median_distance
    //
    else if ( "median_distance" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'median_distance' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_median_distance();
    }
    //--------------------------------------------------------------------------
    //
    //  one_to_one
    //
    else if ( "one_to_one" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'one_to_one' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_one_to_one();
    }
    //--------------------------------------------------------------------------
    //
    //  organized_boundary
    //
    else if ( "organized_boundary" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'organized_boundary' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_organized_boundary();
    }
    //--------------------------------------------------------------------------
    //
    //  poly
    //
    else if ( "poly" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'poly' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_poly();
    }
    //--------------------------------------------------------------------------
    //
    //  sample_consensus
    //
    else if ( "sample_consensus" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'sample_consensus' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_sample_consensus();

    }
    //--------------------------------------------------------------------------
    //
    //  sample_consensus_direct
    //
    else if ( "sample_consensus_direct" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'sample_consensus_direct' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_sample_consensus();
    }
    //--------------------------------------------------------------------------
    //
    //  surface_normal
    //
    else if ( "surface_normal" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'surface_normal' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_surface_normal();
    }
    //--------------------------------------------------------------------------
    //
    //  trimmed
    //
    else if ( "trimmed" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'trimmed' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_trimmed();
    }
    //--------------------------------------------------------------------------
    //
    //  var_trimmed
    //
    else if ( "var_trimmed" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'var_trimmed' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        reject_correspondences_var_trimmed();
    }
    //--------------------------------------------------------------------------
    //
    //  invalid!!!
    //
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << "\""
               <<  config.get_transformation_estimation_3D_correspondences_to_use()
                << "\": invalid transformation_estimation_3D_correspondences_to_use.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------

    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << (int)(correspondences_remaining_ptr->size()) << " correspondences).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_distance()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_distance]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_distance_ptr) correspondences_3D_distance_ptr.reset();
    correspondences_3D_distance_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // perform the job
    pcl::registration::CorrespondenceRejectorDistance corr_rej_dist;
    corr_rej_dist.setMaximumDistance(
                config.get_correspondences_rejection_distant_threshold());

    corr_rej_dist.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
    corr_rej_dist.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

//    corr_rej_dist.setInputCorrespondences( correspondences_ptr );
//    corr_rej_dist.getCorrespondences( *correspondences_3D_distance_ptr );

    corr_rej_dist.getRemainingCorrespondences( *correspondences_ptr
                                               , *correspondences_3D_distance_ptr );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_distance_ptr;


    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << (int)(correspondences_3D_distance_ptr->size()) << " correspondences).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_features()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_features]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_features_ptr) correspondences_3D_features_ptr.reset();
    correspondences_3D_features_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


//    // perform the job
//    pcl::registration::CorrespondenceRejectorFeatures corr_rej_features;

//    corr_rej_features.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
//    corr_rej_features.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );


    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_features_ptr;



//    // done
//    if (has_logger)
//    {
//        ss.str("");
//        ss << method_info << " done successfully ("
//           << (int)(correspondences_3D_distance_ptr->size()) << " correspondences).";
//        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
//    }

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done. NOTE: no result (under construction).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_median_distance()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_median_distance]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_median_distance_ptr) correspondences_3D_median_distance_ptr.reset();
    correspondences_3D_median_distance_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // perform the job
    pcl::registration::CorrespondenceRejectorMedianDistance corr_rej_med_dist;
    corr_rej_med_dist.setMedianFactor(
                config.get_correspondences_rejection_median_factor() );
    corr_rej_med_dist.setInputCorrespondences( correspondences_ptr );
    corr_rej_med_dist.getCorrespondences( *correspondences_3D_median_distance_ptr );

    corr_rej_med_dist.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
    corr_rej_med_dist.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

    corr_rej_med_dist.getRemainingCorrespondences( *correspondences_ptr
                                                   , *correspondences_3D_median_distance_ptr );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_median_distance_ptr;


    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << (int)(correspondences_3D_median_distance_ptr->size()) << " correspondences).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_one_to_one()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_one_to_one]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_one_to_one_ptr) correspondences_3D_one_to_one_ptr.reset();
    correspondences_3D_one_to_one_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // perform the job
    pcl::registration::CorrespondenceRejectorOneToOne corr_rej_121;

//    corr_rej_121.set .setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
//    corr_rej_121.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

    corr_rej_121.getRemainingCorrespondences( *correspondences_ptr
                                              , *correspondences_3D_one_to_one_ptr );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_one_to_one_ptr;


    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << (int)(correspondences_3D_one_to_one_ptr->size()) << " correspondences).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_organized_boundary()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_organized_boundary]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_organized_boundary_ptr) correspondences_3D_organized_boundary_ptr.reset();
    correspondences_3D_organized_boundary_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

//    // perform the job
//    pcl::registration::CorrespondenceRejectionOrganizedBoundary corr_rej_org_bound;

//    corr_rej_org_bound.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
//    corr_rej_org_bound.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

//    corr_rej_org_bound.getRemainingCorrespondences( *correspondences_ptr
//                                              , *correspondences_3D_organized_boundary_ptr );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_organized_boundary_ptr;


//    // done
//    if (has_logger)
//    {
//        ss.str("");
//        ss << method_info << " done successfully ("
//           << (int)(correspondences_3D_organized_boundary_ptr->size()) << " correspondences).";
//        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
//    }

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done. NOTE: no result (under construction).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_poly()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_poly]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_poly_ptr) correspondences_3D_poly_ptr.reset();
    correspondences_3D_poly_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

//    // perform the job
//    pcl::registration::CorrespondenceRejectorPoly<pcl::PointXYZ, pcl::PointXYZ>
//            corr_rej_poly;
//    corr_rej_poly.setCardinality(
//                config.get_correspondences_rejection_polygon_cardinality() );
//    corr_rej_poly.setIterations(
//                config.get_correspondences_rejection_poly_iterartion() );
//    corr_rej_poly.setSimilarityThreshold(
//                config.get_correspondences_rejection_similarity_threshold() );

//    //corr_rej_poly.setSourcePoints( *source_cloud_keypoints_ptr );
//    //corr_rej_poly.setTargetPoints( target_cloud_keypoints_ptr );

//    corr_rej_poly.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
//    corr_rej_poly.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

//    corr_rej_poly.getRemainingCorrespondences( *correspondences_ptr
//                                              , *correspondences_3D_poly_ptr );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_poly_ptr;


    // done
//    if (has_logger)
//    {
//        ss.str("");
//        ss << method_info << " done successfully ("
//           << (int)(correspondences_3D_poly_ptr->size()) << " correspondences).";
//        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
//    }

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done. NOTE: no result (under construction).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_sample_consensus()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_sample_consensus]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_sample_consensus_ptr) correspondences_3D_sample_consensus_ptr.reset();
    correspondences_3D_sample_consensus_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // check required clouds
    if ( !target_cloud_keypoints_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty target_cloud_keypoints_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if ( !source_cloud_keypoints_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty source_cloud_keypoints_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // perform the job
    pcl::registration::CorrespondenceRejectorSampleConsensus<pcl::PointXYZ> corr_rej_sac;
    corr_rej_sac.setInputSource( source_cloud_keypoints_ptr );
    corr_rej_sac.setInputTarget( target_cloud_keypoints_ptr );
    corr_rej_sac.setInlierThreshold(
                config.get_correspondences_rejection_SAC_inlier_threshold() );
    corr_rej_sac.setMaximumIterations(
                config.get_correspondences_rejection_SAC_max_iterations() );
    corr_rej_sac.setRefineModel(
                config.get_correspondences_rejection_SAC_flag_refine() );

//    corr_rej_sac.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
//    corr_rej_sac.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

//    corr_rej_sac.setInputCorrespondences( correspondences_ptr );
//    corr_rej_sac.getCorrespondences( *correspondences_3D_sample_consensus_ptr );

    corr_rej_sac.getRemainingCorrespondences( *correspondences_ptr
                                              , *correspondences_3D_sample_consensus_ptr );

    // transformation
    sample_consensus_best_transformation = corr_rej_sac.getBestTransformation();

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " best transformation\n"
           << get_sample_consensus_best_transformation();
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
    }

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_sample_consensus_ptr;


    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << (int)(correspondences_3D_sample_consensus_ptr->size()) << " correspondences).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_surface_normal()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_surface_normal]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_surface_normal_ptr) correspondences_3D_surface_normal_ptr.reset();
    correspondences_3D_surface_normal_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

//    // check required clouds
//    if ( !target_cloud_keypoints_ptr )
//    {
//        if (has_logger)
//        {
//            ss.str("");
//            ss << method_info << " empty target_cloud_keypoints_ptr.";
//            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
//        }
//        return;
//    }

//    if ( !source_cloud_keypoints_ptr )
//    {
//        if (has_logger)
//        {
//            ss.str("");
//            ss << method_info << " empty source_cloud_keypoints_ptr.";
//            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
//        }
//        return;
//    }

//    // perform the job
//    pcl::registration::CorrespondenceRejectorSurfaceNormal corr_rej_sn;

//    corr_rej_sn.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
//    corr_rej_sn.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_surface_normal_ptr;


//    // done
//    if (has_logger)
//    {
//        ss.str("");
//        ss << method_info << " done successfully ("
//           << (int)(correspondences_3D_surface_normal_ptr->size()) << " correspondences).";
//        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
//    }

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done. NOTE: no result (under construction).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_trimmed()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_trimmed]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_trimmed_ptr) correspondences_3D_trimmed_ptr.reset();
    correspondences_3D_trimmed_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // perform the job
    pcl::registration::CorrespondenceRejectorTrimmed corr_rej_tr;

    corr_rej_tr.setOverlapRatio(
                config.get_correspondences_rejection_overlap_ratio() );
    corr_rej_tr.setMinCorrespondences(
                config.get_correspondences_rejection_min_correspondences() );

//    corr_rej_tr.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
//    corr_rej_tr.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

//    corr_rej_tr.setInputCorrespondences( correspondences_ptr );
//    corr_rej_tr.getCorrespondences( *correspondences_3D_trimmed_ptr );

    corr_rej_tr.getRemainingCorrespondences( *correspondences_ptr
                                             , *correspondences_3D_trimmed_ptr );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_trimmed_ptr;


    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << (int)(correspondences_3D_trimmed_ptr->size()) << " correspondences).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::reject_correspondences_var_trimmed()
{
    std::string method_info( "[RegistrationPipeline::reject_correspondences_var_trimmed]" );
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // clear containers
    if (correspondences_3D_var_trimmed_ptr) correspondences_3D_var_trimmed_ptr.reset();
    correspondences_3D_var_trimmed_ptr = pcl::CorrespondencesPtr(new pcl::Correspondences);


    // check required data
    if ( !correspondences_ptr )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty correspondences_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    // perform the job
    pcl::registration::CorrespondenceRejectorVarTrimmed corr_rej_vtr;

    corr_rej_vtr.setMaxRatio( config.get_correspondences_rejection_max_ratio() );
    corr_rej_vtr.setMinRatio( config.get_correspondences_rejection_min_ratio() );

//    corr_rej_vtr.setInputCorrespondences( correspondences_ptr );
//    corr_rej_vtr.getCorrespondences( *correspondences_3D_var_trimmed_ptr );

    corr_rej_vtr.setInputTarget<pcl::PointXYZ>( target_cloud_keypoints_ptr );
    corr_rej_vtr.setInputSource<pcl::PointXYZ>( source_cloud_keypoints_ptr );

    corr_rej_vtr.getRemainingCorrespondences( *correspondences_ptr
                                              , *correspondences_3D_var_trimmed_ptr );

    // remaining correspondences
    correspondences_remaining_ptr = correspondences_3D_var_trimmed_ptr;


    // done
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << (int)(correspondences_3D_var_trimmed_ptr->size()) << " correspondences).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_matrix_source_to_target_2D_based_on_config() const
{
    std::stringstream ss;

    cv::Mat empty_mat;

    if ( "homography" == config.get_correspondence_base() )
    {
        return get_H_source_to_target_2D();
    }
    else if  ( "fundamental_matrix" == config.get_correspondence_base() )
    {
        return get_F_source_to_target_2D();
    }
    else if  ( "essential_matrix" == config.get_correspondence_base() )
    {
        return get_E_source_to_target_2D();
    }
    else if  ( "affine" == config.get_correspondence_base() )
    {
        return get_A_source_to_target_2D();
    }
    else if  ( "perspective" == config.get_correspondence_base() )
    {
        return get_P_source_to_target_2D();
    }
    else if  ( "rigid_transform" == config.get_correspondence_base() )
    {
        return get_rigid_transformation_source_to_target_2D();
    }
    else if  ( "rigid_transform_direct" == config.get_correspondence_base() )
    {
        return get_rigid_transformation_direct_source_to_target_2D();
    }
    else // invalid
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::get_matrix_source_to_target_2D_based_on_config]"
               << " invalid config data \"" << config.get_correspondence_base() << "\"";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return empty_mat;
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_rigid_transformation_source_to_target_2D() const
{
    return rigid_transformation_source_to_target_2D;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_rigid_transformation_direct_source_to_target_2D() const
{
    return rigid_transformation_direct_source_to_target_2D;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_A_source_to_target_2D() const
{
    return A_source_to_target_2D;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_P_source_to_target_2D() const
{
    return P_source_to_target_2D;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_F_source_to_target_2D() const
{
    return F_source_to_target_2D;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_E_source_to_target_2D() const
{
    return E_source_to_target_2D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::find_transformation_2D_based_on_config()
{
    std::string method_info("[RegistrationPipeline::find_transformation_2D_based_on_config]");
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }


    if ( "homography" == config.get_correspondence_base() )
    {
        find_homography_2D();
    }
    else if  ( "fundamental_matrix" == config.get_correspondence_base() )
    {
        find_fundamental_matrix_2D();
    }
    else if  ( "essential_matrix" == config.get_correspondence_base() )
    {
        find_essential_matrix_2D();
    }
    else if  ( "affine" == config.get_correspondence_base() )
    {
        find_affine_2D();
    }
    else if  ( "perspective" == config.get_correspondence_base() )
    {
        find_perspective_2D();
    }
    else if  ( "rigid_transform" == config.get_correspondence_base() )
    {
        find_rigid_transformation_2D();
    }
    else if  ( "rigid_transform_direct" == config.get_correspondence_base() )
    {
        find_rigid_transformation_2D_direct();
    }
    else // invalid
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " invalid config data \""
               << config.get_correspondence_base() << "\"";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " ...finished successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::find_rigid_transformation_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_rigid_transformation_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    std::vector<int> queryIdxs( filtered_matches_2D.size() );
    std::vector<int> trainIdxs( filtered_matches_2D.size() );

    for ( size_t i = 0; i < filtered_matches_2D.size(); i++ )
    {
        queryIdxs[i] = filtered_matches_2D[i].queryIdx;
        trainIdxs[i] = filtered_matches_2D[i].trainIdx;
    }

    // clear the containers
    target_points_2D.clear();
    source_points_2D.clear();

    cv::KeyPoint::convert( target_keypoints_2D, target_points_2D, queryIdxs );
    cv::KeyPoint::convert( source_keypoints_2D, source_points_2D, trainIdxs );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D] full_affine "
           << (config.get_flag_full_affine()?"ENABLED.":"DISABLED.");
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    rigid_transformation_source_to_target_2D =
            cv::estimateRigidTransform(
                  source_points_2D
                , target_points_2D
                , config.get_flag_full_affine() );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D] matrix\n"
           << rigid_transformation_source_to_target_2D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::find_rigid_transformation_2D_direct()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D_direct] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_rigid_transformation_2D_direct] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_rigid_transformation_2D_direct] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    //--------------------------------------------------------------------------
    // this part is not used in this method, but for other consumer methods.
    //--------------------------------------------------------------------------
    std::vector<int> queryIdxs( filtered_matches_2D.size() );
    std::vector<int> trainIdxs( filtered_matches_2D.size() );

    for ( size_t i = 0; i < filtered_matches_2D.size(); i++ )
    {
        queryIdxs[i] = filtered_matches_2D[i].queryIdx;
        trainIdxs[i] = filtered_matches_2D[i].trainIdx;
    }

    // clear the containers
    target_points_2D.clear();
    source_points_2D.clear();

    cv::KeyPoint::convert( target_keypoints_2D, target_points_2D, queryIdxs );
    cv::KeyPoint::convert( source_keypoints_2D, source_points_2D, trainIdxs );
    //------------------------------------------------------------------------------



    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D_direct] full_affine "
           << (config.get_flag_full_affine()?"ENABLED.":"DISABLED.");
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    // find the matrix
    rigid_transformation_direct_source_to_target_2D =
            cv::estimateRigidTransform(
                  source_color
                , target_color
                , config.get_flag_full_affine() );


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D_direct] matrix\n"
           << rigid_transformation_direct_source_to_target_2D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D_direct] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::find_affine_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_affine_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_affine_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    std::vector<int> queryIdxs( filtered_matches_2D.size() );
    std::vector<int> trainIdxs( filtered_matches_2D.size() );

    for( size_t i = 0; i < filtered_matches_2D.size(); i++ )
    {
        queryIdxs[i] = filtered_matches_2D[i].queryIdx;
        trainIdxs[i] = filtered_matches_2D[i].trainIdx;
    }

    // clear the containers
    target_points_2D.clear();
    source_points_2D.clear();

    cv::KeyPoint::convert( target_keypoints_2D, target_points_2D, queryIdxs );
    cv::KeyPoint::convert( source_keypoints_2D, source_points_2D, trainIdxs );

    A_source_to_target_2D = cv::getAffineTransform(
                  cv::Mat(source_points_2D)
                , cv::Mat(target_points_2D) );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_affine_2D] matrix\n" << A_source_to_target_2D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_affine_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::find_perspective_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_perspective_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_perspective_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    std::vector<int> queryIdxs( filtered_matches_2D.size() );
    std::vector<int> trainIdxs( filtered_matches_2D.size() );

    for( size_t i = 0; i < filtered_matches_2D.size(); i++ )
    {
        queryIdxs[i] = filtered_matches_2D[i].queryIdx;
        trainIdxs[i] = filtered_matches_2D[i].trainIdx;
    }

    // clear the containers
    target_points_2D.clear();
    source_points_2D.clear();

    cv::KeyPoint::convert( target_keypoints_2D, target_points_2D, queryIdxs );
    cv::KeyPoint::convert( source_keypoints_2D, source_points_2D, trainIdxs );

    P_source_to_target_2D = cv::getPerspectiveTransform(
                  cv::Mat(source_points_2D)
                , cv::Mat(target_points_2D) );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_perspective_2D] matrix\n"
           << P_source_to_target_2D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_perspective_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::find_fundamental_matrix_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_fundamental_matrix_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    std::vector<int> queryIdxs( filtered_matches_2D.size() );
    std::vector<int> trainIdxs( filtered_matches_2D.size() );

    for( size_t i = 0; i < filtered_matches_2D.size(); i++ )
    {
        queryIdxs[i] = filtered_matches_2D[i].queryIdx;
        trainIdxs[i] = filtered_matches_2D[i].trainIdx;
    }

    // clear the containers
    target_points_2D.clear();
    source_points_2D.clear();

    cv::KeyPoint::convert( target_keypoints_2D, target_points_2D, queryIdxs );
    cv::KeyPoint::convert( source_keypoints_2D, source_points_2D, trainIdxs );

    //--------------------------------------------------------------------------
    // first, discard the outliers via RANSAC
    //--------------------------------------------------------------------------
    cv::Mat temp_status;
    F_source_to_target_2D = cv::findFundamentalMat(
                  cv::Mat(source_points_2D)
                , cv::Mat(target_points_2D)
                , CV_FM_RANSAC
                , config.get_ransac_max_distance_threshold_2D()
                , config.get_confidence_probability()
                , temp_status );

    std::vector<int> temp_status_vec = std::vector<int>( temp_status );

    // discard outliers
    std::vector<cv::Point2f> source_points_2D_only_inliers;
    std::vector<cv::Point2f> target_points_2D_only_inliers;

    int i(0); // as inliers index
    for ( std::vector<int>::iterator it = temp_status_vec.begin();
          it != temp_status_vec.end(); ++it )
    {
        if ( *it != 0 )
        {
            source_points_2D_only_inliers.push_back( source_points_2D.at(i) );
            target_points_2D_only_inliers.push_back( target_points_2D.at(i) );
        }
        i++;
    }


    //--------------------------------------------------------------------------
    // second, apply the algorithm to inliers only
    //--------------------------------------------------------------------------

    if ( config.get_fundamental_matrix_2D_method() == "RANSAC" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D] RANSAC method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        F_source_to_target_2D = cv::findFundamentalMat(
                      cv::Mat(source_points_2D_only_inliers)
                    , cv::Mat(target_points_2D_only_inliers)
                    , CV_FM_RANSAC
                    , config.get_ransac_max_distance_threshold_2D()
                    , config.get_confidence_probability() );
    }
    else if ( config.get_fundamental_matrix_2D_method() == "LMEDS" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D] LMEDS method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        F_source_to_target_2D = cv::findFundamentalMat(
                      cv::Mat(source_points_2D_only_inliers)
                    , cv::Mat(target_points_2D_only_inliers)
                    , CV_FM_LMEDS
                    , config.get_ransac_max_distance_threshold_2D()
                    , config.get_confidence_probability() );
    }
    else if ( config.get_fundamental_matrix_2D_method() == "7POINT" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D] 7POINT method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        F_source_to_target_2D = cv::findFundamentalMat(
                      cv::Mat(source_points_2D_only_inliers)
                    , cv::Mat(target_points_2D_only_inliers)
                    , CV_FM_7POINT
                    , config.get_ransac_max_distance_threshold_2D()
                    , config.get_confidence_probability() );
    }
    else if ( config.get_fundamental_matrix_2D_method() == "8POINT" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D] 8POINT method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        F_source_to_target_2D = cv::findFundamentalMat(
                      cv::Mat(source_points_2D_only_inliers)
                    , cv::Mat(target_points_2D_only_inliers)
                    , CV_FM_8POINT
                    , config.get_ransac_max_distance_threshold_2D()
                    , config.get_confidence_probability() );
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D] Unknown method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_fundamental_matrix_2D] matrix\n" << F_source_to_target_2D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_fundamental_matrix_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::find_essential_matrix_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_essential_matrix_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_essential_matrix_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    // find F
    // find K
    // return E = Kt . F . K

    // F
    find_fundamental_matrix_2D();
    cv::Mat F = get_F_source_to_target_2D();

    // K
    cv::Mat K = (cv::Mat_<double>(3,3)
                 << config.get_target_intrinsics_color().fx, 0, config.get_target_intrinsics_color().cx
                 , 0, config.get_target_intrinsics_color().fy, config.get_target_intrinsics_color().cy
                 , 0, 0, 1);

    // Kt (K transposed)
    cv::Mat Kt = K.t();

    // E
    cv::Mat E = Kt * F * K;
    E.copyTo( E_source_to_target_2D );


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_essential_matrix_2D] matrix\n" << E_source_to_target_2D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_essential_matrix_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::find_rigid_transformation_2D_standalone(
          const std::vector<cv::DMatch> &matched_
        , const std::vector<cv::KeyPoint> &tkeypoints
        , const std::vector<cv::KeyPoint> &skeypoints
        , std::vector<cv::Point2f> &tpoints
        , std::vector<cv::Point2f> &spoints ) const
{
    std::stringstream ss;

    cv::Mat rigid_matrix;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D_standalone] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (matched_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_rigid_transformation_2D_standalone] empty matched_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return rigid_matrix;
    }

    std::vector<int> queryIdxs( matched_.size() );
    std::vector<int> trainIdxs( matched_.size() );

    for( size_t i = 0; i < matched_.size(); i++ )
    {
        queryIdxs[i] = matched_[i].queryIdx;
        trainIdxs[i] = matched_[i].trainIdx;
    }

    cv::KeyPoint::convert( tkeypoints, tpoints, queryIdxs );
    cv::KeyPoint::convert( skeypoints, spoints, trainIdxs );


    rigid_matrix =
            cv::estimateRigidTransform(
                  cv::Mat(spoints)
                , cv::Mat(tpoints)
                , config.get_flag_full_affine() );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D_standalone] matrix\n"
           << rigid_matrix;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_rigid_transformation_2D_standalone] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return rigid_matrix;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::find_affine_2D_standalone(
          const std::vector<cv::DMatch> &matched_
        , const std::vector<cv::KeyPoint> &tkeypoints
        , const std::vector<cv::KeyPoint> &skeypoints
        , std::vector<cv::Point2f> &tpoints
        , std::vector<cv::Point2f> &spoints ) const
{
    std::stringstream ss;

    cv::Mat affine_matrix;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_affine_2D_standalone] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (matched_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_affine_2D_standalone] empty matched_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return affine_matrix;
    }

    std::vector<int> queryIdxs( matched_.size() );
    std::vector<int> trainIdxs( matched_.size() );

    for( size_t i = 0; i < matched_.size(); i++ )
    {
        queryIdxs[i] = matched_[i].queryIdx;
        trainIdxs[i] = matched_[i].trainIdx;
    }

    cv::KeyPoint::convert( tkeypoints, tpoints, queryIdxs );
    cv::KeyPoint::convert( skeypoints, spoints, trainIdxs );


    affine_matrix = cv::getAffineTransform(
                  cv::Mat(spoints)
                , cv::Mat(tpoints) );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_affine_2D_standalone] matrix\n"
           << affine_matrix;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_affine_2D_standalone] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return affine_matrix;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::find_perspective_2D_standalone(
          const std::vector<cv::DMatch> &matched_
        , const std::vector<cv::KeyPoint> &tkeypoints
        , const std::vector<cv::KeyPoint> &skeypoints
        , std::vector<cv::Point2f> &tpoints
        , std::vector<cv::Point2f> &spoints ) const
{
    std::stringstream ss;

    cv::Mat perspective_matrix;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_perspective_2D_standalone] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (matched_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_perspective_2D_standalone] empty matched_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return perspective_matrix;
    }

    std::vector<int> queryIdxs( matched_.size() );
    std::vector<int> trainIdxs( matched_.size() );

    for( size_t i = 0; i < matched_.size(); i++ )
    {
        queryIdxs[i] = matched_[i].queryIdx;
        trainIdxs[i] = matched_[i].trainIdx;
    }

    cv::KeyPoint::convert( tkeypoints, tpoints, queryIdxs );
    cv::KeyPoint::convert( skeypoints, spoints, trainIdxs );


    perspective_matrix = cv::getPerspectiveTransform(
                  cv::Mat(spoints)
                , cv::Mat(tpoints) );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_perspective_2D_standalone] matrix\n"
           << perspective_matrix;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_perspective_2D_standalone] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return perspective_matrix;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::find_fundamental_matrix_2D_standalone(
          const std::vector<cv::DMatch> &matched_
        , const std::vector<cv::KeyPoint> &tkeypoints
        , const std::vector<cv::KeyPoint> &skeypoints
        , std::vector<cv::Point2f> &tpoints
        , std::vector<cv::Point2f> &spoints ) const
{
    std::stringstream ss;

    cv::Mat fundamental_matrix;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (matched_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] empty matched_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return fundamental_matrix;
    }

    std::vector<int> queryIdxs( matched_.size() );
    std::vector<int> trainIdxs( matched_.size() );

    for( size_t i = 0; i < matched_.size(); i++ )
    {
        queryIdxs[i] = matched_[i].queryIdx;
        trainIdxs[i] = matched_[i].trainIdx;
    }

    cv::KeyPoint::convert( tkeypoints, tpoints, queryIdxs );
    cv::KeyPoint::convert( skeypoints, spoints, trainIdxs );

    if ( config.get_fundamental_matrix_2D_method() == "RANSAC" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] RANSAC method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        fundamental_matrix = cv::findFundamentalMat(
                      cv::Mat(spoints)
                    , cv::Mat(tpoints)
                    , CV_FM_RANSAC
                    , config.get_ransac_max_distance_threshold_2D()
                    , config.get_confidence_probability() );
    }
    else if ( config.get_fundamental_matrix_2D_method() == "LMEDS" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] LMEDS method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        fundamental_matrix = cv::findFundamentalMat(
                      cv::Mat(spoints)
                    , cv::Mat(tpoints)
                    , CV_FM_LMEDS
                    , config.get_ransac_max_distance_threshold_2D()
                    , config.get_confidence_probability() );
    }
    else if ( config.get_fundamental_matrix_2D_method() == "7POINT" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] LMEDS method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        fundamental_matrix = cv::findFundamentalMat(
                      cv::Mat(spoints)
                    , cv::Mat(tpoints)
                    , CV_FM_7POINT
                    , config.get_ransac_max_distance_threshold_2D()
                    , config.get_confidence_probability() );
    }
    else if ( config.get_fundamental_matrix_2D_method() == "8POINT" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] LMEDS method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        fundamental_matrix = cv::findFundamentalMat(
                      cv::Mat(spoints)
                    , cv::Mat(tpoints)
                    , CV_FM_8POINT
                    , config.get_ransac_max_distance_threshold_2D()
                    , config.get_confidence_probability() );
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] Unknown method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return fundamental_matrix;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] matrix\n" << fundamental_matrix;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_fundamental_matrix_2D_standalone] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return fundamental_matrix;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::find_essential_matrix_2D_standalone(
          const std::vector<cv::DMatch> &matched_
        , const std::vector<cv::KeyPoint> &tkeypoints
        , const std::vector<cv::KeyPoint> &skeypoints
        , std::vector<cv::Point2f> &tpoints
        , std::vector<cv::Point2f> &spoints ) const
{
    std::stringstream ss;

    cv::Mat essential_matrix;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_essential_matrix_2D_standalone] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (matched_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_essential_matrix_2D_standalone] empty matched_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return essential_matrix;
    }


    // find F
    // find K
    // return E = Kt . F . K

    // F
    cv::Mat F = find_fundamental_matrix_2D_standalone(
                matched_
              , tkeypoints
              , skeypoints
              , tpoints
              , spoints );

    // K
    cv::Mat K = (cv::Mat_<float>(3,3)
                 << config.get_target_intrinsics_color().fx, 0, config.get_target_intrinsics_color().cx
                 , 0, config.get_target_intrinsics_color().fy, config.get_target_intrinsics_color().cy
                 , 0, 0, 1);
    // E
    essential_matrix = K.t() * F * K;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_essential_matrix_2D_standalone] matrix\n" << essential_matrix;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_essential_matrix_2D_standalone] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return essential_matrix;
}


//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::get_H_source_to_target_2D() const
{
    return H_source_to_target_2D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::find_homography_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_homography_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_homography_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    std::vector<int> queryIdxs( filtered_matches_2D.size() );
    std::vector<int> trainIdxs( filtered_matches_2D.size() );

    for( size_t i = 0; i < filtered_matches_2D.size(); i++ )
    {
        queryIdxs[i] = filtered_matches_2D[i].queryIdx;
        trainIdxs[i] = filtered_matches_2D[i].trainIdx;
    }

    // clear the containers
    target_points_2D.clear();
    source_points_2D.clear();

    cv::KeyPoint::convert( target_keypoints_2D, target_points_2D, queryIdxs );
    cv::KeyPoint::convert( source_keypoints_2D, source_points_2D, trainIdxs );

    if ( config.get_homography_2D_method()=="RANSAC" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_homography_2D] RANSAC method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        H_source_to_target_2D = cv::findHomography(
                    cv::Mat(source_points_2D)
                  , cv::Mat(target_points_2D), CV_RANSAC, config.get_ransac_reprojection_threshold_2D() );
    }
    else if ( config.get_homography_2D_method()=="LMEDS" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_homography_2D] LMEDS method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        H_source_to_target_2D = cv::findHomography(
                    cv::Mat(source_points_2D)
                  , cv::Mat(target_points_2D), CV_LMEDS, config.get_ransac_reprojection_threshold_2D() );
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_homography_2D] Unknown method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_homography_2D] matrix\n" << H_source_to_target_2D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_homography_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::find_homography_2D_standalone(
                                const std::vector<cv::DMatch> &matched_
                                , const std::vector<cv::KeyPoint> &tkeypoints
                                , const std::vector<cv::KeyPoint> &skeypoints
                                , std::vector<cv::Point2f> &tpoints
                                , std::vector<cv::Point2f> &spoints ) const
{
    std::stringstream ss;

    cv::Mat homo_matrix;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_homography_2D_standalone] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (matched_.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_homography_2D_standalone] empty matched_.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return homo_matrix;
    }

    std::vector<int> queryIdxs( matched_.size() );
    std::vector<int> trainIdxs( matched_.size() );

    for( size_t i = 0; i < matched_.size(); i++ )
    {
        queryIdxs[i] = matched_[i].queryIdx;
        trainIdxs[i] = matched_[i].trainIdx;
    }

    cv::KeyPoint::convert( tkeypoints, tpoints, queryIdxs );
    cv::KeyPoint::convert( skeypoints, spoints, trainIdxs );

    if ( config.get_homography_2D_method()=="RANSAC" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_homography_2D_standalone] RANSAC method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        homo_matrix = cv::findHomography(
                    cv::Mat(spoints)
                  , cv::Mat(tpoints)
                  , CV_RANSAC
                  , config.get_ransac_reprojection_threshold_2D() );
    }
    else if ( config.get_homography_2D_method()=="LMEDS" )
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_homography_2D_standalone] LMEDS method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        homo_matrix = cv::findHomography(
                    cv::Mat(spoints)
                  , cv::Mat(tpoints)
                  , CV_LMEDS
                  , config.get_ransac_reprojection_threshold_2D() );
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::find_homography_2D_standalone] Unknown method.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return homo_matrix;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::find_homography_2D] matrix\n" << homo_matrix;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::find_homography_2D_standalone] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return homo_matrix;
}

//==============================================================================
//==============================================================================

std::vector<char>
RegistrationPipeline::get_inliers_mask_2D_based_on_config() const
{
    std::stringstream ss;

    std::vector<char> empty_vec;

    if ( "homography" == config.get_correspondence_base() )
    {
        return get_homography_based_inliers_mask_2D();
    }
    else if  ( "fundamental_matrix" == config.get_correspondence_base() )
    {
        return get_fundamental_matrix_based_inliers_mask_2D();
    }
    else if  ( "essential_matrix" == config.get_correspondence_base() )
    {
        return get_essential_matrix_based_inliers_mask_2D();
    }
    else if  ( "affine" == config.get_correspondence_base() )
    {
        return get_affine_based_inliers_mask_2D();
    }
    else if  ( "perspective" == config.get_correspondence_base() )
    {
        return get_perspective_based_inliers_mask_2D();
    }
    else if  ( "rigid_transform" == config.get_correspondence_base() )
    {
        return get_rigid_transformation_based_inliers_mask_2D();
    }
    else if  ( "rigid_transform_direct" == config.get_correspondence_base() )
    {
        return get_rigid_transformation_direct_based_inliers_mask_2D();
    }
    else // invalid
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::get_inliers_mask_2D_based_on_config]"
               << " invalid config data \"" << config.get_correspondence_base() << "\"";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return empty_vec;
    }
}

//==============================================================================
//==============================================================================

std::vector<char>
RegistrationPipeline::get_rigid_transformation_based_inliers_mask_2D() const
{
    return rigid_transformation_based_inliers_mask_2D;
}

//==============================================================================
//==============================================================================

std::vector<char>
RegistrationPipeline::get_rigid_transformation_direct_based_inliers_mask_2D() const
{
    return rigid_transformation_direct_based_inliers_mask_2D;
}

//==============================================================================
//==============================================================================

std::vector<char>
RegistrationPipeline::get_affine_based_inliers_mask_2D() const
{
    return affine_based_inliers_mask_2D;
}

//==============================================================================
//==============================================================================

std::vector<char>
RegistrationPipeline::get_perspective_based_inliers_mask_2D() const
{
    return perspective_based_inliers_mask_2D;
}

//==============================================================================
//==============================================================================

std::vector<char>
RegistrationPipeline::get_fundamental_matrix_based_inliers_mask_2D() const
{
    return fundamental_matrix_based_inliers_mask_2D;
}

//==============================================================================
//==============================================================================

std::vector<char>
RegistrationPipeline::get_essential_matrix_based_inliers_mask_2D() const
{
    return essential_matrix_based_inliers_mask_2D;
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::get_number_of_inliers_2D_based_on_config() const
{
    std::stringstream ss;

    if ( "homography" == config.get_correspondence_base() )
    {
        return get_number_of_homography_based_inliers_2D();
    }
    else if  ( "fundamental_matrix" == config.get_correspondence_base() )
    {
        return get_number_of_fundamental_matrix_based_inliers_2D();
    }
    else if  ( "essential_matrix" == config.get_correspondence_base() )
    {
        return get_number_of_essential_matrix_based_inliers_2D();
    }
    else if  ( "affine" == config.get_correspondence_base() )
    {
        return get_number_of_affine_based_inliers_2D();
    }
    else if  ( "perspective" == config.get_correspondence_base() )
    {
        return get_number_of_perspective_based_inliers_2D();
    }
    else if  ( "rigid_transform" == config.get_correspondence_base() )
    {
        return get_number_of_rigid_transformation_based_inliers_2D();
    }
    else if  ( "rigid_transform_direct" == config.get_correspondence_base() )
    {
        return get_number_of_rigid_transformation_direct_based_inliers_2D();
    }
    else // invalid
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::get_number_of_inliers_2D_based_on_config]"
               << " invalid config data \"" << config.get_correspondence_base() << "\"";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return -1;
    }
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::get_number_of_rigid_transformation_based_inliers_2D() const
{
    return number_of_rigid_transformation_based_inliers_2D;
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::get_number_of_rigid_transformation_direct_based_inliers_2D() const
{
    return number_of_rigid_transformation_direct_based_inliers_2D;
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::get_number_of_affine_based_inliers_2D() const
{
    return number_of_affine_based_inliers_2D;
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::get_number_of_perspective_based_inliers_2D() const
{
    return number_of_perspective_based_inliers_2D;
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::get_number_of_fundamental_matrix_based_inliers_2D() const
{
    return number_of_fundamental_matrix_based_inliers_2D;
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::get_number_of_essential_matrix_based_inliers_2D() const
{
    return number_of_essential_matrix_based_inliers_2D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_inliers_mask_2D_based_on_config()
{
    std::stringstream ss;

    if ( "homography" == config.get_correspondence_base() )
    {
        generate_homography_based_inliers_mask_2D();
    }
    else if  ( "fundamental_matrix" == config.get_correspondence_base() )
    {
        generate_fundamental_matrix_based_inliers_mask_2D();
    }
    else if  ( "essential_matrix" == config.get_correspondence_base() )
    {
        generate_essential_matrix_based_inliers_mask_2D();
    }
    else if  ( "affine" == config.get_correspondence_base() )
    {
        generate_affine_based_inliers_mask_2D();
    }
    else if  ( "perspective" == config.get_correspondence_base() )
    {
        generate_perspective_based_inliers_mask_2D();
    }
    else if  ( "rigid_transform" == config.get_correspondence_base() )
    {
        generate_rigid_transformation_based_inliers_mask_2D();
    }
    else if  ( "rigid_transform_direct" == config.get_correspondence_base() )
    {
        generate_rigid_transformation_direct_based_inliers_mask_2D();
    }
    else // invalid
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_inliers_mask_2D_based_on_config]"
               << " invalid config data \"" << config.get_correspondence_base() << "\"";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_rigid_transformation_based_inliers_mask_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_rigid_transformation_based_inliers_mask_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (rigid_transformation_source_to_target_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_rigid_transformation_based_inliers_mask_2D] empty rigid_transformation_source_to_target_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_rigid_transformation_based_inliers_mask_2D] "
           << " inliers finding method: \"" << config.get_inliers_finding_method() << "\".";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //..........................................................................
    int num_inliers = 0;
    number_of_rigid_transformation_based_inliers_2D = 0;
    //..........................................................................
    if ( config.get_inliers_finding_method() == "all_as_inliers" )
    {
        rigid_transformation_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 1 );
        num_inliers = rigid_transformation_based_inliers_mask_2D.size();
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_to_target" )
    {
        rigid_transformation_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::transform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , rigid_transformation_source_to_target_2D );

        for( size_t i = 0; i < source_points_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                rigid_transformation_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "target_to_source"
              || config.get_inliers_finding_method() == "source_AND_target"
              || config.get_inliers_finding_method() == "source_OR_target"
            )
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_rigid_transformation_based_inliers_mask_2D] "
           << " this inliers finding method (\"" << config.get_inliers_finding_method()
           << "\") is not applicable for the rigid_transformation based transformaion.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
    }
    //..........................................................................
    else
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_rigid_transformation_based_inliers_mask_2D] "
           << " INVALID inliers finding method (\"" << config.get_inliers_finding_method() << "\").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        return;
    }

    //..........................................................................
    number_of_rigid_transformation_based_inliers_2D = num_inliers;
    //..........................................................................

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_rigid_transformation_based_inliers_mask_2D] done successfully ("
           << number_of_rigid_transformation_based_inliers_2D << " inliers).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_rigid_transformation_direct_based_inliers_mask_2D()
{
    std::stringstream ss;

    std::string method_info(
      "[RegistrationPipeline::generate_rigid_transformation_direct_based_inliers_mask_2D]");

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (rigid_transformation_direct_source_to_target_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty rigid_transformation_direct_source_to_target_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    if (has_logger)
    {
        ss.str("");
        ss << method_info << " inliers finding method: \""
           << config.get_inliers_finding_method() << "\".";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //..........................................................................
    int num_inliers = 0;
    number_of_rigid_transformation_direct_based_inliers_2D = 0;
    //..........................................................................
    if ( config.get_inliers_finding_method() == "all_as_inliers" )
    {
        rigid_transformation_direct_based_inliers_mask_2D
                = std::vector<char>( filtered_matches_2D.size(), 1 );
        num_inliers = rigid_transformation_direct_based_inliers_mask_2D.size();
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_to_target" )
    {
        rigid_transformation_direct_based_inliers_mask_2D
                = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::transform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , rigid_transformation_direct_source_to_target_2D );

        for( size_t i = 0; i < source_points_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                rigid_transformation_direct_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "target_to_source"
              || config.get_inliers_finding_method() == "source_AND_target"
              || config.get_inliers_finding_method() == "source_OR_target"
            )
    {
        ss.str("");
        ss << method_info
           << " this inliers finding method (\"" << config.get_inliers_finding_method()
           << "\") is not applicable for the rigid_transformation direct based transformaion.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
    }
    //..........................................................................
    else
    {
        ss.str("");
        ss << method_info
           << " INVALID inliers finding method (\""
           << config.get_inliers_finding_method() << "\").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        return;
    }

    //..........................................................................
    number_of_rigid_transformation_direct_based_inliers_2D = num_inliers;
    //..........................................................................

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully ("
           << number_of_rigid_transformation_direct_based_inliers_2D << " inliers).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_affine_based_inliers_mask_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_affine_based_inliers_mask_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (A_source_to_target_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_affine_based_inliers_mask_2D] empty A_source_to_target_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_affine_based_inliers_mask_2D] "
           << " inliers finding method: \"" << config.get_inliers_finding_method() << "\".";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //..........................................................................
    int num_inliers = 0;
    number_of_affine_based_inliers_2D = 0;
    //..........................................................................
    if ( config.get_inliers_finding_method() == "all_as_inliers" )
    {
        affine_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 1 );
        num_inliers = affine_based_inliers_mask_2D.size();
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_to_target" )
    {
        affine_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::transform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , A_source_to_target_2D );

        for( size_t i = 0; i < source_points_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                affine_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "target_to_source"
              || config.get_inliers_finding_method() == "source_AND_target"
              || config.get_inliers_finding_method() == "source_OR_target"
            )
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_affine_based_inliers_mask_2D] "
           << " this inliers finding method (\"" << config.get_inliers_finding_method()
           << "\") is not applicable for the affine based transformaion.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
    }
    //..........................................................................
    else
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_affine_based_inliers_mask_2D] "
           << " INVALID inliers finding method (\"" << config.get_inliers_finding_method() << "\").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        return;
    }

    //..........................................................................
    number_of_affine_based_inliers_2D = num_inliers;
    //..........................................................................

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_affine_based_inliers_mask_2D] done successfully ("
           << number_of_affine_based_inliers_2D << " inliers).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_perspective_based_inliers_mask_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_perspective_based_inliers_mask_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (P_source_to_target_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_perspective_based_inliers_mask_2D] empty P_source_to_target_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_perspective_based_inliers_mask_2D] "
           << " inliers finding method: \"" << config.get_inliers_finding_method() << "\".";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //..........................................................................
    int num_inliers = 0;
    number_of_perspective_based_inliers_2D = 0;
    //..........................................................................
    if ( config.get_inliers_finding_method() == "all_as_inliers" )
    {
        perspective_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 1 );
        num_inliers = perspective_based_inliers_mask_2D.size();
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_to_target" )
    {
        perspective_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , P_source_to_target_2D );

        for( size_t i = 0; i < source_points_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                perspective_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "target_to_source" )
    {
        perspective_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , P_source_to_target_2D.inv() );

        for( size_t i = 0; i < target_points_2D.size(); i++ )
        {
            if( cv::norm( source_points_2D[i]
                          - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                perspective_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_AND_target" )
    {
        perspective_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , P_source_to_target_2D );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , P_source_to_target_2D.inv() );

        for( size_t i = 0; i < perspective_based_inliers_mask_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold()
                && // AND
                cv::norm( source_points_2D[i]
                      - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                <= config.get_inliers_max_threshold() ) // inlier
            {
                perspective_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_OR_target" )
    {
        perspective_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , P_source_to_target_2D );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , P_source_to_target_2D.inv() );

        for( size_t i = 0; i < perspective_based_inliers_mask_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold()
                || // OR
                cv::norm( source_points_2D[i]
                      - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                <= config.get_inliers_max_threshold() ) // inlier
            {
                perspective_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_perspective_based_inliers_mask_2D] "
           << " INVALID inliers finding method (\"" << config.get_inliers_finding_method() << "\").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        return;
    }


    //..........................................................................
    number_of_perspective_based_inliers_2D = num_inliers;
    //..........................................................................

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_perspective_based_inliers_mask_2D] done successfully ("
           << number_of_perspective_based_inliers_2D << " inliers).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_fundamental_matrix_based_inliers_mask_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_fundamental_matrix_based_inliers_mask_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (F_source_to_target_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_fundamental_matrix_based_inliers_mask_2D] empty F_source_to_target_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_fundamental_matrix_based_inliers_mask_2D] "
           << " inliers finding method: \"" << config.get_inliers_finding_method() << "\".";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //..........................................................................
    int num_inliers = 0;
    number_of_fundamental_matrix_based_inliers_2D = 0;
    //..........................................................................
    if ( config.get_inliers_finding_method() == "all_as_inliers" )
    {
        fundamental_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 1 );
        num_inliers = fundamental_matrix_based_inliers_mask_2D.size();
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_to_target" )
    {
        fundamental_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , F_source_to_target_2D );

        for( size_t i = 0; i < source_points_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                fundamental_matrix_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "target_to_source" )
    {
        fundamental_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , F_source_to_target_2D.inv() );

        for( size_t i = 0; i < target_points_2D.size(); i++ )
        {
            if( cv::norm( source_points_2D[i]
                          - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                fundamental_matrix_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_AND_target" )
    {
        fundamental_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , F_source_to_target_2D );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , F_source_to_target_2D.inv() );

        for( size_t i = 0; i < fundamental_matrix_based_inliers_mask_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold()
                && // AND
                cv::norm( source_points_2D[i]
                      - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                <= config.get_inliers_max_threshold() ) // inlier
            {
                fundamental_matrix_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_OR_target" )
    {
        fundamental_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , F_source_to_target_2D );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , F_source_to_target_2D.inv() );

        for( size_t i = 0; i < fundamental_matrix_based_inliers_mask_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold()
                || // OR
                cv::norm( source_points_2D[i]
                      - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                <= config.get_inliers_max_threshold() ) // inlier
            {
                fundamental_matrix_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_fundamental_matrix_based_inliers_mask_2D] "
           << " INVALID inliers finding method (\"" << config.get_inliers_finding_method() << "\").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        return;
    }


    //..........................................................................
    number_of_fundamental_matrix_based_inliers_2D = num_inliers;
    //..........................................................................

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_fundamental_matrix_based_inliers_mask_2D] done successfully ("
           << number_of_fundamental_matrix_based_inliers_2D << " inliers).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_essential_matrix_based_inliers_mask_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_essential_matrix_based_inliers_mask_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (E_source_to_target_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_essential_matrix_based_inliers_mask_2D] empty E_source_to_target_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_essential_matrix_based_inliers_mask_2D] "
           << " inliers finding method: \"" << config.get_inliers_finding_method() << "\".";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //..........................................................................
    int num_inliers = 0;
    number_of_essential_matrix_based_inliers_2D = 0;
    //..........................................................................
    if ( config.get_inliers_finding_method() == "all_as_inliers" )
    {
        essential_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 1 );
        num_inliers = essential_matrix_based_inliers_mask_2D.size();
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_to_target" )
    {
        essential_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , E_source_to_target_2D );

        for( size_t i = 0; i < source_points_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                essential_matrix_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "target_to_source" )
    {
        essential_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , E_source_to_target_2D.inv() );

        for( size_t i = 0; i < target_points_2D.size(); i++ )
        {
            if( cv::norm( source_points_2D[i]
                          - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                essential_matrix_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_AND_target" )
    {
        essential_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , E_source_to_target_2D );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , E_source_to_target_2D.inv() );

        for( size_t i = 0; i < essential_matrix_based_inliers_mask_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold()
                && // AND
                cv::norm( source_points_2D[i]
                      - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                <= config.get_inliers_max_threshold() ) // inlier
            {
                essential_matrix_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_OR_target" )
    {
        essential_matrix_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , E_source_to_target_2D );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , E_source_to_target_2D.inv() );

        for( size_t i = 0; i < essential_matrix_based_inliers_mask_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold()
                || // OR
                cv::norm( source_points_2D[i]
                      - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                <= config.get_inliers_max_threshold() ) // inlier
            {
                essential_matrix_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_essential_matrix_based_inliers_mask_2D] "
           << " INVALID inliers finding method (\"" << config.get_inliers_finding_method() << "\").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        return;
    }


    //..........................................................................
    number_of_essential_matrix_based_inliers_2D = num_inliers;
    //..........................................................................

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_essential_matrix_based_inliers_mask_2D] done successfully ("
           << number_of_essential_matrix_based_inliers_2D << " inliers).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_inliers_2D_based_on_config(
        bool flag_rich_keypoints ) const
{
    std::stringstream ss;

    cv::Mat empty_mat;

    if ( "homography" == config.get_correspondence_base() )
    {
        return draw_homography_based_inliers_2D( flag_rich_keypoints );
    }
    else if  ( "fundamental_matrix" == config.get_correspondence_base() )
    {
        return draw_fundamental_matrix_based_inliers_2D( flag_rich_keypoints );
    }
    else if  ( "essential_matrix" == config.get_correspondence_base() )
    {
        return draw_essential_matrix_based_inliers_2D( flag_rich_keypoints );
    }
    else if  ( "affine" == config.get_correspondence_base() )
    {
        return draw_affine_based_inliers_2D( flag_rich_keypoints );
    }
    else if  ( "perspective" == config.get_correspondence_base() )
    {
        return draw_perspective_based_inliers_2D( flag_rich_keypoints );
    }
    else if  ( "rigid_transform" == config.get_correspondence_base() )
    {
        return draw_rigid_transformation_based_inliers_2D( flag_rich_keypoints );
    }
    else if  ( "rigid_transform_direct" == config.get_correspondence_base() )
    {
        return draw_rigid_transformation_direct_based_inliers_2D( flag_rich_keypoints );
    }
    else // invalid
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_inliers_2D_based_on_config]"
               << " invalid config data \"" << config.get_correspondence_base() << "\"";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return empty_mat;
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_rigid_transformation_based_inliers_2D(
        bool flag_rich_keypoints ) const
{
    std::stringstream ss;

    cv::Mat result_image;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_rigid_transformation_based_inliers_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_rigid_transformation_based_inliers_2D] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_rigid_transformation_based_inliers_2D] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_rigid_transformation_based_inliers_2D] empty target_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_rigid_transformation_based_inliers_2D] empty source_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_rigid_transformation_based_inliers_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }



    if ( flag_rich_keypoints )
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , rigid_transformation_based_inliers_mask_2D
                         , cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS
        );
    }
    else
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , rigid_transformation_based_inliers_mask_2D
        );
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_rigid_transformation_based_inliers_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return result_image;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_rigid_transformation_direct_based_inliers_2D(
        bool flag_rich_keypoints ) const
{
    std::stringstream ss;

    std::string method_info(
                "[RegistrationPipeline::draw_rigid_transformation_direct_based_inliers_2D]" );
    cv::Mat result_image;

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info<<" empty target_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty source_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }



    if ( flag_rich_keypoints )
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , rigid_transformation_direct_based_inliers_mask_2D
                         , cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS
        );
    }
    else
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , rigid_transformation_direct_based_inliers_mask_2D
        );
    }

    if (has_logger)
    {
        ss.str("");
        ss << method_info << " done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return result_image;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_affine_based_inliers_2D( bool flag_rich_keypoints ) const
{
    std::stringstream ss;

    cv::Mat result_image;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_affine_based_inliers_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_affine_based_inliers_2D] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_affine_based_inliers_2D] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_affine_based_inliers_2D] empty target_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_affine_based_inliers_2D] empty source_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_affine_based_inliers_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }



    if ( flag_rich_keypoints )
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , affine_based_inliers_mask_2D
                         , cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS
        );
    }
    else
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , affine_based_inliers_mask_2D
        );
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_affine_based_inliers_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return result_image;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_perspective_based_inliers_2D( bool flag_rich_keypoints ) const
{
    std::stringstream ss;

    cv::Mat result_image;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_perspective_based_inliers_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_perspective_based_inliers_2D] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_perspective_based_inliers_2D] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_perspective_based_inliers_2D] empty target_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_perspective_based_inliers_2D] empty source_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_perspective_based_inliers_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }



    if ( flag_rich_keypoints )
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , perspective_based_inliers_mask_2D
                         , cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS
        );
    }
    else
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , perspective_based_inliers_mask_2D
        );
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_perspective_based_inliers_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return result_image;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_fundamental_matrix_based_inliers_2D( bool flag_rich_keypoints ) const
{
    std::stringstream ss;

    cv::Mat result_image;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_fundamental_matrix_based_inliers_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_fundamental_matrix_based_inliers_2D] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_fundamental_matrix_based_inliers_2D] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_fundamental_matrix_based_inliers_2D] empty target_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_fundamental_matrix_based_inliers_2D] empty source_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_fundamental_matrix_based_inliers_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }



    if ( flag_rich_keypoints )
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , fundamental_matrix_based_inliers_mask_2D
                         , cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS
        );
    }
    else
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , fundamental_matrix_based_inliers_mask_2D
        );
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_fundamental_matrix_based_inliers_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return result_image;
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_essential_matrix_based_inliers_2D( bool flag_rich_keypoints ) const
{
    std::stringstream ss;

    cv::Mat result_image;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_essential_matrix_based_inliers_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_essential_matrix_based_inliers_2D] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_essential_matrix_based_inliers_2D] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_essential_matrix_based_inliers_2D] empty target_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_essential_matrix_based_inliers_2D] empty source_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_essential_matrix_based_inliers_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }



    if ( flag_rich_keypoints )
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , essential_matrix_based_inliers_mask_2D
                         , cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS
        );
    }
    else
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , essential_matrix_based_inliers_mask_2D
        );
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_essential_matrix_based_inliers_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return result_image;
}

//==============================================================================
//==============================================================================

std::vector<char>
RegistrationPipeline::get_homography_based_inliers_mask_2D() const
{
    return homography_based_inliers_mask_2D;
}

//==============================================================================
//==============================================================================

int
RegistrationPipeline::get_number_of_homography_based_inliers_2D() const
{
    return number_of_homography_based_inliers_2D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_homography_based_inliers_mask_2D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_homography_based_inliers_mask_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (H_source_to_target_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_homography_based_inliers_mask_2D] empty H_source_to_target_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_homography_based_inliers_mask_2D] "
           << " inliers finding method: \"" << config.get_inliers_finding_method() << "\".";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    //..........................................................................
    int num_inliers = 0;
    number_of_homography_based_inliers_2D = 0;
    //..........................................................................
    if ( config.get_inliers_finding_method() == "all_as_inliers" )
    {
        homography_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 1 );
        num_inliers = homography_based_inliers_mask_2D.size();
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_to_target" )
    {
        homography_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , H_source_to_target_2D );

        for( size_t i = 0; i < source_points_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                homography_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "target_to_source" )
    {
        homography_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , H_source_to_target_2D.inv() );

        for( size_t i = 0; i < target_points_2D.size(); i++ )
        {
            if( cv::norm( source_points_2D[i]
                          - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold() ) // inlier
            {
                homography_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_AND_target" )
    {
        homography_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , H_source_to_target_2D );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , H_source_to_target_2D.inv() );

        for( size_t i = 0; i < homography_based_inliers_mask_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold()
                && // AND
                cv::norm( source_points_2D[i]
                      - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                <= config.get_inliers_max_threshold() ) // inlier
            {
                homography_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else if ( config.get_inliers_finding_method() == "source_OR_target" )
    {
        homography_based_inliers_mask_2D = std::vector<char>( filtered_matches_2D.size(), 0 );

        cv::Mat points_transformed_s2t;
        cv::perspectiveTransform( cv::Mat(source_points_2D)
                                  , points_transformed_s2t
                                  , H_source_to_target_2D );

        cv::Mat points_transformed_t2s;
        cv::perspectiveTransform( cv::Mat(target_points_2D)
                                  , points_transformed_t2s
                                  , H_source_to_target_2D.inv() );

        for( size_t i = 0; i < homography_based_inliers_mask_2D.size(); i++ )
        {
            if( cv::norm( target_points_2D[i]
                          - points_transformed_s2t.at<cv::Point2f>((int)i,0))
                    <= config.get_inliers_max_threshold()
                || // OR
                cv::norm( source_points_2D[i]
                      - points_transformed_t2s.at<cv::Point2f>((int)i,0))
                <= config.get_inliers_max_threshold() ) // inlier
            {
                homography_based_inliers_mask_2D[i] = 1;
                num_inliers++;
            }
        }
    }
    //..........................................................................
    else
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_homography_based_inliers_mask_2D] "
           << " INVALID inliers finding method (\"" << config.get_inliers_finding_method() << "\").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        return;
    }


    //..........................................................................
    number_of_homography_based_inliers_2D = num_inliers;
    //..........................................................................

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_homography_based_inliers_mask_2D] done successfully ("
           << number_of_homography_based_inliers_2D << " inliers).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

cv::Mat
RegistrationPipeline::draw_homography_based_inliers_2D( bool flag_rich_keypoints ) const
{
    std::stringstream ss;

    cv::Mat result_image;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_homography_based_inliers_2D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (target_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_homography_based_inliers_2D] empty target_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (source_color.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_homography_based_inliers_2D] empty source_color.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return result_image;
    }

    if (target_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_homography_based_inliers_2D] empty target_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (source_keypoints_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_homography_based_inliers_2D] empty source_keypoints_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    if (filtered_matches_2D.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::draw_homography_based_inliers_2D] empty filtered_matches_2D.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }



    if ( flag_rich_keypoints )
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , homography_based_inliers_mask_2D
                         , cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS
        );
    }
    else
    {
        cv::drawMatches( target_color, target_keypoints_2D
                         , source_color, source_keypoints_2D
                         , filtered_matches_2D, result_image
                         , cv::Scalar(0, 255, 0), cv::Scalar(0, 0, 150)
                         , homography_based_inliers_mask_2D
        );
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::draw_homography_based_inliers_2D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return result_image;
}

//==============================================================================
//==============================================================================

pcl::PointCloud<pcl::PointXYZ>::Ptr
RegistrationPipeline::get_target_cloud_keypoints_ptr() const
{
    return target_cloud_keypoints_ptr;
}

//==============================================================================
//==============================================================================


pcl::PointCloud<pcl::PointXYZ>::Ptr
RegistrationPipeline::get_source_cloud_keypoints_ptr() const
{
    return source_cloud_keypoints_ptr;
}

//==============================================================================
//==============================================================================


pcl::PointCloud<pcl::PointXYZ>::Ptr
RegistrationPipeline::get_target_cloud_depth_ptr() const
{
    return target_cloud_depth_ptr;
}

//==============================================================================
//==============================================================================


pcl::PointCloud<pcl::PointXYZ>::Ptr
RegistrationPipeline::get_source_cloud_depth_ptr() const
{
    return source_cloud_depth_ptr;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_target_cloud_keypoints( bool adjust )
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_target_cloud_keypoints] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if ( target_cloud_keypoints_ptr ) target_cloud_keypoints_ptr.reset();
    target_cloud_keypoints_ptr = pcl::PointCloud<pcl::PointXYZ>::Ptr(
                new pcl::PointCloud<pcl::PointXYZ>);

    if (target_depth.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_target_cloud_keypoints] empty target_depth.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (target_keypoints_2D_pixels.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_target_cloud_keypoints] empty target_keypoints_2D_pixels.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    for (size_t i=0; i<target_keypoints_2D_pixels.size(); ++i)
    {
        // check for the depth image
        if ( 0==target_depth.at<uint16_t>(target_keypoints_2D_pixels.at(i).y
                                          , target_keypoints_2D_pixels.at(i).x) )
            continue;

        // create a temporary point
        pcl::PointXYZ target_point_3D;

        // calculate the coordinates of temporary point
        double z = target_depth.at<uint16_t>(target_keypoints_2D_pixels.at(i).y
                                             , target_keypoints_2D_pixels.at(i).x)
                / config.get_depth_image_scale() ;

        target_point_3D.z = z;
        target_point_3D.x = (target_keypoints_2D_pixels.at(i).x
                             - config.get_target_intrinsics_depth().cx)
                * (z / config.get_target_intrinsics_depth().fx);
        target_point_3D.y = (target_keypoints_2D_pixels.at(i).y
                             - config.get_target_intrinsics_depth().cy)
                * (z / config.get_target_intrinsics_depth().fy);

        target_cloud_keypoints_ptr->push_back( target_point_3D );
    }


    // size adjustment
    target_cloud_keypoints_ptr->width = target_cloud_keypoints_ptr->points.size();
    target_cloud_keypoints_ptr->height = 1;


    // pose adjustment
    if (adjust)
    {
        target_cloud_keypoints_ptr->sensor_orientation_ = Eigen::Quaternionf (0,1,0,0);
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_target_cloud_keypoints] done successfully ("
           << target_cloud_keypoints_ptr->size() << " points).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_source_cloud_keypoints( bool adjust )
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_source_cloud_keypoints] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if ( source_cloud_keypoints_ptr ) source_cloud_keypoints_ptr.reset();
    source_cloud_keypoints_ptr = pcl::PointCloud<pcl::PointXYZ>::Ptr(
                new pcl::PointCloud<pcl::PointXYZ>);

    if (source_depth.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_source_cloud_keypoints] empty source_depth.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (source_keypoints_2D_pixels.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_source_cloud_keypoints] empty source_keypoints_2D_pixels.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    for (size_t i=0; i<source_keypoints_2D_pixels.size(); ++i)
    {
        // check for the depth image
        if ( 0==source_depth.at<uint16_t>(source_keypoints_2D_pixels.at(i).y
                                          , source_keypoints_2D_pixels.at(i).x) )
            continue;

        // create a temporary point
        pcl::PointXYZ source_point_3D;

        // calculate the coordinates of temporary point
        double z = source_depth.at<uint16_t>(source_keypoints_2D_pixels.at(i).y
                                             , source_keypoints_2D_pixels.at(i).x)
                / config.get_depth_image_scale();

        source_point_3D.z = z;
        source_point_3D.x = (source_keypoints_2D_pixels.at(i).x
                             - config.get_source_intrinsics_depth().cx)
                * (z / config.get_source_intrinsics_depth().fx);
        source_point_3D.y = (source_keypoints_2D_pixels.at(i).y
                             - config.get_source_intrinsics_depth().cy)
                * (z / config.get_source_intrinsics_depth().fy);

        source_cloud_keypoints_ptr->push_back( source_point_3D );
    }


    // size adjustment
    source_cloud_keypoints_ptr->width = source_cloud_keypoints_ptr->points.size();
    source_cloud_keypoints_ptr->height = 1;


    // pose adjustment
    if (adjust)
    {
        source_cloud_keypoints_ptr->sensor_orientation_ = Eigen::Quaternionf (0,1,0,0);
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_source_cloud_keypoints] done successfully ("
           << source_cloud_keypoints_ptr->size() << " points).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_target_cloud_depth( bool adjust )
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_target_cloud_full] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if ( target_cloud_depth_ptr ) target_cloud_depth_ptr.reset();
    target_cloud_depth_ptr = pcl::PointCloud<pcl::PointXYZ>::Ptr(
                new pcl::PointCloud<pcl::PointXYZ>);

    if (target_depth.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_target_cloud_full] empty target_depth.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    target_cloud_depth_ptr = hjh::depth2pointcloud( target_depth
                                               , config.get_target_intrinsics_depth()
                                               , config.get_depth_image_scale()
                                               , adjust );
    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_target_cloud_full] done successfully ("
           << target_cloud_depth_ptr->size() << " points).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::generate_source_cloud_depth( bool adjust )
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_source_cloud_full] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if ( source_cloud_depth_ptr ) source_cloud_depth_ptr.reset();
    source_cloud_depth_ptr = pcl::PointCloud<pcl::PointXYZ>::Ptr(
                new pcl::PointCloud<pcl::PointXYZ>);

    if (source_depth.empty())
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::generate_source_cloud_full] empty source_depth.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    source_cloud_depth_ptr = hjh::depth2pointcloud( source_depth
                                               , config.get_source_intrinsics_depth()
                                               , config.get_depth_image_scale()
                                               , adjust );
    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::generate_source_cloud_full] done successfully ("
           << source_cloud_depth_ptr->size() << " points).";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
 }

//==============================================================================
//==============================================================================

bool
RegistrationPipeline::init_transformation_estimator_3D()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::init_transformation_estimator_3D_ptr] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (transformation_estimator_3D_ptr) transformation_estimator_3D_ptr.reset();

    if ( "DualQuaternion" == config.get_transformation_estimation_3D_method() )
    {
        transformation_estimator_3D_ptr = pcl::registration::TransformationEstimation<pcl::PointXYZ, pcl::PointXYZ>::Ptr(
                    new pcl::registration::TransformationEstimationDualQuaternion<pcl::PointXYZ, pcl::PointXYZ>() );
    }
    else
    if ( "SVD" == config.get_transformation_estimation_3D_method() )
    {
        transformation_estimator_3D_ptr = pcl::registration::TransformationEstimation<pcl::PointXYZ, pcl::PointXYZ>::Ptr(
                    new pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ>(true) );
    }
    else
    if ( "SVDscale" == config.get_transformation_estimation_3D_method() )
    {
        transformation_estimator_3D_ptr = pcl::registration::TransformationEstimation<pcl::PointXYZ, pcl::PointXYZ>::Ptr(
                    new pcl::registration::TransformationEstimationSVDScale<pcl::PointXYZ, pcl::PointXYZ>() );
    }
    else
    if ( "LM" == config.get_transformation_estimation_3D_method() )
    {
        transformation_estimator_3D_ptr = pcl::registration::TransformationEstimation<pcl::PointXYZ, pcl::PointXYZ>::Ptr(
                    new pcl::registration::TransformationEstimationLM<pcl::PointXYZ, pcl::PointXYZ>() );
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::init_transformation_estimator_3D_ptr] unsupported method '"
               << config.get_transformation_estimation_3D_method() << "'.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return false;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::init_transformation_estimator_3D_ptr] done successfully ("
           << config.get_transformation_estimation_3D_method() << ").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
    return true;
}

//==============================================================================
//==============================================================================

Eigen::Matrix4f
RegistrationPipeline::get_transformation_3D() const
{
    return transformation_3D;
}

//==============================================================================
//==============================================================================

void
RegistrationPipeline::estimate_transformation_3D()
{
    std::stringstream ss;

    std::string method_info( "[RegistrationPipeline::estimate_transformation_3D]" );
    if (has_logger)
    {
        ss.str("");
        ss << method_info << " started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }


    if (!transformation_estimator_3D_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " uninitialized transformation_estimator_3D_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (!target_cloud_keypoints_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " invalid target_cloud_keypoints.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }

    if (!source_cloud_keypoints_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " invalid source_cloud_keypoints.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }



    //--------------------------------------------------------------------------
    // perform the job based on the config data for correspondences rejection:
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    //
    //  no_rejection
    //
    if ( "no_rejection" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on '2D/3D' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_ptr  // <<< here is important
                        , transformation_3D );
        }
    }
    //--------------------------------------------------------------------------
    //
    //  general
    //
    else if ( "general" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'general 3D' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  reciprocal
    //
    else if ( "reciprocal" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'reciprocal' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_reciprocal_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_reciprocal_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_reciprocal_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_reciprocal_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_reciprocal_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  distance
    //
    else if ( "distance" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'distance' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_distance_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_distance_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_distance_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_reciprocal_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_distance_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  features
    //
    else if ( "features" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'features' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_features_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_features_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_features_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_features_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_features_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  median_distance
    //
    else if ( "median_distance" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'median_distance' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_median_distance_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_median_distance_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_median_distance_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_median_distance_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_median_distance_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  one_to_one
    //
    else if ( "one_to_one" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'one_to_one' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_one_to_one_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_one_to_one_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_one_to_one_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_one_to_one_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_one_to_one_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  organized_boundary
    //
    else if ( "organized_boundary" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'organized_boundary' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_organized_boundary_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_organized_boundary_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_organized_boundary_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_organized_boundary_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_organized_boundary_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  poly
    //
    else if ( "poly" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'poly' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_poly_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_poly_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_poly_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_poly_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_poly_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  sample_consensus
    //
    else if ( "sample_consensus" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'sample_consensus' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_sample_consensus_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_sample_consensus_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_sample_consensus_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_sample_consensus_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_sample_consensus_ptr  // <<< here is important
                        , transformation_3D );
        }
    }
    //--------------------------------------------------------------------------
    //
    //  sample_consensus_direct
    //
    else if ( "sample_consensus_direct" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'sample_consensus_direct' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // result is already ready, no need for calculation
        transformation_3D = get_sample_consensus_best_transformation();
    }
    //--------------------------------------------------------------------------
    //
    //  surface_normal
    //
    else if ( "surface_normal" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'surface_normal' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_surface_normal_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_surface_normal_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_surface_normal_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_surface_normal_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_surface_normal_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  trimmed
    //
    else if ( "trimmed" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'trimmed' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_trimmed_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_trimmed_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_trimmed_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_trimmed_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_trimmed_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  var_trimmed
    //
    else if ( "var_trimmed" == config.get_transformation_estimation_3D_correspondences_to_use() )
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << " based on 'var_trimmed' correspondences.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }

        // check the correspondences data
        if (!correspondences_3D_var_trimmed_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " invalid correspondences_3D_var_trimmed_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return;
        }

        if (correspondences_3D_var_trimmed_ptr->size()==0)
        {
            if (has_logger)
            {
                ss.str("");
                ss << method_info << " empty correspondences_3D_var_trimmed_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
            }
        }
        else
        {
            // rigid transformation based on selected correspondences
            transformation_estimator_3D_ptr->estimateRigidTransformation(
                        *source_cloud_keypoints_ptr
                        , *target_cloud_keypoints_ptr
                        , *correspondences_3D_var_trimmed_ptr  // <<< here is important
                        , transformation_3D );
        }

    }
    //--------------------------------------------------------------------------
    //
    //  invalid!!!
    //
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << method_info << "\""
               <<  config.get_transformation_estimation_3D_correspondences_to_use()
                << "\": invalid transformation_estimation_3D_correspondences_to_use.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return;
    }


    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::estimate_transformation_3D] transformation_3D\n"
           << transformation_3D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::estimate_transformation_3D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

Eigen::Matrix4f
RegistrationPipeline::estimate_transformation_3D_standalone(
        const pcl::PointCloud<pcl::PointXYZ>::Ptr& target
        , const pcl::PointCloud<pcl::PointXYZ>::Ptr& source
        , const pcl::CorrespondencesPtr &corres ) const
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::estimate_transformation_3D_standalone] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    Eigen::Matrix4f trans;
    trans = Eigen::MatrixXf(4,4).setIdentity();


    if (!transformation_estimator_3D_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_transformation_3D_standalone] uninitialized transformation_estimator_3D_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return trans;
    }

    if (!target)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_transformation_3D_standalone] invalid target.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return trans;
    }

    if (!source)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_transformation_3D_standalone] invalid source.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return trans;
    }

    if (!corres)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_transformation_3D_standalone] invalid correspondences_ptr (corres).";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return trans;
    }

    if (corres->size()==0)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_transformation_3D_standalone] empty correspondences_ptr (corres).";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }

    // perform the job
    transformation_estimator_3D_ptr->estimateRigidTransformation(
                *source, *target
                , *corres, trans );

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::estimate_transformation_3D_standalone] transformation_3D\n"
           << transformation_3D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::estimate_transformation_3D_standalone] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return trans;
}

//==============================================================================
//==============================================================================

bool
RegistrationPipeline::init_icp_ptr()
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::init_icp_ptr] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    if (icp_ptr) icp_ptr.reset();

    if ( "ICP" == config.get_icp_type() )
    {
        icp_ptr = pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ>::Ptr(
                    new pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ>);
    }
    else
    if ( "GICP" == config.get_icp_type() )
    {
        icp_ptr = pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ>::Ptr(
                    new pcl::GeneralizedIterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ>);
    }
    else
    if ( "ICP_NL" == config.get_icp_type() )
    {
        icp_ptr = pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ>::Ptr(
                    new pcl::IterativeClosestPointNonLinear<pcl::PointXYZ, pcl::PointXYZ>);
    }
    else
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::init_icp_ptr] unsupported method '"
               << config.get_icp_type() << "'.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return false;
    }

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::init_icp_ptr] done successfully ("
           << config.get_icp_type() << ").";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return true;
}

//==============================================================================
//==============================================================================

Eigen::Matrix4f
RegistrationPipeline::get_icp_transformation_3D() const
{
    return icp_transformation_3D;
}

//==============================================================================
//==============================================================================

pcl::PointCloud<pcl::PointXYZ>::Ptr
RegistrationPipeline::estimate_icp_transformation_3D( bool &has_converged
                                                      , double &fitness_score
                                                      , bool apply_on_keypoints
                                                      , bool apply_on_pointclouds
                                                      , bool apply_transformation )
{
    std::stringstream ss;

    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::estimate_icp_transformation_3D] started...";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr aligned_cloud_ptr // temporary cloud
        = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>());


    if (!icp_ptr)
    {
        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_icp_transformation_3D] uninitialized icp_ptr.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
        }
        return aligned_cloud_ptr;
    }

    //--------------------------------------------------------------------------
    // check input data
    //--------------------------------------------------------------------------

    if (apply_on_keypoints)
    {
        if (!target_cloud_keypoints_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << "[RegistrationPipeline::estimate_icp_transformation_3D] invalid target_cloud_keypoints_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return aligned_cloud_ptr;
        }
        if (!source_cloud_keypoints_ptr)
        {
            if (has_logger)
            {
                ss.str("");
                ss << "[RegistrationPipeline::estimate_icp_transformation_3D] invalid source_cloud_keypoints_ptr.";
                logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
            }
            return aligned_cloud_ptr;
        }
    }
    else
    {
        if (apply_on_pointclouds)
        {
            if (!target_cloud_ptr)
            {
                if (has_logger)
                {
                    ss.str("");
                    ss << "[RegistrationPipeline::estimate_icp_transformation_3D] invalid target_cloud_ptr.";
                    logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
                }
                return aligned_cloud_ptr;
            }

            if (!source_cloud_ptr)
            {
                if (has_logger)
                {
                    ss.str("");
                    ss << "[RegistrationPipeline::estimate_icp_transformation_3D] invalid source_cloud_ptr.";
                    logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
                }
                return aligned_cloud_ptr;
            }
        }
        else
        {
            if (!target_cloud_depth_ptr)
            {
                if (has_logger)
                {
                    ss.str("");
                    ss << "[RegistrationPipeline::estimate_icp_transformation_3D] invalid target_cloud_depth_ptr.";
                    logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
                }
                return aligned_cloud_ptr;
            }

            if (!source_cloud_depth_ptr)
            {
                if (has_logger)
                {
                    ss.str("");
                    ss << "[RegistrationPipeline::estimate_icp_transformation_3D] invalid source_cloud_depth_ptr.";
                    logger_ptr->add_log( ss.str(), hjh::log::LOG_ERROR );
                }
                return aligned_cloud_ptr;
            }
        }
    }


    //--------------------------------------------------------------------------
    // settings
    //--------------------------------------------------------------------------

    icp_ptr->setEuclideanFitnessEpsilon( config.icp_settings.euclidean_fitness_epsilon );
    icp_ptr->setMaxCorrespondenceDistance( config.icp_settings.corr_dist_threshold );
    icp_ptr->setMaximumIterations( config.icp_settings.max_iterations );
    icp_ptr->setRANSACIterations( config.icp_settings.ransac_iterations );
    icp_ptr->setRANSACOutlierRejectionThreshold( config.icp_settings.inlier_threshold);
    icp_ptr->setTransformationEpsilon( config.icp_settings.transformation_epsilon );


    //--------------------------------------------------------------------------
    // fine registration
    //--------------------------------------------------------------------------

    if (apply_on_keypoints)
    {
        icp_ptr->setInputSource( source_cloud_keypoints_ptr );
        icp_ptr->setInputTarget( target_cloud_keypoints_ptr );
    }
    else
    {
        if (apply_on_pointclouds)
        {
            icp_ptr->setInputSource( source_cloud_ptr );
            icp_ptr->setInputTarget( target_cloud_ptr );
        }
        else
        {
            icp_ptr->setInputSource( source_cloud_depth_ptr );
            icp_ptr->setInputTarget( target_cloud_depth_ptr );
        }
    }

    //--------------------------------------------------------------------------
    // transformation
    //--------------------------------------------------------------------------

    if (apply_transformation)
    {
        icp_ptr->align( *aligned_cloud_ptr, transformation_3D );
    }
    else
    {
        icp_ptr->align( *aligned_cloud_ptr );
    }

    icp_transformation_3D = icp_ptr->getFinalTransformation();


    //--------------------------------------------------------------------------
    // reporting
    //--------------------------------------------------------------------------

    if (icp_ptr->hasConverged())
    {
        has_converged = true;
        fitness_score = icp_ptr->getFitnessScore();

        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_icp_transformation_3D] ICP has converged (score: "
               << fitness_score << ").";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
        }
    }
    else
    {
        has_converged = false;

        if (has_logger)
        {
            ss.str("");
            ss << "[RegistrationPipeline::estimate_icp_transformation_3D] ICP has NOT converged.";
            logger_ptr->add_log( ss.str(), hjh::log::LOG_WARNING );
        }
    }


    if (has_logger)
    {
        ss.str("");
        ss << "[RegistrationPipeline::estimate_icp_transformation_3D] icp_transformation_3D\n"
           << icp_transformation_3D;
        logger_ptr->add_log( ss.str(), hjh::log::LOG_INFO );
        ss.str("");
        ss << "[RegistrationPipeline::estimate_icp_transformation_3D] done successfully.";
        logger_ptr->add_log( ss.str(), hjh::log::LOG_DEBUG );
    }

    return aligned_cloud_ptr;
}


//==============================================================================
//==============================================================================


//==============================================================================
//==============================================================================

} // end of namespace registration
} // end of namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
