#ifndef _ADJUSTED_BOX_IMP_HPP_20150810
#define _ADJUSTED_BOX_IMP_HPP_20150810

#include "boost_serialization.hpp"

namespace hjh {
namespace registration {

// forward declaration
class AdjustedBox;

// *****************************************************************************
// *****************************************************************************

template<class Archive>
void
AdjustedBox::serialize( Archive & ar, const unsigned int version )
{
    ar & BOOST_SERIALIZATION_NVP( box_number );
    ar & BOOST_SERIALIZATION_NVP( box_ID );
    ar & BOOST_SERIALIZATION_NVP( transformation );
}

// *****************************************************************************
// *****************************************************************************

} // end of namespace registration
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
