
#ifndef _BOX_PYRAMID_PAIR_HPP_20150727
#define _BOX_PYRAMID_PAIR_HPP_20150727

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "haniDefinitions.hpp"

namespace hjh {
namespace registration {

// *****************************************************************************
// *********************     E N U E R A T I O N S   ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************         C L A S S E S       ***************************
// *****************************************************************************

/*!
 * \brief The BoxPyramidPair class contains the information which matches a box
 * to an appropriate pyramid (if any match is available).
 * This class also provides facilities to compare two instantiated objects of this
 * class to decide which match is <B>better</B>.
 */
class BoxPyramidPair
{
public:

    /*!
     * To be used as 'mode' for comparison in overlaoded operators.
     */
    enum ComparisonMode {
        COMPARE_SCORE = 1
        //!< Based on fitness score.
        , COMPARE_MATCHES
        //!< Based on number of matches.
        , COMPARE_SCORE_X_MATCHES
        //!< Based on both fitness score and number of matches.
        //, COMPARE_
    };

    //--------------------------------------------------------------------------
    BoxPyramidPair();
    //!< Default constructor.

    BoxPyramidPair( const BoxPyramidPair &obj );
    //!< Copy constructor.

    /*!
     * \brief Constructor.
     * \param[in]   isValid_        Validity flag of this pair.
     * \param[in]   which_box_      Source box.
     * \param[in]   which_pyramid_  Target pyramid.
     * \param[in]   score_          Fitness score of the match.
     * \param[in]   num_matches_    Number of inlier matches between source and
     * target (after 2D and 3D rejections and filtering).
     * \param[in]   transform_      The transformation Rt in hjh::Pose format to
     * align the source (box) to the target (pyramid).
     * \param[in]   transform_ICP_  Same as transform_ but based on ICP.
     * \param[in]   mode_           The comparison mode to inialize the \ref
     * comparison_mode (used in overloaded operations).
     */
    BoxPyramidPair( bool isValid_
                    , int which_box_
                    , int which_pyramid_
                    , double score_
                    , int num_matches_
                    , hjh::Pose transform_
                    , hjh::Pose transform_ICP_
                    , ComparisonMode mode_ );

    virtual
    ~BoxPyramidPair();
    //!< Destructor.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    virtual
    inline
    BoxPyramidPair&
    operator=( const BoxPyramidPair &obj );
    //!< Overloaded '=' (assignemt) operator.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    typedef boost::shared_ptr<BoxPyramidPair> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const BoxPyramidPair> ConstPtr;
    //!< Const shared pointer.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    /*!
     * \brief operator == is the overloaded '==' (equality) operator for this
     * class (comparison only based on \ref score and \ref number_of_matches).
     * \param[in]   rhs The right-hand side operand.
     * \return boolean compraison result.
     */
    virtual
    bool
    operator==( const BoxPyramidPair &rhs ) const;
    //!<

    /*!
     * \brief operator != is the overloaded '!=' (inequality) operator for this
     * class (comparison only based on \ref score and \ref number_of_matches).
     * \param[in]   rhs The right-hand side operand.
     * \return boolean compraison result.
     */
    virtual
    inline
    bool
    operator!=( const BoxPyramidPair &rhs ) const;

    /*!
     * \brief operator < is the overloaded '<' (less than) operator for this
     * class (comparison only based on \ref score and \ref number_of_matches).
     * \note The result is calculated due to \ref comparison_mode value.
     * \param[in]   rhs The right-hand side operand.
     * \return boolean compraison result.
     */
    virtual
    inline
    bool
    operator<( const BoxPyramidPair &rhs ) const;

    /*!
     * \brief operator <= is the overloaded '<=' (less or equal than) operator for this
     * class (comparison only based on \ref score and \ref number_of_matches).
     * \note The result is calculated due to \ref comparison_mode value.
     * \param[in]   rhs The right-hand side operand.
     * \return boolean compraison result.
     */
    virtual
    inline
    bool
    operator<=( const BoxPyramidPair &rhs ) const;

    /*!
     * \brief operator > is the overloaded '>' (greater/larger than) operator for this
     * class (comparison only based on \ref score and \ref number_of_matches).
     * \note The result is calculated due to \ref comparison_mode value.
     * \param[in]   rhs The right-hand side operand.
     * \return boolean compraison result.
     */
    virtual
    inline
    bool
    operator>( const BoxPyramidPair &rhs ) const;

    /*!
     * \brief operator >= is the overloaded '>=' (greater/larger or equal than)
     * operator for this class (comparison only based on \ref score and
     * \ref number_of_matches).
     * \note The result is calculated due to \ref comparison_mode value.
     * \param[in]   rhs The right-hand side operand.
     * \return boolean compraison result.
     */
    virtual
    inline
    bool
    operator>=( const BoxPyramidPair &rhs ) const;
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    // boost archive ...
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    virtual
    inline
    bool
    is_valid() const;
    //!< An alias for \ref get_flag_valid.

    virtual
    inline
    bool
    get_flag_valid() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    void
    set_flag_valid( bool flag_ );
    //!< Sets the corresponding data member.

    virtual
    inline
    int
    get_box() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    void
    set_box( int box_ );
    //!< Sets the corresponding data member.

    virtual
    inline
    int
    get_pyramid() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    void
    set_pyramid( int pyramid_ );
    //!< Sets the corresponding data member.

    virtual
    inline
    double
    get_score() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    void
    set_score( double score_ );
    //!< Sets the corresponding data member.

    virtual
    inline
    int
    get_number_of_matches() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    void
    set_number_of_matches( int num_matches_ );
    //!< Sets the corresponding data member.

    virtual
    inline
    hjh::Pose
    get_transform() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    void
    set_transform( hjh::Pose Rt );
    //!< Sets the corresponding data member.

    virtual
    inline
    hjh::Pose
    get_transform_ICP() const;
    //!< Returns the corresponding data member.

    virtual
    inline
    void
    set_transform_ICP( hjh::Pose Rt );
    //!< Sets the corresponding data member.


    virtual
    inline
    ComparisonMode
    get_comparison_mode() const;
    //!< Returns the corresponding data member.

    /*!
     * \brief Sets the corresponding data member (prevents invalid assignment
     * by setting \ref COMPARE_SCORE_X_MATCHES as the \ref comparison_mode).
     */
    virtual
    inline
    void
    set_comparison_mode( ComparisonMode mode_ );
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    friend std::ostream&
    operator<<(std::ostream& os, const BoxPyramidPair& obj);
    //!< Overloaded output-stream.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
private:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler.
    //--------------------------------------------------------------------------



protected:

    //--------------------------------------------------------------------------
    /*!
     * \brief flag_valid indicates the validity of the match between the source
     * (box) and the target (pyramid).
     */
    bool flag_valid;

    int box;
    //!< The source.

    int pyramid;
    //!< The target.

    double score;
    //!< The fitness score of the match between the source and target.

    int number_of_matches;
    //!< The the number of inlier 2D/3D matches between the source and target.

    /*!
     * \brief transform represents the Rt matrix in hjh::Pose format to align
     * the source into the target.
     * \sa transform_ICP.
     */
    hjh::Pose transform;

    /*!
     * \brief transform_ICP represents the Rt matrix in hjh::Pose format to align
     * the source into the target (based on ICP algorithm).
     * \sa transform.
     */
    hjh::Pose transform_ICP;

    /*!
     * \brief comparison_mode contains the mode used by overlaoded operators to
     * compare a pair of objects.
     */
    ComparisonMode comparison_mode;
    //--------------------------------------------------------------------------
};


// *****************************************************************************
// *****************************************************************************

} // end of namespace registration
} // end of namespace hjh

#include "Registration/BoxPyramidPair_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
