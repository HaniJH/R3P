
// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "Registration/BoxPyramidRegistration.hpp"

#include "haniUtilities.hpp"
#include "Utilities/PointCloudUtilities.hpp"

// to read images
#include <opencv2/opencv.hpp>

// to read point clouds
#include <pcl/io/pcd_io.h>

#include <string>
#include <sstream>

namespace hjh {
namespace registration {

// *****************************************************************************
// *********************    D E F I N I T I O N S    ***************************
// *****************************************************************************


//==============================================================================
//==============================================================================

void
load_pyramid_vector( const std::string &folder_name
                 , const std::vector<hjh::pcd2img::PointCloudToPyramidLog> &logs
                 , std::vector<hjh::pcd2img::SquarePyramidPack> &pyramids
                 , PYRAMID_COMPONENT flag
                 , double H_FOV_start
                 , double H_FOV_end
                 , double V_FOV_start
                 , double V_FOV_end
                 , const Logger::Ptr &external_logger_ptr
                 )
{
    bool log = true;
    if ( !external_logger_ptr ) log = false;

    std::string function_info = "[hjh::registration::load_pyramid_vector]";
    std::stringstream ss;

    if ( log )
    {
        ss.str("");
        ss << function_info << " started...";
        external_logger_ptr->add_log( ss.str(), LOG_DEBUG );
    }

    int counter(0);
    int counter_max = logs.size();


    // reading the images and pointclouds and put in the pyramid vector
    for ( std::vector<hjh::pcd2img::PointCloudToPyramidLog>::const_iterator
          it = logs.begin(); it != logs.end(); ++it, counter++ )
    {
        if (    it->horizontal_FOV >= H_FOV_start
             && it->horizontal_FOV <= H_FOV_end
             && it->vertical_FOV   >= V_FOV_start
             && it->vertical_FOV   <= V_FOV_end )
        {
            // temporary container
            hjh::pcd2img::SquarePyramidPack temp_pyramid;

            // first, transform the log data
            temp_pyramid.log = *it;

            // read the color image
            if ( flag & PYRAMID_COLOR_IMAGE )
            {
                temp_pyramid.color_image.release();
                temp_pyramid.color_image
                        = cv::imread( folder_name + "/" + it->color_image_filename
                                      , CV_LOAD_IMAGE_ANYCOLOR );
                if ( log )
                {
                    ss.str("");
                    if ( temp_pyramid.color_image.empty() )
                    {
                        ss << function_info << " failure in loading color image.";
                        external_logger_ptr->add_log( ss.str(), LOG_WARNING );
                    }
                    else
                    {
                        ss << function_info << " color image loaded successfully.";
                        external_logger_ptr->add_log( ss.str(), LOG_INFO );
                    }
                }
            }

            // read the depth image
            if ( flag & PYRAMID_DEPTH_IMAGE )
            {
                temp_pyramid.depth_image.release();
                temp_pyramid.depth_image
                        = cv::imread( folder_name + "/" + it->depth_image_filename
                                      , CV_LOAD_IMAGE_ANYDEPTH );
                if ( log )
                {
                    ss.str("");
                    if ( temp_pyramid.depth_image.empty() )
                    {
                        ss << function_info << " failure in loading depth image.";
                        external_logger_ptr->add_log( ss.str(), LOG_WARNING );
                    }
                    else
                    {
                        ss << function_info << " depth image loaded successfully.";
                        external_logger_ptr->add_log( ss.str(), LOG_INFO );
                    }
                }
            }

            // read the original cloud
            if ( flag & PYRAMID_ORIGINAL_CLOUD )
            {
                temp_pyramid.original_cloud_ptr = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(
                            new pcl::PointCloud<pcl::PointXYZRGB> );
                if ( 0 > pcl::io::loadPCDFile( folder_name + "/" + it->original_cloud_filename
                                      , *(temp_pyramid.original_cloud_ptr) ) )
                {
                    if ( log )
                    {
                        ss.str("");
                        ss << function_info << " failure in loading original cloud.";
                        external_logger_ptr->add_log( ss.str(), LOG_WARNING );
                    }
                }
                else
                {
                    if ( log )
                    {
                        ss.str("");
                        ss << function_info << " original cloud loaded successfully.";
                        external_logger_ptr->add_log( ss.str(), LOG_INFO );
                    }
                }
            }

            // read the voxelized cloud
            if ( flag & PYRAMID_VOXELIZED_CLOUD )
            {
                temp_pyramid.voxelized_cloud_ptr = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(
                            new pcl::PointCloud<pcl::PointXYZRGB> );
                if ( 0 > pcl::io::loadPCDFile( folder_name + "/" + it->voxelized_cloud_filename
                                      , *(temp_pyramid.voxelized_cloud_ptr) ) )
                {
                    if ( log )
                    {
                        ss.str("");
                        ss << function_info << " failure in loading voxelized cloud.";
                        external_logger_ptr->add_log( ss.str(), LOG_WARNING );
                    }
                }
                else
                {
                    if ( log )
                    {
                        ss.str("");
                        ss << function_info << " voxelized cloud loaded successfully.";
                        external_logger_ptr->add_log( ss.str(), LOG_INFO );
                    }
                }
            }

            // add it to the vector
            pyramids.push_back( temp_pyramid );

            if ( log )
            {
                ss.str("");
                ss << function_info << " \"" << it->pose.posename << "\" added"
                   << " (" << std::setprecision( 2 ) << std::fixed << counter * 100.0f / counter_max << "%)";
                external_logger_ptr->add_log( ss.str(), LOG_DEBUG );
            }        }

    }

    if ( log )
    {
        ss.str("");
        ss << function_info << " ...done.";
        external_logger_ptr->add_log( ss.str(), LOG_DEBUG );
    }
}

//==============================================================================
//==============================================================================

hjh::registration::BoxPyramidPair
register_box_to_appropriate_pyramid(const hjh::boxfusion::RichBox &box
        , const std::vector<hjh::pcd2img::SquarePyramidPack> &pyramids_vec
        , const hjh::registration::RegistrationPipeline::Ptr &regpipeline_ptr
        , const int box_counter_
        , const int number_of_boxes_
        , const int loop_start_
        , const double hint_horizontal_FOV_degree
        , const double hint_vertical_FOV_degree
        , const bool flag_valid_FOV_hint
        , const uint16_t hint_horizontal_search_radius
        , const uint16_t hint_vertical_search_radius
        , const bool flag_always_check_zero_radius
        , const pcd2img::PointCloudParsingConfig::Ptr cfg
        , const hjh::vis::ImgVis::Ptr vis_ptr
        , const unsigned int delay_ms
        , const hjh::log::Logger::Ptr &exlogger_ptr )
{
    // the external logger
    using namespace hjh::log;
    bool logflag = true;
    if ( !exlogger_ptr ) logflag = false;
    std::string method_info( "[register_box_to_appropriate_pyramid]" );

    // the image viewer
    bool visflag = false;
    if (vis_ptr) visflag = true;

    // temporary string stream
    std::stringstream ss;

    if ( logflag )
    {
        ss.str("");
        ss << method_info << box.info.get_ID() << ": started...";
        exlogger_ptr->add_log( ss.str(), LOG_DEBUG );
    }
    //##########################################################################
    // started ...
    //##########################################################################

    //..........................................................................
    //  best fitness score and local pyramid counter
    //..........................................................................

    hjh::registration::BoxPyramidPair best_score_till_now;

    //..........................................................................
    //  reading the source files ()
    // \note Pyramid is 'target' and Box is 'source'.
    //..........................................................................
{
    if ( logflag )
    {
        ss.str("");
        ss << method_info << " Box as the source and pyramid as the target.";
        exlogger_ptr->add_log( ss.str(), LOG_INFO );
    }

    cv::Mat sc; box.get_first_color().copyTo( sc );
    cv::Mat sd; box.get_first_depth().copyTo( sd );

    regpipeline_ptr->set_source_color( sc );
    regpipeline_ptr->set_source_depth( sd );

    if ( vis_ptr )
    {
        ss.str("");
        ss << box.info.get_ID() << " (color)";
        vis_ptr->set_caption( ss.str(), 0, 1 );
    }
}

    //..........................................................................
    //  RegPipeline: detector 2D
    //..........................................................................
{
    regpipeline_ptr->detect_source_keypoints_2D();
    regpipeline_ptr->draw_source_keypoints_2D();

    if ( vis_ptr )
    {
        vis_ptr->set_image( regpipeline_ptr->get_source_color(), 0,1 );
    }
}

    //..........................................................................
    //  RegPipeline: descriptor 2D
    //..........................................................................

    regpipeline_ptr->compute_source_descriptors_2D();

    //--------------------------------------------------------------------------
    //  loop (each pyramid as a target)
    //--------------------------------------------------------------------------

    // the local log data
    std::stringstream lss;

    // local loop counter
    int pyramid_counter(0);
    int max_pyramids = pyramids_vec.size();


    //--------------------------------------------------------------------------
    //      H I N T
    //--------------------------------------------------------------------------

    double hfov(0), vfov(0);
    double hstep(0), vstep(0);

    if ( flag_valid_FOV_hint )
    {
        // convert hint
        convert_FOV_hint_to_FOV_specific( hint_horizontal_FOV_degree
                                          , hint_vertical_FOV_degree
                                          , hfov, vfov, *cfg );

        // calculate step size
        hstep = ( 1 - cfg->horizontal_overlap ) * cfg->pcd_to_img_cfg.horizontal_FOV;
        vstep = ( 1 - cfg->vertical_overlap ) * cfg->pcd_to_img_cfg.vertical_FOV;

        ss.str("");
        ss << method_info << ": applying hint (" << hfov << ", " << vfov << ", "
           << hint_horizontal_search_radius << ", " << hint_vertical_search_radius << ").";
        exlogger_ptr->add_log( ss.str(), LOG_INFO );
    }


    //##########################################################################
    //          l o o p :   b o x   t o   p y r a m i d S
    //##########################################################################
    for ( std::vector<hjh::pcd2img::SquarePyramidPack>::const_iterator
         pyramid_it = pyramids_vec.begin();
         pyramid_it != pyramids_vec.end(); ++pyramid_it )
    {
        //hjh::wait_ms( 40 );

        // local info
        lss.str("");
        lss << box.info.get_ID() << ", pyramid( " << pyramid_counter << " )";

        if ( vis_ptr )
        {
            int total_steps = number_of_boxes_ * max_pyramids;
            int current_step = ((box_counter_-loop_start_) * max_pyramids) + pyramid_counter;
            double step_percentage = (current_step * 100.0) / total_steps;

            ss.str("");
            ss << std::setprecision(2) << std::fixed
               << step_percentage << "%     "
               << " box (" << (box_counter_+1)-loop_start_
               << " of " << number_of_boxes_ << "), "
               << " pyramid (" << pyramid_counter+1
               << " of " << max_pyramids << ").";
            vis_ptr->add_log( ss.str() );
        }

        //......................................................................
        // to decide about "go/no go" in each step of the loop
        //......................................................................

        bool go_ahead( true );

        //--------------------------------------------------------------------------
        //      H I N T
        //--------------------------------------------------------------------------
        // choosing 1 is just for having some tolerance
        if ( flag_valid_FOV_hint )
        {

            if (
                    // horizontally
                    ( pyramid_it->log.horizontal_FOV
                      <= (hfov + (hstep * hint_horizontal_search_radius) )
                    &&
                      pyramid_it->log.horizontal_FOV
                      >= (hfov - (hstep * hint_horizontal_search_radius) )
                    &&
                      ( std::abs( pyramid_it->log.vertical_FOV
                             - (vfov + (vstep * hint_vertical_search_radius) ) )
                        <= 1
                      ||
                        std::abs( pyramid_it->log.vertical_FOV
                             - (vfov - (vstep * hint_vertical_search_radius) ) )
                        <= 1 )
                    )

                    ||

                    // vertically
                    ( pyramid_it->log.vertical_FOV
                      <= (vfov + (vstep * hint_vertical_search_radius) )
                    &&
                      pyramid_it->log.vertical_FOV
                      >= (vfov - (vstep * hint_vertical_search_radius) )
                    &&
                      ( std::abs( pyramid_it->log.horizontal_FOV
                             - (hfov + (hstep * hint_horizontal_search_radius) ) )
                        <= 1
                      ||
                        std::abs( pyramid_it->log.horizontal_FOV
                             - (hfov - (hstep * hint_horizontal_search_radius) ) )
                        <= 1 )
                    )
               )
            {
                go_ahead = true;
            }
            else
            {
                go_ahead = false;
            }

            // cancel the radius effect for zero radius if required
            if ( !go_ahead && flag_always_check_zero_radius )
            {
                if ( std::abs( pyramid_it->log.horizontal_FOV - hfov ) < 1
                     &&
                     std::abs( pyramid_it->log.vertical_FOV - vfov ) < 1 )
                {
                    go_ahead = true;
                }
            }

            if ( logflag )
            {
                if ( go_ahead )
                {
                    ss.str("");
                    ss << method_info << " processing pyramid FOV (" << pyramid_it->log.horizontal_FOV
                          << ", " << pyramid_it->log.vertical_FOV << ") is okay.";
                    exlogger_ptr->add_log( ss.str(), LOG_OKAY );
                }
                else
                {
                    ss.str("");
                    ss << method_info << " processing pyramid FOV (" << pyramid_it->log.horizontal_FOV
                          << ", " << pyramid_it->log.vertical_FOV << ") is failed.";
                    exlogger_ptr->add_log( ss.str(), LOG_WARNING );
                }
            }
        }
        //--------------------------------------------------------------------------


        //......................................................................
        //  reading the target files
        //......................................................................

        if ( go_ahead )
        {
            if ( logflag )
            {
                ss.str("");
                ss << method_info << " reading the target files...";
                exlogger_ptr->add_log( ss.str(), LOG_DEBUG );
            }

            cv::Mat tc; pyramid_it->color_image.copyTo( tc );
            cv::Mat td; pyramid_it->depth_image.copyTo( td );
            if ( tc.empty() || td.empty() )
            {
                go_ahead = false;
            }

            regpipeline_ptr->set_target_color( tc );
            regpipeline_ptr->set_target_depth( td );

            if ( vis_ptr )
            {
                ss.str("");
                ss << lss.str() << " (color)";
                vis_ptr->set_caption( ss.str(), 0,0 );
            }

            if ( go_ahead )
            {

                if ( logflag )
                {
                    ss.str("");
                    ss << method_info << " " << lss.str()
                       << " reading the target files done: successfully.";
                    exlogger_ptr->add_log( ss.str(), LOG_DEBUG );
                }
            }
            else
            {
                if ( logflag )
                {
                    ss.str("");
                    ss << method_info << " " << lss.str()
                       << " reading the target files done: DON'T GO.";
                    exlogger_ptr->add_log( ss.str(), LOG_WARNING );
                }
                // DO NOT GO!
            }
        }

        //......................................................................
        //  RegPipeline: detector 2D
        //......................................................................

        if ( go_ahead )
        {
            regpipeline_ptr->detect_target_keypoints_2D();
            regpipeline_ptr->draw_target_keypoints_2D();

            if ( vis_ptr )
            {
                vis_ptr->set_image( regpipeline_ptr->get_target_color(), 0,0 );
            }
        }

        //......................................................................
        //  RegPipeline: descriptor 2D
        //......................................................................

        if ( go_ahead )
        {
            regpipeline_ptr->compute_target_descriptors_2D();
        }

        //......................................................................
        //  RegPipeline: matcher 2D
        //......................................................................

        if ( go_ahead )
        {
            regpipeline_ptr->match_2D();
            ss.str("");
            ss << method_info << " " << lss.str() << " number of matches: "
               << regpipeline_ptr->get_filtered_matches_2D().size();
            exlogger_ptr->add_log( ss.str(), LOG_INFO );

            if ( regpipeline_ptr->get_filtered_matches_2D().size() < 4 )
            {
                go_ahead = false;
            }

            if ( go_ahead )
            {
                // empty
            }
            else
            {
                if ( logflag )
                {
                    ss.str("");
                    ss << lss.str() << " no match.";
                    exlogger_ptr->add_log( ss.str(), LOG_WARNING );
                }
                // DO NOT GO!
            }
        }

        //......................................................................
        // RegPipeline: 2D inliers...
        // decide based on the "correspondence_base"
        //......................................................................


        if ( go_ahead )
        {
            regpipeline_ptr->find_transformation_2D_based_on_config();
            regpipeline_ptr->generate_inliers_mask_2D_based_on_config();

            if ( regpipeline_ptr->get_matrix_source_to_target_2D_based_on_config().empty()
                 ||
                 regpipeline_ptr->get_number_of_inliers_2D_based_on_config() < 4 )
            {
                go_ahead = false;
            }

            if ( go_ahead )
            {
                if ( visflag )
                {
                    cv::Mat inliers = regpipeline_ptr->draw_inliers_2D_based_on_config( true );
                    ss.str("");
                    ss << "inlier matches ("
                       << regpipeline_ptr->config.get_correspondence_base() << ")";
                    cv::imshow( ss.str(), inliers );
                }
            }
            else
            {
                if ( logflag )
                {
                    ss.str("");
                    ss << method_info << " " << lss.str() << " not enough 2D inlier.";
                    exlogger_ptr->add_log( ss.str(), LOG_WARNING );
                }
                // DO NOT GO!
            }
        }

        //......................................................................
        // correpondences 2D/3D
        //......................................................................

        if ( go_ahead )
        {
            regpipeline_ptr->estimate_correspondences();

            if ( logflag )
            {
                ss.str("");
                ss << method_info << " " << "correpondences 2D/3D: "
                   << regpipeline_ptr->get_correspondences_ptr()->size();
                exlogger_ptr->add_log(ss.str());
            }

            cv::Mat remaning_inliers
                    = regpipeline_ptr->draw_correspondent_inlier_keypoints();
            cv::imshow( "remaning inliers", remaning_inliers );

            int nremaining2D3D
                    = regpipeline_ptr->get_correspondences_ptr()->size();
            if ( nremaining2D3D < 4  )
            {
                go_ahead = false;

                if ( logflag )
                {
                    ss.str("");
                    ss << method_info << " " << lss.str()
                       << " not enough 2D/3D correpondences ("
                       << nremaining2D3D << ").";
                    exlogger_ptr->add_log( ss.str(), LOG_WARNING );
                }
                // DO NOT GO!
            }
        }

        //......................................................................
        // convert to pointcloud
        //......................................................................

        if ( go_ahead )
        {
            regpipeline_ptr->generate_source_cloud_keypoints();

            if ( logflag )
            {
                ss.str("");
                ss << method_info << " source keypoints-cloud generated: "
                   << regpipeline_ptr->get_source_cloud_keypoints_ptr()->size();
                exlogger_ptr->add_log(ss.str());
            }

            regpipeline_ptr->generate_target_cloud_keypoints();

            if ( logflag )
            {
                ss.str("");
                ss << method_info << " target keypoints-cloud generated: "
                   << regpipeline_ptr->get_target_cloud_keypoints_ptr()->size();
                exlogger_ptr->add_log(ss.str());
            }

        }

        //......................................................................
        // correpondences 3D
        //......................................................................

        if ( go_ahead )
        {
            regpipeline_ptr->determine_correspondences();
        }

        //......................................................................
        // correpondences 3D rejection
        //......................................................................

        // temp variable
        int nremainings(0);

        if ( go_ahead )
        {
            regpipeline_ptr->reject_correspondences_based_on_config();

            nremainings
                = regpipeline_ptr->get_correspondences_remaining_ptr()->size();

            if ( nremainings < 4 )
            {
                go_ahead = false;

                if ( logflag )
                {
                    ss.str("");
                    ss << method_info << " " << lss.str()
                       << " not enough 3D correpondences after rejection ("
                       << nremainings << ").";
                    exlogger_ptr->add_log( ss.str(), LOG_WARNING );
                }
                // DO NOT GO!
            }
        }

        //......................................................................
        // transformation estimation 3D
        //......................................................................

        if ( go_ahead )
        {
            regpipeline_ptr->estimate_transformation_3D();

            if ( logflag )
            {
                ss.str("");
                ss << method_info << " transformation 3D:\n"
                   << regpipeline_ptr->get_transformation_3D();
                exlogger_ptr->add_log( ss.str() );
            }
        }

        //......................................................................
        // fine registration 3D (ICP)
        // two steps: (1) key points, (2) if converged then on depth-clouds.
        //......................................................................

        // temp variables
        bool converged(false);
        double fscore(0);

        if ( go_ahead )
        {
            // (1) key points
            converged = false;
            fscore = 0;
            regpipeline_ptr->estimate_icp_transformation_3D(
                        converged
                        , fscore
                        , true  // apply_on_keypoints
                        , false // apply_on_pointclouds
                        , true  // apply_transformation
                        );

            // check the convergence
            go_ahead = converged;


            if ( go_ahead )
            {
                if ( logflag )
                {
                    ss.str("");
                    ss << method_info
                       << " ICP transformation 3D has converged (on keypoints) with fintness score ("
                       << fscore << "):\n"
                       << regpipeline_ptr->get_icp_transformation_3D();
                    exlogger_ptr->add_log( ss.str() );
                }

//                // (2) if converged then on depth-clouds.
//                converged = false;
//                fscore = 0;
//                regpipeline_ptr->estimate_icp_transformation_3D(
//                            converged
//                            , fscore
//                            , false  // apply_on_keypoints
//                            , false // apply_on_pointclouds
//                            , true  // apply_transformation
//                            );

//                go_ahead = converged;

//                if ( go_ahead )
//                {
//                    if ( logflag )
//                    {
//                        ss.str("");
//                        ss << method_info
//                           << " ICP transformation 3D has converged (on depth clouds) with fintness score ("
//                           << fscore << "):\n"
//                           << regpipeline_ptr->get_icp_transformation_3D();
//                        ulogger->add_log( ss.str() );
//                    }
//                }
//                else
//                {
//                    if ( logflag )
//                    {
//                        ss.str("");
//                        ss << method_info << " " << lss
//                           << " ICP transformation (on depth clouds) 3D has NOT converged.";
//                        logger->add_log( ss.str(), LOG_WARNING );
//                    }
//                    // DO NOT GO!
//                }


            }
            else
            {
                if ( logflag )
                {
                    ss << method_info << " " << lss.str()
                       << " ICP transformation (on keypoints) 3D has NOT converged.";
                    exlogger_ptr->add_log( ss.str(), LOG_WARNING );
                }
                // DO NOT GO!
            }
        }


        //......................................................................
        // updating the 'best score till now'
        //......................................................................

        if ( go_ahead )
        {
            // temp variables
            hjh::Pose pose( "transform", true ), pose_icp( "ICP_transform", true );

            pose.fromMatrix4f( regpipeline_ptr->get_transformation_3D() );
            pose_icp.fromMatrix4f( regpipeline_ptr->get_icp_transformation_3D() );

            hjh::registration::BoxPyramidPair new_score( converged
                 , box_counter_
                 , pyramid_counter
                 , fscore
                 , nremainings
                 , pose
                 , pose_icp
                 , hjh::registration::BoxPyramidPair::COMPARE_SCORE_X_MATCHES );

            if ( best_score_till_now < new_score )
            {
                best_score_till_now = new_score;

                if ( logflag )
                {
                    ss.str("");
                    ss << method_info << " " << lss.str()
                       << " best_score_till_now: " << best_score_till_now;
                    exlogger_ptr->add_log( ss.str(), LOG_INFO );
                }
            }
        }



        //......................................................................
        // visualization: pointclouds
        //......................................................................

        if ( go_ahead )
        {
            // to do
        }







        //......................................................................
        //  increasing the counter
        //......................................................................

        pyramid_counter++;

        //......................................................................
        // delay if required
        //......................................................................

        if ( delay_ms > 0 )
        {
            hjh::wait_ms( delay_ms );
        }
    }

    //##########################################################################
    //          e n d   o f   l o o p
    //##########################################################################


    //......................................................................
    // delay if required
    //......................................................................

    if ( delay_ms > 0 )
    {
        hjh::wait_ms( delay_ms );
    }

    //##########################################################################
    // ... finished.
    //##########################################################################
    if ( logflag )
    {
        ss.str("");
        ss << method_info << " " << lss.str()
           << " best_score: " << best_score_till_now;
        exlogger_ptr->add_log( ss.str(), LOG_INFO );
        ss.str("");
        ss << method_info << " box " << box.info.get_ID() << ": ...finished.";
        exlogger_ptr->add_log( ss.str(), LOG_DEBUG );
    }

    return best_score_till_now;
}

//==============================================================================
//==============================================================================

int
addjust_box_to_model_based_on_pyramid(
        int box_number_
        , int pyramid_number_
        , const hjh::registration::BoxPyramidPair &b2p_data_
        , const hjh::boxfusion::BoxChain::Ptr &box_chain_
        , const std::vector<hjh::pcd2img::PointCloudToPyramidLog> &pyramid_log_vector_
        , hjh::Pose &pose_
        , const Logger::Ptr &external_logger_ptr
        )
{
    bool log = true;
    if ( !external_logger_ptr ) log = false;

    std::string function_info = "[hjh::registration::addjust_box_to_model_based_on_pyramid]";
    std::stringstream ss;

    if ( log )
    {
        ss.str("");
        ss << function_info << " started...";
        external_logger_ptr->add_log( ss.str(), LOG_DEBUG );
    }

    hjh::Pose temp_pose;
    Eigen::Matrix4f transformation = temp_pose.toMatrix4f();

    if ( log )
    {
        ss.str("");
        ss << function_info << " initialized transformation:\n" << transformation;
        external_logger_ptr->add_log( ss.str(), LOG_INFO );
    }

    //--------------------------------------------------------------------------
    // inverse of box-pose,
    //--------------------------------------------------------------------------

//    transformation =
//            box_chain_->get_chain().at( box_number_ )->info.get_pose().toMatrix4f().inverse()
//            * transformation;

//    if ( log )
//    {
//        ss.str("");
//        ss << function_info << " 'inverse of box-pose' applied:\n"
//           << transformation;
//        external_logger_ptr->add_log( ss.str(), LOG_INFO );
//    }

    //--------------------------------------------------------------------------
    // box: back-to-origin,
    //--------------------------------------------------------------------------

    transformation =
            box_chain_->get_chain().at( box_number_ ).info.get_back_to_origin_transformation()
            * transformation;

    if ( log )
    {
        ss.str("");
        ss << function_info << " 'back-to-origin' applied:\n"
           << transformation;
        external_logger_ptr->add_log( ss.str(), LOG_INFO );
    }

    //--------------------------------------------------------------------------
    // box-to-pyramid,
    //--------------------------------------------------------------------------

    transformation = b2p_data_.get_transform().toMatrix4f()
            * transformation;

    if ( log )
    {
        ss.str("");
        ss << function_info << " 'box-to-pyramid' applied:\n"
           << transformation;
        external_logger_ptr->add_log( ss.str(), LOG_INFO );
    }

    //--------------------------------------------------------------------------
    // from-Kinect-to-FARO,
    //--------------------------------------------------------------------------

    transformation = hjh::utility::Rt_Kinect_to_FARO() * transformation;

    if ( log )
    {
        ss.str("");
        ss << function_info << " 'from-Kinect-to-FARO' applied:\n"
           << transformation;
        external_logger_ptr->add_log( ss.str(), LOG_INFO );
    }

    //--------------------------------------------------------------------------
    // inverse of pyramid-pose.
    //--------------------------------------------------------------------------

    transformation = pyramid_log_vector_.at( pyramid_number_ ).pose.toMatrix4f().inverse()
            * transformation;



    if ( log )
    {
        ss.str("");
        ss << function_info << " 'inverse of pyramid-pose' applied:\n"
           << transformation;
        external_logger_ptr->add_log( ss.str(), LOG_INFO );
    }

    //--------------------------------------------------------------------------
    // put the result in the pose
    //--------------------------------------------------------------------------
    pose_.fromMatrix4f( transformation );

    if ( log )
    {
        ss.str("");
        ss << function_info << " final transformation:\n" << transformation;
        external_logger_ptr->add_log( ss.str(), LOG_DEBUG );
    }
    if ( log )
    {
        ss.str("");
        ss << function_info << " ...done.";
        external_logger_ptr->add_log( ss.str(), LOG_DEBUG );
    }
    return 0;
}

//==============================================================================
//==============================================================================

bool
extract_FOV_hint_from_box_pose( const hjh::Pose &box_pose_
                      , double &horizontal_FOV_hint
                      , double &vertical_FOV_hint )
{
    // toDo BADAN
    return false;
}

//==============================================================================
//==============================================================================

void
convert_FOV_hint_to_FOV_specific( double   hint_horizontal_FOV_degree
                                  , double hint_vertical_FOV_degree
                                  , double &specific_horizontal_FOV_degree
                                  , double &specific_vertical_FOV_degree
                                  , double horizontal_FOV_degree
                                  , double vertical_FOV_degree
                                  , double horizontal_overlap
                                  , double vertical_overlap
                                  , double max_horizontal_FOV
                                  , double min_horizontal_FOV
                                  , double max_vertical_FOV
                                  , double min_vertical_FOV )
{
    //--------------------------------------------------------------------------
    // check the hint range
    //--------------------------------------------------------------------------
    // Horizontal FOV:
    //   - for negative FOVs
    if ( hint_horizontal_FOV_degree < 0 )
    {
        hint_horizontal_FOV_degree += 360;
        convert_FOV_hint_to_FOV_specific(
                    hint_horizontal_FOV_degree
                    , hint_vertical_FOV_degree
                    , specific_horizontal_FOV_degree
                    , specific_vertical_FOV_degree
                    , horizontal_FOV_degree
                    , vertical_FOV_degree
                    , horizontal_overlap
                    , vertical_overlap
                    , max_horizontal_FOV
                    , min_horizontal_FOV
                    , max_vertical_FOV
                    , min_vertical_FOV );
        return;
    }
    //   - for out-of-range FOVs
    if ( hint_horizontal_FOV_degree > 360 )
    {
        hint_horizontal_FOV_degree -= 360;
        convert_FOV_hint_to_FOV_specific(
                    hint_horizontal_FOV_degree
                    , hint_vertical_FOV_degree
                    , specific_horizontal_FOV_degree
                    , specific_vertical_FOV_degree
                    , horizontal_FOV_degree
                    , vertical_FOV_degree
                    , horizontal_overlap
                    , vertical_overlap
                    , max_horizontal_FOV
                    , min_horizontal_FOV
                    , max_vertical_FOV
                    , min_vertical_FOV );
        return;
    }
    //--------------------------------------------------------------------------
    // Vertical FOV:
    //   - for < -90 FOVs
    if ( hint_vertical_FOV_degree < -90 )
    {
        hint_vertical_FOV_degree += 180;
        convert_FOV_hint_to_FOV_specific(
                    hint_horizontal_FOV_degree
                    , hint_vertical_FOV_degree
                    , specific_horizontal_FOV_degree
                    , specific_vertical_FOV_degree
                    , horizontal_FOV_degree
                    , vertical_FOV_degree
                    , horizontal_overlap
                    , vertical_overlap
                    , max_horizontal_FOV
                    , min_horizontal_FOV
                    , max_vertical_FOV
                    , min_vertical_FOV );
        return;
    }
    //   - for out-of-range (>90) FOVs
    if ( hint_vertical_FOV_degree > 90 )
    {
        hint_vertical_FOV_degree -= 180;
        convert_FOV_hint_to_FOV_specific(
                    hint_horizontal_FOV_degree
                    , hint_vertical_FOV_degree
                    , specific_horizontal_FOV_degree
                    , specific_vertical_FOV_degree
                    , horizontal_FOV_degree
                    , vertical_FOV_degree
                    , horizontal_overlap
                    , vertical_overlap
                    , max_horizontal_FOV
                    , min_horizontal_FOV
                    , max_vertical_FOV
                    , min_vertical_FOV );
        return;
    }
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    //  find the FOVs which covers the hint
    //--------------------------------------------------------------------------
    // Horizontal
    if ( hint_horizontal_FOV_degree <= min_horizontal_FOV )
    {
        specific_horizontal_FOV_degree = min_horizontal_FOV;
    }
    else if (  hint_horizontal_FOV_degree >= max_horizontal_FOV )
    {
        double hfov( min_horizontal_FOV );
        while ( hfov <= max_horizontal_FOV )
        {
            specific_horizontal_FOV_degree = hfov;
            hfov += ( 1 - horizontal_overlap ) * horizontal_FOV_degree;
        }
    }
    else
    {
        double hfov( min_horizontal_FOV ), dist( horizontal_FOV_degree );
        while ( hfov < max_horizontal_FOV )
        {
            // middle of the frame - hint
            double d = std::abs( ( hfov + (horizontal_FOV_degree / 2) )
                                 - hint_horizontal_FOV_degree );
            if ( d <= dist )
            {
                specific_horizontal_FOV_degree = hfov;
                dist = d;
            }

            // next one
            hfov += ( 1 - horizontal_overlap ) * horizontal_FOV_degree;
        }
    }
    //--------------------------------------------------------------------------
    // Vertival
    if ( hint_vertical_FOV_degree <= min_vertical_FOV )
    {
        specific_vertical_FOV_degree = min_vertical_FOV;
    }
    else if (  hint_vertical_FOV_degree >= max_vertical_FOV )
    {
        double vfov( min_vertical_FOV );
        while ( vfov <= max_vertical_FOV )
        {
            specific_vertical_FOV_degree = vfov;
            vfov += ( 1 - vertical_overlap ) * vertical_FOV_degree;
        }
    }
    else
    {
        double vfov( min_vertical_FOV ), dist( vertical_FOV_degree );
        while ( vfov < max_vertical_FOV )
        {
            // middle of the frame - hint
            double d = std::abs( ( vfov + (vertical_FOV_degree / 2) )
                                 - hint_vertical_FOV_degree );
            if ( d <= dist )
            {
                specific_vertical_FOV_degree = vfov;
                dist = d;
            }

            // next one
            vfov += ( 1 - vertical_overlap ) * vertical_FOV_degree;
        }
    }
    //--------------------------------------------------------------------------
}

//==============================================================================
//==============================================================================

void
convert_FOV_hint_to_FOV_specific( double hint_horizontal_FOV_degree
                              , double hint_vertical_FOV_degree
                              , double &specific_horizontal_FOV_degree // output
                              , double &specific_vertical_FOV_degree // output
                              , const hjh::pcd2img::PointCloudParsingConfig &cfg
                              )
{
    convert_FOV_hint_to_FOV_specific( hint_horizontal_FOV_degree
                                      , hint_vertical_FOV_degree
                                      , specific_horizontal_FOV_degree
                                      , specific_vertical_FOV_degree
                                      , cfg.pcd_to_img_cfg.horizontal_FOV
                                      , cfg.pcd_to_img_cfg.vertical_FOV
                                      , cfg.horizontal_overlap
                                      , cfg.vertical_overlap
                                      , cfg.max_horizontal_FOV
                                      , cfg.min_horizontal_FOV
                                      , cfg.max_vertical_FOV
                                      , cfg.min_vertical_FOV );
}

//==============================================================================
//==============================================================================

//==============================================================================
//==============================================================================

//==============================================================================
//==============================================================================


} // namespace registration
} // namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
