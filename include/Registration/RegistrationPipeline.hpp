
#ifndef _REGISTRATION_PIPELINE_HPP_20150224_
#define _REGISTRATION_PIPELINE_HPP_20150224_

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************


#include "Registration/RegistrationPipeline_config.hpp"
#include "Logger/Logger.hpp"


#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/correspondence.h>
#include <pcl/registration/transformation_estimation.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/correspondence_rejection.h>
#include <pcl/registration/correspondence_rejection_distance.h>
#include <pcl/registration/correspondence_rejection_features.h>
#include <pcl/registration/correspondence_rejection_median_distance.h>
#include <pcl/registration/correspondence_rejection_one_to_one.h>
#include <pcl/registration/correspondence_rejection_organized_boundary.h>
#include <pcl/registration/correspondence_rejection_poly.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/correspondence_rejection_surface_normal.h>
#include <pcl/registration/correspondence_rejection_trimmed.h>
#include <pcl/registration/correspondence_rejection_var_trimmed.h>


#include <string>
#include <ctime>
#include <vector>
#include <stdint.h>
#include <boost/shared_ptr.hpp>
#include <boost/thread/shared_mutex.hpp>


namespace hjh {
namespace registration {

// *****************************************************************************
// *********************     E N U E R A T I O N S   ***************************
// *****************************************************************************

// /*!
// * \brief The INLIER_MASK enum defines various ways to determine inliers among
// * matched features between the source and target images.
// */
//enum INLIER_METHOD : uint8_t
//{
//      ALL_AS_INLIERS = 0
//    //!< it taks all the matched features as inliers.
//    , SINGLE_SOURCE_TO_TARGET
//    //!< single-direction: it transforms the source to the target and then keeps the features which are matched after transformation.
//    , SINGLE_TARGET_TO_SOURCE
//    //!< single-direction: it transforms the target to the source (if possible) and then keeps the features which are matched after transformation.
//    , BI_SOURCE_AND_TARGET
//    //!< bidirection: intersection of the inliers obtained by the \ref SINGLE_SOURCE_TO_TARGET and \ref SINGLE_TARGET_TO_SOURCE.
//    , BI_SOURCE_OR_TARGET
//    //!< bidirection: union of the inliers obtained by the \ref SINGLE_SOURCE_TO_TARGET and \ref SINGLE_TARGET_TO_SOURCE.
//};



// *****************************************************************************
// *********************         C L A S S E S       ***************************
// *****************************************************************************


/*!
 * \brief RegistrationPipeline class defines a pipeline with various stages to
 * register a pair of 2D/3D data together.
 *
 * \note It registers the source into the target.
 *
 * \note This class has a thread-safe implementation.
 *
 */
class RegistrationPipeline {

public:

    RegistrationPipeline();
    //!< Default constructor.

    RegistrationPipeline(  log::Logger::Ptr external_logger_ );
    //!< Constructor connected to an external logger module.

    virtual
    ~RegistrationPipeline();
    //!< Destructor.


    typedef boost::shared_ptr<RegistrationPipeline> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const RegistrationPipeline> ConstPtr;
    //!< Constant shared pointer.


    /*!
     * \brief Initializes all the component of the module based on \ref config data.
     * \note In order to utilize any functionality of this object, it is required
     * to initialize the module first.
     * \return A negative integer if anything goes wrong.
     */
    virtual
    int
    initialize();

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    bool
    is_initialized() const;

    /*!
     * \brief Exchanges the source and target data (including images, pointclouds
     * , keypoints, and descriptors) with each other.
     * \note Usage: cascading a chain of target-source pairs.
     */
    virtual
    void
    swap_source_and_target();

    //--------------------------------------------------------------------------
    //      c o n f i g u r a t i o n
    //--------------------------------------------------------------------------

    RegistrationPipelineConfig config;
    //!< Configuration ralated to the module.

    //--------------------------------------------------------------------------
    //      i n p u t   d a t a   2 D / 3 D
    //--------------------------------------------------------------------------

    virtual
    cv::Mat
    get_target_color() const;
    //!< Returns the corresponding data member.

    virtual
    cv::Mat
    get_target_depth() const;
    //!< Returns the corresponding data member.

    virtual
    cv::Mat
    get_source_color() const;
    //!< Returns the corresponding data member.

    virtual
    cv::Mat
    get_source_depth() const;
    //!< Returns the corresponding data member.

    virtual
    void
    set_target_color( const cv::Mat& img_ );
    //!< Sets the corresponding data member (checks the input data).

    virtual
    void
    set_target_depth( const cv::Mat& img_ );
    //!< Sets the corresponding data member (checks the input data).

    virtual
    void
    set_source_color( const cv::Mat& img_ );
    //!< Sets the corresponding data member (checks the input data).

    virtual
    void
    set_source_depth( const cv::Mat& img_ );
    //!< Sets the corresponding data member (checks the input data).


    virtual
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    get_target_cloud_ptr() const;
    //!< Returns the corresponding data member.

    virtual
    void
    set_target_cloud_ptr( const pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr ) ;
    //!< Sets the corresponding data member (checks the input data).

    virtual
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    get_source_cloud_ptr() const;
    //!< Returns the corresponding data member.

    virtual
    void
    set_source_cloud_ptr( const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud_ptr ) ;
    //!< Sets the corresponding data member (checks the input data).



    //--------------------------------------------------------------------------
    //      F e a t u r e   D e t e c t o r   2 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the target keypoints.
     */
    virtual
    std::vector<cv::KeyPoint>
    get_target_keypoints_2D() const;

    /*!
     * \brief Returns the source keypoints.
     */
    virtual
    std::vector<cv::KeyPoint>
    get_source_keypoints_2D() const;

    /*!
     * \brief Detects the keypoints of the \ref target_color and puts them in
     * \ref target_keypoints.
     * \sa get_target_keypoints_2D.
     */
    virtual
    void
    detect_target_keypoints_2D();

    /*!
     * \brief Detects the keypoints of the \ref source_color and puts them in
     * \ref source_keypoints.
     * * \sa get_source_keypoints_2D.
     */
    virtual
    void
    detect_source_keypoints_2D();

    /*!
     * \brief Detects the keypoints of the input \ref image_ and returns them
     * (can be used as an standalone method independent of the module data).
     */
    virtual
    std::vector<cv::KeyPoint>
    detect_keypoints_2D( const cv::Mat& image_ ) const;

    /*!
     * \brief Draws the detected \ref target_keypoints on the \ref target_color image.
     */
    virtual
    void
    draw_target_keypoints_2D();

    /*!
     * \brief Draws the detected \ref source_keypoints on the \ref source_color image.
     */
    virtual
    void
    draw_source_keypoints_2D();

    /*!
     * \brief Draws the input \ref keypoints_ on the input \ref image_.
     * (can be used as an standalone method independent of the module data).
     */
    virtual
    void
    draw_keypoints_2D( const std::vector<cv::KeyPoint> &keypoints_, cv::Mat image_ ) const;


    //--------------------------------------------------------------------------
    //      F e a t u r e   D e s c r i p t o r   2 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the descriptors for the target keypoints.
     */
    virtual
    cv::Mat
    get_target_descriptors_2D() const;

    /*!
     * \brief Returns the descriptors for the target keypoints.
     */
    virtual
    cv::Mat
    get_source_descriptors_2D() const;


    /*!
     * \brief Compute descriptors for the keypoints of the target image.
     * \sa get_target_descriptors_2D.
     */
    virtual
    void
    compute_target_descriptors_2D();

    /*!
     * \brief Compute descriptors for the keypoints of the source image.
     * \sa get_source_descriptors_2D.
     */
    virtual
    void
    compute_source_descriptors_2D();

    /*!
     * \brief Compute descriptors for the input keypoints/image and returns them.
     * (can be used as an standalone method independent of the module data).
     */
    virtual
    cv::Mat
    compute_descriptors_2D( const cv::Mat& image_
                          , std::vector<cv::KeyPoint> &keypoints_ ) const;


    //--------------------------------------------------------------------------
    //      F e a t u r e   M a t c h e r   2 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the matched descriptors of target and source.
     */
    virtual
    std::vector<cv::DMatch>
    get_filtered_matches_2D() const;

    /*!
     * \brief Matches the source descriptors into target ones based on \ref
     * RegistrationPipelineConfig::matching_filter_type (and \ref
     * RegistrationPipelineConfig::knn_number_of_matches).
     * \sa get_filtered_matches_2D.
     */
    virtual
    void
    match_2D();

    /*!
     * \brief Standalone version of \ref match_2D.
     *
     * Matches the source descriptors into target ones based on \ref
     * RegistrationPipelineConfig::matching_filter_type (and \ref
     * RegistrationPipelineConfig::knn_number_of_matches) .
     * \return The matched descriptors of target and source.
     */
    virtual
    std::vector<cv::DMatch>
    match_2D_standalone( cv::Mat target_des_, cv::Mat source_des_ );


    //--------------------------------------------------------------------------
    //      A f f i n e   2 D
    //      P e r s p e c t i v e
    //      H o m o g r a p h y   2 D
    //      F u n d a m e n t a l   M a t r i x   2 D
    //      E s s e n t i a l   M a t r i x   2 D
    //      R i g i d   T r a n s f o r m a t i o n   2 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the 2D matrix to transfom source to target based on the
     * config data (\ref RegistrationPipelineConfig::correspondence_base).
     * \sa find_transformation_2D_based_on_config,
     * get_inliers_mask_2D_based_on_config,
     * get_number_of_inliers_2D_based_on_config,
     * generate_inliers_mask_2D_based_on_config,
     * and draw_inliers_2D_based_on_config.
     */
    virtual
    cv::Mat
    get_matrix_source_to_target_2D_based_on_config() const;

    /*!
     * \brief Returns the 2D rigid transformation matrix.
     */
    virtual
    cv::Mat
    get_rigid_transformation_source_to_target_2D() const;

    /*!
     * \brief Returns the direct 2D rigid transformation matrix.
     */
    virtual
    cv::Mat
    get_rigid_transformation_direct_source_to_target_2D() const;

    /*!
     * \brief Returns the 2D affine matrix.
     */
    virtual
    cv::Mat
    get_A_source_to_target_2D() const;

    /*!
     * \brief Returns the 2D perspective matrix.
     */
    virtual
    cv::Mat
    get_P_source_to_target_2D() const;

    /*!
     * \brief Returns the 2D fundamental matrix.
     */
    virtual
    cv::Mat
    get_F_source_to_target_2D() const;

    /*!
     * \brief Returns the 2D essential matrix.
     */
    virtual
    cv::Mat
    get_E_source_to_target_2D() const;

    /*!
     * \brief Returns the 2D homography matrix.
     */
    virtual
    cv::Mat
    get_H_source_to_target_2D() const;

    //..........................................................................

    /*!
     * \brief find_transformation_2D_based_on_config finds the 2D transfom (source
     * to target) based on the config data (\ref
     * RegistrationPipelineConfig::correspondence_base).
     * \sa get_matrix_source_to_target_2D_based_on_config,
     * get_inliers_mask_2D_based_on_config,
     * get_number_of_inliers_2D_based_on_config,
     * generate_inliers_mask_2D_based_on_config,
     * and draw_inliers_2D_based_on_config.
     */
    virtual
    void
    find_transformation_2D_based_on_config();

    /*!
     * \brief Finds the 2D rigid transformation matrix (based on extracted features).
     * \note To find a rigid transformation based on themselves itself (not
     * extracted features) refer to \ref find_rigid_transformation_2D_direct method.
     * \sa get_rigid_transformation_source_to_target_2D.
     */
    virtual
    void
    find_rigid_transformation_2D();

    /*!
     * \brief Finds the 2D rigid transformation matrix (based on images themselves).
     * \note This method works independently from extracted features.
     * \note To find a rigid transformation based on extracted features refer to
     * \ref find_rigid_transformation_2D method.
     * \sa get_rigid_transformation_source_to_target_2D.
     */
    virtual
    void
    find_rigid_transformation_2D_direct();

    /*!
     * \brief Finds the 2D affine matrix.
     * \note UNDER CONSTRUCTION.
     * \sa get_A_source_to_target_2D.
     */
    virtual
    void
    find_affine_2D();

    /*!
     * \brief Finds the 2D perspective matrix.
     * \note UNDER CONSTRUCTION.
     * \sa get_P_source_to_target_2D.
     */
    virtual
    void
    find_perspective_2D();

    /*!
     * \brief Finds the 2D fundamental matrix.
     * \sa get_F_source_to_target_2D.
     */
    virtual
    void
    find_fundamental_matrix_2D();

    /*!
     * \brief Finds the 2D essential matrix.
     * \note Internally, this method calls \ref find_fundamental_matrix_2D.
     * \sa get_E_source_to_target_2D.
     */
    virtual
    void
    find_essential_matrix_2D();

    /*!
     * \brief Finds the 2D homography matrix.
     * \sa get_H_source_to_target_2D.
     */
    virtual
    void
    find_homography_2D();

    //..........................................................................

    /*!
     * \brief Standalone version of the \ref find_rigid_transformation_2D.
     * \return The rigid transformation matrix (affine) matrix.
     */
    virtual
    cv::Mat
    find_rigid_transformation_2D_standalone( const std::vector<cv::DMatch> &matched_
                                   , const std::vector<cv::KeyPoint> &tkeypoints
                                   , const std::vector<cv::KeyPoint> &skeypoints
                                   , std::vector<cv::Point2f> &tpoints
                                   , std::vector<cv::Point2f> &spoints ) const;

    /*!
     * \brief Standalone version of the \ref find_affine_2D.
     * \return The affine matrix.
     */
    virtual
    cv::Mat
    find_affine_2D_standalone( const std::vector<cv::DMatch> &matched_
                                   , const std::vector<cv::KeyPoint> &tkeypoints
                                   , const std::vector<cv::KeyPoint> &skeypoints
                                   , std::vector<cv::Point2f> &tpoints
                                   , std::vector<cv::Point2f> &spoints ) const;

    /*!
     * \brief Standalone version of the \ref find_perspective_2D.
     * \return The perspective matrix.
     */
    virtual
    cv::Mat
    find_perspective_2D_standalone( const std::vector<cv::DMatch> &matched_
                                   , const std::vector<cv::KeyPoint> &tkeypoints
                                   , const std::vector<cv::KeyPoint> &skeypoints
                                   , std::vector<cv::Point2f> &tpoints
                                   , std::vector<cv::Point2f> &spoints ) const;

    /*!
     * \brief Standalone version of the \ref find_fundamental_matrix_2D.
     * \return The fundamental matrix.
     */
    virtual
    cv::Mat
    find_fundamental_matrix_2D_standalone( const std::vector<cv::DMatch> &matched_
                                   , const std::vector<cv::KeyPoint> &tkeypoints
                                   , const std::vector<cv::KeyPoint> &skeypoints
                                   , std::vector<cv::Point2f> &tpoints
                                   , std::vector<cv::Point2f> &spoints ) const;

    /*!
     * \brief Standalone version of the \ref find_essential_matrix_2D.
     * \note Internally, this method calls \ref find_fundamental_matrix_2D_standalone.
     * \return The fundamental matrix.
     */
    virtual
    cv::Mat
    find_essential_matrix_2D_standalone( const std::vector<cv::DMatch> &matched_
                                   , const std::vector<cv::KeyPoint> &tkeypoints
                                   , const std::vector<cv::KeyPoint> &skeypoints
                                   , std::vector<cv::Point2f> &tpoints
                                   , std::vector<cv::Point2f> &spoints ) const;

    /*!
     * \brief Standalone version of the \ref find_homography_2D.
     * \return The homography matrix.
     */
    virtual
    cv::Mat
    find_homography_2D_standalone( const std::vector<cv::DMatch> &matched_
                                   , const std::vector<cv::KeyPoint> &tkeypoints
                                   , const std::vector<cv::KeyPoint> &skeypoints
                                   , std::vector<cv::Point2f> &tpoints
                                   , std::vector<cv::Point2f> &spoints ) const;

    //..........................................................................

    /*!
     * \brief get_inliers_mask_2D_based_on_config returns the inliers (2D) based
     * on the config data (\ref RegistrationPipelineConfig::correspondence_base).
     * \sa get_matrix_source_to_target_2D_based_on_config,
     * find_transformation_2D_based_on_config,
     * get_number_of_inliers_2D_based_on_config,
     * generate_inliers_mask_2D_based_on_config,
     * and draw_inliers_2D_based_on_config.
     */
    virtual
    std::vector<char>
    get_inliers_mask_2D_based_on_config() const;

    /*!
     * \brief Returns the \ref rigid_transformation_based_inliers_mask_2D.
     */
    virtual
    std::vector<char>
    get_rigid_transformation_based_inliers_mask_2D() const;

    /*!
     * \brief Returns the \ref rigid_transformation_direct_based_inliers_mask_2D.
     */
    virtual
    std::vector<char>
    get_rigid_transformation_direct_based_inliers_mask_2D() const;

    /*!
     * \brief Returns the \ref affine_based_inliers_mask_2D.
     */
    virtual
    std::vector<char>
    get_affine_based_inliers_mask_2D() const;

    /*!
     * \brief Returns the \ref perspective_based_inliers_mask_2D.
     */
    virtual
    std::vector<char>
    get_perspective_based_inliers_mask_2D() const;

    /*!
     * \brief Returns the \ref fundamental_matrix_based_inliers_mask_2D.
     */
    virtual
    std::vector<char>
    get_fundamental_matrix_based_inliers_mask_2D() const;

    /*!
     * \brief Returns the \ref essential_matrix_based_inliers_mask_2D.
     */
    virtual
    std::vector<char>
    get_essential_matrix_based_inliers_mask_2D() const;

    /*!
     * \brief Returns the \ref homography_based_inliers_mask_2D.
     */
    virtual
    std::vector<char>
    get_homography_based_inliers_mask_2D() const;

    //..........................................................................

    /*!
     * \brief get_number_of_inliers_2D_based_on_config returns the number of inliers
     * (2D) based on the config data (\ref
     * RegistrationPipelineConfig::correspondence_base).
     * \return -1 for invalid config data.
     * \sa get_matrix_source_to_target_2D_based_on_config,
     * find_transformation_2D_based_on_config,
     * get_inliers_mask_2D_based_on_config,
     * generate_inliers_mask_2D_based_on_config,
     * and draw_inliers_2D_based_on_config.
     */
    virtual
    int
    get_number_of_inliers_2D_based_on_config() const;

    /*!
     * \brief Returns the number of inliers exists in \ref
     * rigid_transformation_based_inliers_mask_2D.
     */
    virtual
    int
    get_number_of_rigid_transformation_based_inliers_2D() const;

    /*!
     * \brief Returns the number of inliers exists in \ref
     * rigid_transformation_direct_based_inliers_mask_2D.
     */
    virtual
    int
    get_number_of_rigid_transformation_direct_based_inliers_2D() const;

    /*!
     * \brief Returns the number of inliers exists in \ref
     * affine_based_inliers_mask_2D.
     */
    virtual
    int
    get_number_of_affine_based_inliers_2D() const;

    /*!
     * \brief Returns the number of inliers exists in \ref
     * perspective_based_inliers_mask_2D.
     */
    virtual
    int
    get_number_of_perspective_based_inliers_2D() const;

    /*!
     * \brief Returns the number of inliers exists in \ref
     * fundamental_matrix_based_inliers_mask_2D.
     */
    virtual
    int
    get_number_of_fundamental_matrix_based_inliers_2D() const;

    /*!
     * \brief Returns the number of inliers exists in \ref
     * essential_matrix_based_inliers_mask_2D.
     */
    virtual
    int
    get_number_of_essential_matrix_based_inliers_2D() const;

     /*!
     * \brief Returns the number of inliers exists in \ref
     * homography_based_inliers_mask_2D.
     */
    virtual
    int
    get_number_of_homography_based_inliers_2D() const;

    //..........................................................................

    /*!
     * \brief generate_inliers_mask_2D_based_on_config generates the inliers mask
     * 2D based on the config data (\ref RegistrationPipelineConfig::correspondence_base).
     * \note The result is dependent on the \ref inliers_max_threshold.
     * \sa get_matrix_source_to_target_2D_based_on_config,
     * find_transformation_2D_based_on_config,
     * get_inliers_mask_2D_based_on_config,
     * get_number_of_inliers_2D_based_on_config,
     * and draw_inliers_2D_based_on_config.
     */
    virtual
    void
    generate_inliers_mask_2D_based_on_config();


    /*!
     * \brief Generates the \ref rigid_transformation_based_inliers_mask_2D.
     * \note The result is dependent on the \ref inliers_max_threshold.
     * \sa get_rigid_transformation_based_inliers_mask_2D and get_number_of_rigid_transformation_based_inliers_2D.
     */
    virtual
    void
    generate_rigid_transformation_based_inliers_mask_2D();

    /*!
     * \brief Generates the \ref rigid_transformation_based_inliers_mask_2D.
     * \note The result is dependent on the \ref inliers_max_threshold.
     * \sa get_rigid_transformation_based_inliers_mask_2D and get_number_of_rigid_transformation_based_inliers_2D.
     */
    virtual
    void
    generate_rigid_transformation_direct_based_inliers_mask_2D();

    /*!
     * \brief Generates the \ref affine_based_inliers_mask_2D.
     * \note The result is dependent on the \ref inliers_max_threshold.
     * \sa get_affine_based_inliers_mask_2D and get_number_of_affine_based_inliers_2D.
     */
    virtual
    void
    generate_affine_based_inliers_mask_2D();

    /*!
     * \brief Generates the \ref perspective_based_inliers_mask_2D.
     * \note The result is dependent on the \ref inliers_max_threshold.
     * \sa get_perspective_based_inliers_mask_2D and get_number_of_perspective_based_inliers_2D.
     */
    virtual
    void
    generate_perspective_based_inliers_mask_2D();

    /*!
     * \brief Generates the \ref fundamental_matrix_based_inliers_mask_2D.
     * \note The result is dependent on the \ref inliers_max_threshold.
     * \sa get_fundamental_matrix_based_inliers_mask_2D and get_number_of_fundamental_matrix_based_inliers_2D.
     */
    virtual
    void
    generate_fundamental_matrix_based_inliers_mask_2D();

    /*!
     * \brief Generates the \ref essential_matrix_based_inliers_mask_2D.
     * \note The result is dependent on the \ref inliers_max_threshold.
     * \sa get_essential_matrix_based_inliers_mask_2D and get_number_of_essential_matrix_based_inliers_2D.
     */
    virtual
    void
    generate_essential_matrix_based_inliers_mask_2D();

    /*!
     * \brief Generates the \ref homography_based_inliers_mask_2D.
     * \note The result is dependent on the \ref inliers_max_threshold.
     * \sa get_homography_based_inliers_mask_2D and get_number_of_homography_based_inliers_2D.
     */
    virtual
    void
    generate_homography_based_inliers_mask_2D();

    //..........................................................................

    /*!
     * \brief draw_inliers_2D_based_on_config draws inliers and put the result in
     * an image based on the config data (\ref
     * RegistrationPipelineConfig::correspondence_base).
     * \param[in]   flag_rich_keypoints Indicates whether to draw rich-keypoints or not.
     * \return The result image.
     * \sa get_matrix_source_to_target_2D_based_on_config,
     * find_transformation_2D_based_on_config,
     * get_inliers_mask_2D_based_on_config,
     * get_number_of_inliers_2D_based_on_config,
     * and generate_inliers_mask_2D_based_on_config.
     */
    virtual
    cv::Mat
    draw_inliers_2D_based_on_config( bool flag_rich_keypoints=false ) const;

    /*!
     * \brief Draws inliers (rigid_transformation based) and put the result in an image.
     * \param[in]   flag_rich_keypoints Indicates whether to draw rich-keypoints or not.
     * \return The result image.
     */
    virtual
    cv::Mat
    draw_rigid_transformation_based_inliers_2D( bool flag_rich_keypoints=false ) const;

    /*!
     * \brief Draws inliers (rigid_transformation_direct based) and put the result
     * in an image.
     * \param[in]   flag_rich_keypoints Indicates whether to draw rich-keypoints or not.
     * \return The result image.
     */
    virtual
    cv::Mat
    draw_rigid_transformation_direct_based_inliers_2D( bool flag_rich_keypoints=false ) const;

    /*!
     * \brief Draws inliers (affine based) and put the result in an image.
     * \param[in]   flag_rich_keypoints Indicates whether to draw rich-keypoints or not.
     * \return The result image.
     */
    virtual
    cv::Mat
    draw_affine_based_inliers_2D( bool flag_rich_keypoints=false ) const;

    /*!
     * \brief Draws inliers (perspective based) and put the result in an image.
     * \param[in]   flag_rich_keypoints Indicates whether to draw rich-keypoints or not.
     * \return The result image.
     */
    virtual
    cv::Mat
    draw_perspective_based_inliers_2D( bool flag_rich_keypoints=false ) const;

    /*!
     * \brief Draws inliers (fundamental_matrix based) and put the result in an image.
     * \param[in]   flag_rich_keypoints Indicates whether to draw rich-keypoints or not.
     * \return The result image.
     */
    virtual
    cv::Mat
    draw_fundamental_matrix_based_inliers_2D( bool flag_rich_keypoints=false ) const;

    /*!
     * \brief Draws inliers (essential_matrix based) and put the result in an image.
     * \param[in]   flag_rich_keypoints Indicates whether to draw rich-keypoints or not.
     * \return The result image.
     */
    virtual
    cv::Mat
    draw_essential_matrix_based_inliers_2D( bool flag_rich_keypoints=false ) const;

    /*!
     * \brief Draws inliers (homography based) and put the result in an image.
     * \param[in]   flag_rich_keypoints Indicates whether to draw rich-keypoints or not.
     * \return The result image.
     */
    virtual
    cv::Mat
    draw_homography_based_inliers_2D( bool flag_rich_keypoints=false ) const;



    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   2 D / 3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the \ref correspondences_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_ptr() const;

    /*!
     * \brief Returns the corresponding member-data.
     */
    virtual
    std::vector<cv::Point>
    get_target_keypoints_2D_pixels() const;

    /*!
     * \brief Returns the corresponding member-data.
     */
    virtual
    std::vector<cv::Point>
    get_source_keypoints_2D_pixels() const;

    /*!
     * \brief Estimates the correspondences_ptr between the target and source
     * keypoint-clouds according to the \ref filtered_matches_2D and the target
     * and source depth-images. This method generates the following data:
     * the \ref source_keypoints_2D_pixels, the \ref target_keypoints_2D_pixels,
     * and the \ref correspondences_ptr.
     * \note It finds correspondeces based on the method indicated by the
     * \ref RegistrationPipelineConfig::correspondence_base.
     * \sa draw_correspondent_keypoints, get_correspondences_ptr, and
     * determine_correspondences.
     */
    virtual
    void
    estimate_correspondences();

    /*!
     * \brief draw_correspondent_keypoints draws the remaining keypoints after
     * applying the \ref estimate_correspondences method (left: target, right:
     * source).
     * \return An image containing the masked target and source color images
     * with connected pairs of matched keypoints.
     */
    virtual
    cv::Mat
    draw_correspondent_inlier_keypoints() const;


    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the \ref correspondences_3D_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_remaining_ptr.
     * \sa reject_correspondences_based_on_config.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_remaining_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_reciprocal_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_reciprocal_ptr() const;

    /*!
     * \brief determine_correspondences determines correspondences between two
     * keypoints point-clouds by applying 3D domain algorithms (updates both
     * the \ref correspondences_3D_ptr and \ref correspondences_3D_reciprocal_ptr).
     * \note It requires to call the \ref generate_target_cloud_keypoints and
     * \ref generate_source_cloud_keypoints methods before calling this method
     * to generate keypoints clouds.
     * \sa estimate_correspondences;
     */
    virtual
    void
    determine_correspondences();


    //--------------------------------------------------------------------------
    //
    //      C o r r e s p o n d e n c e s   R e j e c t i o n
    //      ( i n l i e r s    3 D )
    //
    //      from PCL library:
    //          - distance
    //          - features
    //          - median distance
    //          - one to one
    //          - organized boundary
    //          - poly
    //          - sample consensus
    //          - surface normal
    //          - trimmed
    //          - var trimmed
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the \ref correspondences_3D_distance_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_distance_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_features_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_features_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_median_distance_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_median_distance_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_one_to_one_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_one_to_one_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_organized_boundary_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_organized_boundary_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_poly_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_poly_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_sample_consensus_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_sample_consensus_ptr() const;

    /*!
     * \brief Returns the \ref sample_consensus_best_transformation.
     * \note Valid only after calling the \ref reject_correspondences_sample_consensus
     * method.
     */
    virtual
    Eigen::Matrix4f
    get_sample_consensus_best_transformation() const;

    /*!
     * \brief Returns the \ref correspondences_3D_surface_normal_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_surface_normal_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_trimmed_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_trimmed_ptr() const;

    /*!
     * \brief Returns the \ref correspondences_3D_var_trimmed_ptr.
     */
    virtual
    pcl::CorrespondencesPtr
    get_correspondences_3D_var_trimmed_ptr() const;

    //--------------------------------------------------------------------------

    /*!
     * \brief reject_correspondences_based_on_config rejects the outlier correspondences
     * based on the method indicated via config data (
     * \ref RegistrationPipelineConfig::transformation_estimation_3D_correspondences_to_use).
     * \sa get_correspondences_remaining_ptr.
     */
    virtual
    void
    reject_correspondences_based_on_config();

    /*!
     * \brief reject_correspondences_distance rejects the outlier correspondences
     * based on 'distance' method.
     * \sa get_correspondences_3D_distance_ptr.
     */
    virtual
    void
    reject_correspondences_distance();

    /*!
     * \brief <B>UNDER CONSTRUCTION</b> reject_correspondences_features rejects
     * the outlier correspondences based on 'features' method.
     * \note This method is under construction...
     * \sa get_correspondences_3D_features_ptr.
     */
    virtual
    void
    reject_correspondences_features();

    /*!
     * \brief reject_correspondences_median_distance rejects the outlier
     * correspondences based on 'median distance' method.
     * \sa get_correspondences_3D_median_distance_ptr.
     */
    virtual
    void
    reject_correspondences_median_distance();

    /*!
     * \brief reject_correspondences_one_to_one rejects the outlier
     * correspondences based on 'one to one' method.
     * \sa get_correspondences_3D_one_to_one_ptr.
     */
    virtual
    void
    reject_correspondences_one_to_one();

    /*!
     * \brief <B>UNDER CONSTRUCTION</b> reject_correspondences_organized_boundary rejects the outlier
     * correspondences based on 'organized boundary' method.
     * \note This method is under construction...
     * \sa get_correspondences_3D_organized_boundary_ptr.
     */
    virtual
    void
    reject_correspondences_organized_boundary();

    /*!
     * \brief <B>UNDER CONSTRUCTION</b> reject_correspondences_poly rejects the outlier
     * correspondences based on 'poly' method.
     * \note This method is under construction...
     * \sa get_correspondences_3D_poly_ptr.
     */
    virtual
    void
    reject_correspondences_poly();

    /*!
     * \brief reject_correspondences_sample_consensus rejects the outlier
     * correspondences based on 'sample consensus (SAC)' method.
     * \sa get_correspondences_3D_sample_consensus_ptr.
     */
    virtual
    void
    reject_correspondences_sample_consensus();

    /*!
     * \brief <B>UNDER CONSTRUCTION</b> reject_correspondences_surface_normal rejects the outlier
     * correspondences based on 'surface normal' method.
     * \note This method is under construction...
     * \sa get_correspondences_3D_surface_normal_ptr.
     */
    virtual
    void
    reject_correspondences_surface_normal();

    /*!
     * \brief reject_correspondences_trimmed rejects the outlier
     * correspondences based on 'trimmed' method.
     * \sa get_correspondences_3D_trimmed_ptr.
     */
    virtual
    void
    reject_correspondences_trimmed();

    /*!
     * \brief reject_correspondences_var_trimmed rejects the outlier
     * correspondences based on 'var trimmed' method.
     * \sa get_correspondences_3D_var_trimmed_ptr.
     */
    virtual
    void
    reject_correspondences_var_trimmed();



    //--------------------------------------------------------------------------
    //     C o n v e r t   2 D   f e a t u r e s   t o   p o i n t c l o u d
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the corresponding member-data.
     */
    virtual
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    get_target_cloud_keypoints_ptr() const;

    /*!
     * \brief Returns the corresponding member-data.
     */
    virtual
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    get_source_cloud_keypoints_ptr() const;

    /*!
     * \brief Returns the corresponding member-data.
     */
    virtual
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    get_target_cloud_depth_ptr() const;

    /*!
     * \brief Returns the corresponding member-data.
     */
    virtual
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    get_source_cloud_depth_ptr() const;

    /*!
     * \brief Converts the target-keypoints into the corresponding pointcloud.
     * \param[in]   adjust  indicates whether adjust the pose or not.
     * \note The \ref estimate_correspondences_ptr should be called before in order
     * to generate \ref the target_keypoints_2D_pixels (used in this method).
     * \sa get_target_cloud_keypoints.
     */
    virtual
    inline
    void
    generate_target_cloud_keypoints( bool adjust=false );

    /*!
     * \brief Converts the source-keypoints into the corresponding pointcloud.
     * \param[in]   adjust  indicates whether adjust the pose or not.
     * \note The \ref estimate_correspondences_ptr should be called before in order
     * to generate \ref the source_keypoints_2D_pixels (used in this method).
     * \sa get_source_cloud_keypoints.
     */
    virtual
    inline
    void
    generate_source_cloud_keypoints( bool adjust=false );

    /*!
     * \brief Converts the target depth-image into the corresponding pointcloud.
     * \param[in]   adjust  indicates whether adjust the pose or not.
     * \sa get_target_cloud_depth.
     */
    virtual
    inline
    void
    generate_target_cloud_depth( bool adjust=false );

    /*!
     * \brief Converts the source depth-image into the corresponding pointcloud.
     * \param[in]   adjust  indicates whether adjust the pose or not.
     * \sa get_source_cloud_depth.
     */
    virtual
    inline
    void
    generate_source_cloud_depth( bool adjust=false );


    //--------------------------------------------------------------------------
    //     T r a n s f o r m a t i o n   E s t i m a t i o n   3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Initialize the \ref transformation_estimator_3D_ptr according to the
     * \ref config data.
     * \return 'true' if succeeds, 'false' otherwise.
     */
    virtual
    bool
    init_transformation_estimator_3D();


    virtual
    Eigen::Matrix4f
    get_transformation_3D() const;
    //!< Returns the \ref transformation_3D.

    /*!
     * \brief Estimates the transfation according to the method initialized with.
     * \note The \ref init_transformation_estimator_3D_ptr should be called before
     * at least once.
     * \note The correspondences which this method uses is dependent on the
     * \ref RegistrationPipelineConfig::transformation_estimation_3D_correspondences_to_use
     * config data.
     * \sa get_transformation_3D and RegistrationPipelineConfig::transformation_estimation_3D_correspondences_to_use.
     */
    virtual
    void
    estimate_transformation_3D();

    /*!
     * \brief The standalone version of the \ref estimate_transformation_3D.
     * \note The \ref init_transformation_estimator_3D_ptr should be called before
     * at least once.
     * \return The transformation matrix.
     */
    virtual
    Eigen::Matrix4f
    estimate_transformation_3D_standalone(
            const pcl::PointCloud<pcl::PointXYZ>::Ptr& target
            , const pcl::PointCloud<pcl::PointXYZ>::Ptr& source
            , const pcl::CorrespondencesPtr& corres ) const;


    //--------------------------------------------------------------------------
    //     F i n e   R e g i s t r a t i o n   3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Initialize the \ref icp_ptr according to the \ref config data.
     * \return 'true' if succeeds, 'false' otherwise.
     */
    virtual
    bool
    init_icp_ptr();


    virtual
    Eigen::Matrix4f
    get_icp_transformation_3D() const;
    //!< Returns the \ref icp_transformation_3D.

    /*!
     * \brief Estimates the transfation according to the method initialized with.
     * \param[out]  has_converged   indicates whether the algorithm has conveged
     * or not.
     * \param[out]  fitness_score   contains the fitness score in terms of convegence
     * , meaningless otherwise.
     * \param[in]   apply_on_keypoints      'true' means to apply it only on the
     * keypoints pointcloud (\ref target_cloud_keypoints_ptr and \ref
     * source_cloud_keypoints_ptr) instead of full pointclouds.
     * \param[in]   apply_on_pointclouds    'true' means to apply it on the input
     * pointclouds (\ref target_cloud_ptr and \ref source_cloud_ptr), and 'false'
     * means to apply it on corresponding pointclouds of the input depth-images
     * (\ref target_cloud_depth_ptr and \ref source_cloud_depth_ptr).
     * \param[in]   apply_transformation    'true' means to use the \ref
     * transformation_3D as an initial guess for the ICP algorithm.
     * \note The \ref init_icp_ptr should be called before (at least once).
     * \return The transformed pointcloud.
     * \sa get_icp_transformation_3D.
     */
    virtual
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    estimate_icp_transformation_3D( bool &has_converged
                                    , double &fitness_score
                                    , bool apply_on_keypoints = true
                                    , bool apply_on_pointclouds = true
                                    , bool apply_transformation = true );
//    put the ! back
//    /* ! <<<
//     * \brief The standalone version of the \ref estimate_transformation_3D.
//     * \note The \ref init_transformation_estimator_3D_ptr should be called before
//     * at least once.
//     * \return The transformation matrix.
//     */
//    virtual
//    Eigen::Matrix4f
//    estimate_icp_transformation_3D_standalone(
//            const pcl::PointCloud<pcl::PointXYZ>::Ptr& target
//            , const pcl::PointCloud<pcl::PointXYZ>::Ptr& source
//            , const pcl::CorrespondencesPtr& corres ) const;



protected:

    /*!
     * \brief To provide thread-safeness.
     */
    mutable boost::shared_mutex shared_mut;

    /*!
     * \brief Remindes whether the module is connected to an external logger or not.
     */
    bool has_logger;

    /*!
     * \brief A universal <B>external</B> logger (smart pointer).
     */
    log::Logger::Ptr logger_ptr;

    /*!
     * \brief Indicates whether the module is ready to use or not.
     */
    bool initialized;

    //--------------------------------------------------------------------------
    //      i n p u t   d a t a   2 D / 3 D
    //--------------------------------------------------------------------------

    cv::Mat target_color;
    //!< Contains the target color-image.

    cv::Mat target_depth;
    //!< Contains the target depth-image.

    cv::Mat source_color;
    //!< Contains the source color-image.

    cv::Mat source_depth;
    //!< Contains the source depth-image.

    /*!
     * \brief Refers to the input target-pointcloud (e.g. the pointcloud obtained
     * from a 3D synthetic model).
     * \note This is different from the \ref target_cloud_depth_ptr.
     */
    pcl::PointCloud<pcl::PointXYZ>::Ptr target_cloud_ptr;

    /*!
     * \brief Refers to the input source-pointcloud (e.g. the pointcloud obtained
     * from a 3D synthetic model).
     * \note This is different from the \ref source_cloud_depth_ptr.
     */
    pcl::PointCloud<pcl::PointXYZ>::Ptr source_cloud_ptr;


    //--------------------------------------------------------------------------
    //      F e a t u r e   D e t e c t o r   2 D
    //--------------------------------------------------------------------------
    /*!
     * \brief Contains the 2D feature detector.
     *
     * Currently, the supported feature-detectors are
     * FAST, STAR, SIFT, SURF, ORB, BRISK,MSER, GFTT, HARRIS,Dense, SimpleBlob
     * (refer to the
     * <a href="http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_feature_detectors.html#featuredetector-create">OpenCV</a>
     * documentation).
     */
    cv::Ptr<cv::FeatureDetector> detector_2D_ptr;

    std::vector<cv::KeyPoint> target_keypoints_2D;
    //!< Collection of keypoints detected in the target image.

    std::vector<cv::KeyPoint> source_keypoints_2D;
    //!< Collection of keypoints detected in the source image.

    //--------------------------------------------------------------------------
    //      F e a t u r e   D e s c r i p t o r   2 D
    //--------------------------------------------------------------------------
    /*!
     * \brief Contains the 2D feature descriptor.
     *
     * Currently, the supported feature-descriptors are
     * SIFT, SURF, BRIEF, BRISK, ORB, FREAK
     * (refer to the
     * <a href="http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_extractors.html#descriptorextractor-create">OpenCV</a>
     * documentation).
     */
    cv::Ptr<cv::DescriptorExtractor> descriptor_2D_ptr;

    cv::Mat target_descriptors_2D;
    //!< Contains described features for the keypoints of the target image.

    cv::Mat source_descriptors_2D;
    //!< Contains described features for the keypoints of the source image.


    //--------------------------------------------------------------------------
    //      F e a t u r e   M a t c h e r   2 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Contains the matcher module for matching two sets of descriptors.
     *
     * Currently, the supported descriptors-matchers are
     * BruteForce (it uses L2 ), BruteForce-L1, BruteForce-Hamming,
     * BruteForce-Hamming(2), FlannBased
     * (refer to the
     * <a href="http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_matchers.html#descriptormatcher-create">OpenCV</a>
     * documentation).
     */
    cv::Ptr<cv::DescriptorMatcher> matcher_2D_ptr;

    std::vector<cv::DMatch> filtered_matches_2D;
    //!< Contains filtered matches of target and source descriptors.


    //--------------------------------------------------------------------------
    //      A f f i n e   2 D
    //      P e r s p e c t i v e
    //      H o m o g r a p h y   2 D
    //      F u n d a m e n t a l   M a t r i x   2 D
    //      R i g i d   T r a n s f o r m a t i o n   2 D
    //--------------------------------------------------------------------------

    std::vector<cv::Point2f> target_points_2D;
    //!< Converted target points (from keypoints).

    std::vector<cv::Point2f> source_points_2D;
    //!< Converted source points (from keypoints).



    /*!
     * \brief rigid_transformation_source_to_target_2D is a matrix representing
     * an optimal affine transform [A|b] (a 2 x 3 floating-point matrix) that
     * approximates best the affine transformation between source and target
     * (maps source to target, M.source~target, M = rigid transform matrix).
     */
    cv::Mat rigid_transformation_source_to_target_2D;

    /*!
     * \brief A version of \ref rigid_transformation_source_to_target_2D containing
     * the rigid transformation matrix, which is resulted via applying the algorithm
     * directly on input images instead of found keypoints.
     */
    cv::Mat rigid_transformation_direct_source_to_target_2D;

    /*!
     * \brief F_source_to_target_2D is a matrix representing the affine transform
     * mapping source pixels to target ones, A.source~target.
     */
    cv::Mat A_source_to_target_2D;

    /*!
     * \brief F_source_to_target_2D is a matrix representing the perspective
     * transform mapping source pixels to target ones, P.source~target.
     */
    cv::Mat P_source_to_target_2D;

    /*!
     * \brief F_source_to_target_2D is a fundamental matrix from the corresponding
     * points in two images (mapping source pixels to target ones, F.source~target).
     */
    cv::Mat F_source_to_target_2D;

    /*!
     * \brief E_source_to_target_2D is a essential matrix from the corresponding
     * points in two images (mapping source pixels to target ones, E.source~target).
     */
    cv::Mat E_source_to_target_2D;

    /*!
     * \brief H_source_to_target_2D is the homography matrix between two
     * planes (mapping source pixels to target ones, H.source~target).
     */
    cv::Mat H_source_to_target_2D;



    std::vector<char> rigid_transformation_based_inliers_mask_2D;
    //!< Keeps the flag for each match indicating whether as inlier(1) or not(0).

    int number_of_rigid_transformation_based_inliers_2D;
    //!< Number of the inliers exists in \ref rigid_transformation_based_inliers_mask_2D.


    std::vector<char> rigid_transformation_direct_based_inliers_mask_2D;
    //!< Keeps the flag for each match indicating whether as inlier(1) or not(0).

    int number_of_rigid_transformation_direct_based_inliers_2D;
    //!< Number of the inliers exists in \ref rigid_transformation_direct_based_inliers_mask_2D.


    std::vector<char> affine_based_inliers_mask_2D;
    //!< Keeps the flag for each match indicating whether as inlier(1) or not(0).

    int number_of_affine_based_inliers_2D;
    //!< Number of the inliers exists in \ref affine_based_inliers_mask_2D.


    std::vector<char> perspective_based_inliers_mask_2D;
    //!< Keeps the flag for each match indicating whether as inlier(1) or not(0).

    int number_of_perspective_based_inliers_2D;
    //!< Number of the inliers exists in \ref perspective_based_inliers_mask_2D.


    std::vector<char> fundamental_matrix_based_inliers_mask_2D;
    //!< Keeps the flag for each match indicating whether as inlier(1) or not(0).

    int number_of_fundamental_matrix_based_inliers_2D;
    //!< Number of the inliers exists in \ref fundamental_matrix_based_inliers_mask_2D.


    std::vector<char> essential_matrix_based_inliers_mask_2D;
    //!< Keeps the flag for each match indicating whether as inlier(1) or not(0).

    int number_of_essential_matrix_based_inliers_2D;
    //!< Number of the inliers exists in \ref essential_matrix_based_inliers_mask_2D.


    std::vector<char> homography_based_inliers_mask_2D;
    //!< Keeps the flag for each match indicating whether as inlier(1) or not(0).

    int number_of_homography_based_inliers_2D;
    //!< Number of the inliers exists in \ref homography_based_inliers_mask_2D.


    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   2 D / 3 D
    //--------------------------------------------------------------------------

    pcl::CorrespondencesPtr correspondences_ptr;
    //!< Contains the correspondences between the target and source keypoints.

    std::vector<cv::Point> target_keypoints_2D_pixels;
    //!< Keeps the rounded pixels for the \ref target_keypoints_2D.

    std::vector<cv::Point> source_keypoints_2D_pixels;
    //!< Keeps the rounded pixels for the \ref source_keypoints_2D.


    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   3 D
    //--------------------------------------------------------------------------

    pcl::CorrespondencesPtr correspondences_3D_ptr;
    //!< Contains the correspondences between the keypoints clouds via 3D algorithms.

    pcl::CorrespondencesPtr correspondences_3D_reciprocal_ptr;
    //!< Contains inlier correspondences based on 'reciprocal' rejection method.


    /*!
     * \brief correspondences_remaining_ptr contains the remaining correspondences
     * between the keypoints clouds after applying rejection algorithms selected
     * based on the configuration data of
     * \ref RegistrationPipelineConfig::transformation_estimation_3D_correspondences_to_use.
     */
    pcl::CorrespondencesPtr correspondences_remaining_ptr;


    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   R e j e c t i o n
    //      ( i n l i e r s    3 D )
    //
    //      from PCL library:
    //          - distance
    //          - features
    //          - median_distance
    //          - one_to_one
    //          - organized_boundary
    //          - poly
    //          - sample_consensus
    //          - sample_consensus_direct
    //          - surface_normal
    //          - trimmed
    //          - var_trimmed
    //--------------------------------------------------------------------------

    pcl::CorrespondencesPtr correspondences_3D_distance_ptr;
    //!< Contains inlier correspondences based on 'distance' rejection method.

    pcl::CorrespondencesPtr correspondences_3D_features_ptr;
    //!< Contains inlier correspondences based on 'features' rejection method.

    pcl::CorrespondencesPtr correspondences_3D_median_distance_ptr;
    //!< Contains inlier correspondences based on 'median distance' rejection method.

    pcl::CorrespondencesPtr correspondences_3D_one_to_one_ptr;
    //!< Contains inlier correspondences based on 'one to one' rejection method.

    pcl::CorrespondencesPtr correspondences_3D_organized_boundary_ptr;
    //!< Contains inlier correspondences based on 'organized boundary' rejection method.

    pcl::CorrespondencesPtr correspondences_3D_poly_ptr;
    //!< Contains inlier correspondences based on 'poly' rejection method.

    pcl::CorrespondencesPtr correspondences_3D_sample_consensus_ptr;
    //!< Contains inlier correspondences based on 'sample consensus' rejection method.

    Eigen::Matrix4f sample_consensus_best_transformation;
    //!< The best transformation calculated via the \ref reject_correspondences_sample_consensus method.

    pcl::CorrespondencesPtr correspondences_3D_surface_normal_ptr;
    //!< Contains inlier correspondences based on 'surface normal' rejection method.

    pcl::CorrespondencesPtr correspondences_3D_trimmed_ptr;
    //!< Contains inlier correspondences based on 'trimmed' rejection method.

    pcl::CorrespondencesPtr correspondences_3D_var_trimmed_ptr;
    //!< Contains inlier correspondences based on 'var trimmed' rejection method.


    //--------------------------------------------------------------------------
    //     C o n v e r t   2 D   f e a t u r e s   t o   p o i n t c l o u d
    //--------------------------------------------------------------------------

    pcl::PointCloud<pcl::PointXYZ>::Ptr target_cloud_keypoints_ptr;
    //!< Keeps the corresponding pointcloud of converted 2D-features (form \ref target_keypoints_2D).

    pcl::PointCloud<pcl::PointXYZ>::Ptr source_cloud_keypoints_ptr;
    //!< Keeps the corresponding pointcloud of converted 2D-features (from \ref source_keypoints_2D).

    pcl::PointCloud<pcl::PointXYZ>::Ptr target_cloud_depth_ptr;
    //!< Keeps the corresponding pointcloud of the target depth-image (\ref target_depth).

    pcl::PointCloud<pcl::PointXYZ>::Ptr source_cloud_depth_ptr;
    //!< Keeps the corresponding pointcloud of the source depth-image (\ref source_depth).


    //--------------------------------------------------------------------------
    //     T r a n s f o r m a t i o n   E s t i m a t i o n   3 D
    //--------------------------------------------------------------------------

    pcl::registration::TransformationEstimation<pcl::PointXYZ, pcl::PointXYZ>::Ptr transformation_estimator_3D_ptr;
    //!< Estimates the 3D transformation between two pointclouds.

    Eigen::Matrix4f transformation_3D;
    //!< The resulted 3D transformation obtained via an estimator (such as \ref transformation_estimator_3D_ptr).


    //--------------------------------------------------------------------------
    //     F i n e   R e g i s t r a t i o n   3 D
    //--------------------------------------------------------------------------

    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ>::Ptr icp_ptr;
    //!< Refers to an ICP algorithm defined via \ref config (icp_setting).

    Eigen::Matrix4f icp_transformation_3D;
    //!< The resulted 3D transformation obtained via the \ref icp_ptr.

};

// *****************************************************************************
// *****************************************************************************

} // end of namespace registration
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
