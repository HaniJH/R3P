// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "Registration/BoxPyramidPair.hpp"

#include <sstream>

namespace hjh {
namespace registration {

// *****************************************************************************
// *********************         G L O B A L S       ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************      P R O T O T Y P E S    ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************     D E F I N I T I O N S   ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

BoxPyramidPair::BoxPyramidPair()
    : flag_valid( false )
    , box( -1 )
    , pyramid( -1 )
    , score( -1 )
    , number_of_matches( 0)
    , transform( hjh::Pose() )
    , transform_ICP( hjh::Pose() )
    , comparison_mode( COMPARE_SCORE_X_MATCHES )
{
    // empty
}

//==============================================================================
//==============================================================================

BoxPyramidPair::BoxPyramidPair( const BoxPyramidPair &obj )
    : flag_valid( obj.flag_valid )
    , box( obj.box )
    , pyramid( obj.pyramid )
    , score( obj.score )
    , number_of_matches( obj.number_of_matches )
    , transform( obj.transform )
    , transform_ICP( obj.transform_ICP )
    , comparison_mode( obj.comparison_mode )
{
    // empty
}

//==============================================================================
//==============================================================================

BoxPyramidPair::BoxPyramidPair( bool isValid_
                , int which_box_
                , int which_pyramid_
                , double score_
                , int num_matches_
                , hjh::Pose transform_
                , hjh::Pose transform_ICP_
                , ComparisonMode mode_ )
    : flag_valid( isValid_ )
    , box( which_box_ )
    , pyramid( which_pyramid_ )
    , score( score_ )
    , number_of_matches( num_matches_ )
    , transform( transform_ )
    , transform_ICP( transform_ICP_ )
//    , comparison_mode( mode_ )
{
    set_comparison_mode( mode_ );
}

//==============================================================================
//==============================================================================

BoxPyramidPair::~BoxPyramidPair()
{
    // empty
}

//==============================================================================
//==============================================================================

BoxPyramidPair&
BoxPyramidPair::operator=( const BoxPyramidPair &obj )
{
    if ( this != &obj )
    {
        this->flag_valid = obj.flag_valid;
        this->box = obj.box;
        this->pyramid = obj.pyramid;
        this->score = obj.score;
        this->number_of_matches = obj.number_of_matches;
        this->transform = obj.transform;
        this->transform_ICP = obj.transform_ICP;
        this->comparison_mode = obj.comparison_mode;
    }

    // for cascaded assignments.
    return *this;
}

//==============================================================================
//==============================================================================

//inline bool operator==(const X& lhs, const X& rhs){ /* do actual comparison */ }
//inline bool operator!=(const X& lhs, const X& rhs){return !operator==(lhs,rhs);}
//inline bool operator< (const X& lhs, const X& rhs){ /* do actual comparison */ }
//inline bool operator> (const X& lhs, const X& rhs){return  operator< (rhs,lhs);}
//inline bool operator<=(const X& lhs, const X& rhs){return !operator> (lhs,rhs);}
//inline bool operator>=(const X& lhs, const X& rhs){return !operator< (lhs,rhs);}


bool
BoxPyramidPair::operator==( const BoxPyramidPair &rhs ) const
{
    return ( rhs.score==score && rhs.number_of_matches==number_of_matches );
}

//==============================================================================
//==============================================================================

bool
BoxPyramidPair::operator!=( const BoxPyramidPair &rhs ) const
{
    return !( *this == rhs );
}

//==============================================================================
//==============================================================================

bool
BoxPyramidPair::operator<( const BoxPyramidPair &rhs ) const
{
    // the lower the fitness score is, the better is the registration.
    switch( comparison_mode )
    {
    case COMPARE_SCORE: return this->score > rhs.score;
    case COMPARE_MATCHES: return this->number_of_matches < rhs.number_of_matches;
    case COMPARE_SCORE_X_MATCHES:
        return ( this->number_of_matches / this->score
                 <
                 rhs.number_of_matches / rhs.score );
    default:
        return false; // never happens due to set_comparison_mode().
    }
    return false;
}

//==============================================================================
//==============================================================================

bool
BoxPyramidPair::operator<=( const BoxPyramidPair &rhs ) const
{
    return !( *this > rhs );
}

//==============================================================================
//==============================================================================

bool
BoxPyramidPair::operator>( const BoxPyramidPair &rhs ) const
{
    return rhs < *this;
}

//==============================================================================
//==============================================================================

bool
BoxPyramidPair::operator>=( const BoxPyramidPair &rhs ) const
{
    return !( *this < rhs );
}

//==============================================================================
//==============================================================================

bool
BoxPyramidPair::is_valid() const
{
    return flag_valid;
}

//==============================================================================
//==============================================================================

bool
BoxPyramidPair::get_flag_valid() const
{
    return flag_valid;
}

//==============================================================================
//==============================================================================

void
BoxPyramidPair::set_flag_valid( bool flag_ )
{
    flag_valid = flag_;
}

//==============================================================================
//==============================================================================

int
BoxPyramidPair::get_box() const
{
    return box;
}

//==============================================================================
//==============================================================================

void
BoxPyramidPair::set_box( int box_ )
{
    box = box_;
}

//==============================================================================
//==============================================================================

int
BoxPyramidPair::get_pyramid() const
{
    return pyramid;
}

//==============================================================================
//==============================================================================

void
BoxPyramidPair::set_pyramid( int pyramid_ )
{
    pyramid = pyramid_;
}

//==============================================================================
//==============================================================================

double
BoxPyramidPair::get_score() const
{
    return score;
}

//==============================================================================
//==============================================================================

void
BoxPyramidPair::set_score( double score_ )
{
    score = score_;
}

//==============================================================================
//==============================================================================

int
BoxPyramidPair::get_number_of_matches() const
{
    return number_of_matches;
}

//==============================================================================
//==============================================================================

void
BoxPyramidPair::set_number_of_matches( int num_matches_ )
{
    number_of_matches = num_matches_;
}

//==============================================================================
//==============================================================================

hjh::Pose
BoxPyramidPair::get_transform() const
{
    return transform;
}

//==============================================================================
//==============================================================================

void
BoxPyramidPair::set_transform( hjh::Pose Rt )
{
    transform = Rt;
}

//==============================================================================
//==============================================================================

hjh::Pose
BoxPyramidPair::get_transform_ICP() const
{
    return transform_ICP;
}

//==============================================================================
//==============================================================================

void
BoxPyramidPair::set_transform_ICP( hjh::Pose Rt )
{
    transform_ICP = Rt;
}

//==============================================================================
//==============================================================================

BoxPyramidPair::ComparisonMode
BoxPyramidPair::get_comparison_mode() const
{
    return comparison_mode;
}

//==============================================================================
//==============================================================================

void
BoxPyramidPair::set_comparison_mode( ComparisonMode mode_ )
{
    switch( mode_ )
    {
    case COMPARE_SCORE:
    case COMPARE_MATCHES:
    case COMPARE_SCORE_X_MATCHES:
        comparison_mode = mode_;
    default:
        comparison_mode = COMPARE_SCORE_X_MATCHES;
    }
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<( std::ostream& os, const BoxPyramidPair& obj )
{
    os
       << "       flag_valid : " << (obj.flag_valid?"YES":"NO")
       << "\n              box : " << obj.box
       << "\n          pyramid : " << obj.pyramid
       << "\n            score : " << obj.score
       << "\nnumber_of_matches : " << obj.number_of_matches
       << "\n        transform :\n" << obj.transform.translation
       << "\n" << obj.transform.rotation
       << "\n    transform_ICP :\n" << obj.transform_ICP.translation
       << "\n" << obj.transform_ICP.rotation
       << "\n  comparison_mode : " << obj.comparison_mode
       ;
    return os;
}

//==============================================================================
//==============================================================================

} // end of namespace registration
} // end of namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
