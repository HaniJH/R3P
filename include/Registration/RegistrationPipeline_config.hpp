
#ifndef _REGISTRATION_PIPELINE_CONFIG_HPP_20150218_
#define _REGISTRATION_PIPELINE_CONFIG_HPP_20150218_
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "boost_serialization.hpp"

#include "Configuration/base_config.hpp"
#include "haniDefinitions.hpp"

#include <opencv2/core/core.hpp>        // OpenCV serialization
#include <stdint.h>

namespace hjh {
namespace registration {

//==============================================================================
//==============================================================================

// default config filename
const std::string default_filename_registration_pipeline_config =
        "registration_pipeline_config.xml";

// *****************************************************************************
// *********************     E N U E R A T I O N S   ***************************
// *****************************************************************************

/*!
 * \brief Various levels of a 3D data (e.g. pointcloud type).
 */
enum DATA_TYPE_3D : uint8_t
{
    DT3D_DEFAULT               = 0x00   //!< Default behavior (per application).
    , ONLY_3D_COORDINATES      = 0X01   //!< Contains only XYZ values.
    , INTENSITY_3D_COORDINATES = 0X02   //!< XYZ values and conrresponding intensity.
    , COLOR_3D_COORDINATES     = 0x04   //!< XYZ values and conrresponding color.
};


/*!
 * \brief Various method of downsampling (mostly for 3D data).
 */
enum DOWNSAMPLING_METHOD : uint8_t
{
    NO_DOWNSAMPLING           = 0X00    //!< No downsampling.
    , DM_DEFAULT              = 0X01    //!< Default behavior (per application).
    , RANDOM_DOWNSAMPLING     = 0X02    //!< Keep part of cloud randomly.
    , OCTREE_DOWNSAMPLING     = 0X04    //!< Re-structure the cloud by an oc-tree.
    , VOXELIZING_DOWNSAMPLING = 0x08    //!< Voxelizing the cloud.
};


/*!
 * \brief Various methods to find neighbours in 3D (e.g. for normal computation).
 */
enum NEIGHBOUR_METHOD : uint8_t
{
    NO_NEIGHBOUR    = 0x00  //!< No neighbour finding.
    , NM_DEFAULT    = 0x01  //!< Default behavior (per application).
    , KD_TREE       = 0X02  //!< Only uses kd-tree.
    , KD_TREE_FLANN = 0X04  //!< Uses kd-tree and FLANN.
};


/*!
 * \brief Various methods to extract 3D keypoints.
 */
enum KEYPOINT_3D_METHOD : uint8_t
{
    NO_KEYPOINT_3D = 0X00   //!< Means "do not extract 3D keypoints".
    , KP3D_DEFAULT = 0X01   //!< Default behavior (per application).
    , ISS_3D       = 0X02   //!< ISS in 3D.
    , SIFT_3D      = 0X03   //!< SIFT in 3D.
    , HARRIS_3D    = 0X04   //!< Harris in 3D.
    , TOMASI_3D    = 0X05   //!< Tomasi in 3D.
    , NOBLE_3D     = 0X06   //!< Noble in 3D.
    , LOWE_3D      = 0X07   //!< Lowe in 3D.
    , CURVATURE_3D = 0X08   //!< Curvature in 3D.
};


// /*!
// * \brief Various methods to extract 2D keypoints.
// *
// * \todo Add more.
// */
//enum KEYPOINT_2D_METHOD : uint8_t
//{
//    NO_KEYPOINT_2D = 0X00   //!< Means "do not extract 2D keypoints".
//    , KP2D_DEFAULT = 0X01   //!< Default behavior (per application).
//    , SIFT_2D_KP   = 0X02   //!< SIFT in 2D.
//    , SURF_2D_KP   = 0X03   //!< SURF in 2D.
//};


/*!
 * \brief Various 3D descriptors.
 */
enum DESCRIPTOR_3D : uint8_t
{
    NO_DESCRIPTOR_3D = 0X00 //!< Means "do not use any 3D descriptor".
    , DES3D_DEFAULT  = 0X01 //!< Default behavior (per application).
    , PFH            = 0X02 //!< Point feature histogram.
    , FPFH           = 0X03 //!< Fast point feature histogram.
    , SHOT_RGB       = 0X04 //!< Shot with RGB.
    , PFH_RGB        = 0X05 //!< Point feature histogram and RGB.
};


// /*!
// * \brief Various 2D descriptors.
// *
// * \todo add more.
// */
//enum DESCRIPTOR_2D : uint8_t
//{
//    NO_DESCRIPTOR_2D = 0X00 //!< Means "do not use any 2D descriptor".
//    , DES2D_DEFAULT  = 0X01 //!< Default behavior (per application).
//    , SIFT_2D_DES    = 0X02 //!< SIFT in 2D.
//    , SURF_2D_DES    = 0X03 //!< SURF in 2D.
//};


/*!
 * \brief Various <B>flags<\B> to enable alignment-process steps.
 *
 * \note these flags could be used together.
 * \todo add more.
 */
enum ALIGNMENT_STEPS : uint8_t
{
    AS_NONE                  = 0x00 //!< all flags disabled.
    , AS_DEFAULT             = 0X01 //!< default behavior (per application).
    , WITH_CORRESPONDENCE    = 0X02 //!< to enable correspondence creation.
    , OUTLIER_REMOVAL        = 0X03 //!< to remove outliers (e.g. keypoints).
    , SAC_BASED              = 0X04 //!< to perform SAC-based registration.
    , ICP_FINE_REGISTRATION  = 0X05 //!< to enable ICP registration.
};


// *****************************************************************************
// *********************         C L A S S E S       ***************************
// *****************************************************************************


//==============================================================================
//      d o w n s a m p l i n g   p a r a m e t e r s
//==============================================================================

/*!
 * \brief The DownsamplingParameters class contains all the parameters needed
 * for downsampling a piece of data (e.g. a pointcloud).
 */
class DownsamplingParameters
{
public:
    DownsamplingParameters();
    //!< Default constructor.

    DownsamplingParameters( double percentage_, int octree_level_, double voxel_size_ );
    //!< Constructor.

    virtual
    ~DownsamplingParameters();
    //!< Destructor.


    typedef boost::shared_ptr<DownsamplingParameters> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const DownsamplingParameters> ConstPtr;
    //!< Constant shared pointer.


    friend std::ostream&
    operator<<( std::ostream& os, const DownsamplingParameters& obj );
    //!< Overloaded output-stream.


    virtual
    void
    write( cv::FileStorage& fs ) const;
    //!< OpenCV serialization for this class (write).

    virtual
    void
    read( const cv::FileNode& node );
    //!< OpenCV serialization for this class (read).


    virtual
    void
    set_percentage( double percentage_ );
    //!< Sets corresponding data member.

    virtual
    double
    get_percentage() const;
    //!< Returns corresponding data member.

    virtual
    void
    set_octree_level( int octree_level_ );
    //!< Sets corresponding data member.

    virtual
    int
    get_octree_level() const;
    //!< Returns corresponding data member.

    virtual
    void
    set_voxel_size( double voxel_size_ );
    //!< Sets corresponding data member.

    virtual
    double
    get_voxel_size() const;
    //!< Returns corresponding data member.


protected:

    /*!
     * \brief percentage_ is a number in [0, 1] indicating the percentage to
     * keep the data (e.g. random sampling).
     */
    double percentage;

    /*!
     * \brief octree_level_ represents the level for the corresponding oc-tree.
     */
    int octree_level;

    /*!
     * \brief voxel_size_ indicates the size of a cubic voxel (used in both
     * octree and voxelizing methods for downsampling).
     */
    double voxel_size;
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static
void
write( cv::FileStorage& fs, const std::string&, const DownsamplingParameters& x)
{
    x.write( fs );
}

static
void
read( const cv::FileNode& node, DownsamplingParameters& x
      , const DownsamplingParameters& default_value = DownsamplingParameters() )
{
    if( node.empty() )
        x = default_value;
    else
        x.read( node );
}


//==============================================================================
//      n o r m a l   p a r a m e t e r s
//==============================================================================

/*!
 * \brief The NormalComputingParameters class contains all the parameters needed
 * for computing normals.
 */
class NormalComputingParameters
{
public:
    NormalComputingParameters();
    //!< Default constructor.

    NormalComputingParameters( double search_radious );
    //!< Constructor.

    virtual
    ~NormalComputingParameters();
    //!< Destructor.


    typedef boost::shared_ptr<NormalComputingParameters> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const NormalComputingParameters> ConstPtr;
    //!< Constant shared pointer.


    friend std::ostream&
    operator<<( std::ostream& os, const NormalComputingParameters& obj );
    //!< Overloaded output-stream.


    virtual
    void
    write( cv::FileStorage& fs ) const;
    //!< OpenCV serialization for this class (write).

    virtual
    void
    read( const cv::FileNode& node );
    //!< OpenCV serialization for this class (read).


    virtual
    void
    set_search_radius( double radius );
    //!< Sets corresponding data member.

    virtual
    double
    get_search_radius() const;
    //!< Returns corresponding data member.


protected:

    double search_radius;
    //!< radius of a sphere around the points to find neighbours.
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static
void
write( cv::FileStorage& fs, const std::string&, const NormalComputingParameters& x)
{
    x.write( fs );
}

static
void
read( const cv::FileNode& node, NormalComputingParameters& x
      , const NormalComputingParameters& default_value = NormalComputingParameters() )
{
    if( node.empty() )
        x = default_value;
    else
        x.read( node );
}



//==============================================================================
//     I C P   C o n f i g
//==============================================================================

/*!
 * \brief The ICP_config class provides the most used settings in PCL::ICP module.
 */
class ICP_config
{
public:

    ICP_config();
    //!< Default constructor.

    virtual
    ~ICP_config();
    //!< Destructor.


    typedef boost::shared_ptr<ICP_config> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const ICP_config> ConstPtr;
    //!< Constant shared pointer.


    friend
    std::ostream&
    operator<<( std::ostream& os, const ICP_config& obj );
    //!< Overloaded output-stream.


    virtual
    void
    write( cv::FileStorage& fs ) const;
    //!< OpenCV serialization for this class (write).

    virtual
    void
    read( const cv::FileNode& node );
    //!< OpenCV serialization for this class (read).


    /*!
     * \brief (Coppied form the PCL doc) The maximum allowed Euclidean error
     * between two consecutive steps in the ICP loop, before the algorithm is
     * considered to have converged.
     */
    double euclidean_fitness_epsilon;

    /*!
     * \brief (Coppied form the PCL doc) The maximum distance threshold between
     * two correspondent points in source <-> target. If the distance is larger
     * than this threshold, the points will be ignored in the alignement process.
     */
    double corr_dist_threshold;

    /*!
     * \brief (Coppied form the PCL doc) The maximum number of iterations the
     * internal optimization should run for.
     */
    int max_iterations;

    /*!
     * \brief (Coppied form the PCL doc) The number of iterations RANSAC should
     * run for.
     */
    int ransac_iterations;

    /*!
     * \brief (Coppied form the PCL doc) The inlier distance threshold for the
     * internal RANSAC outlier rejection loop.
     */
    double inlier_threshold;

    /*!
     * \brief (Coppied form the PCL doc) The maximum difference between two
     * consecutive transformations in order to consider convergence (user defined).
     */
    double transformation_epsilon;


protected:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler \note Phi is excluded.
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static
void
write( cv::FileStorage& fs, const std::string&, const ICP_config& x)
{
    x.write( fs );
}

static
void
read( const cv::FileNode& node, ICP_config& x
      , const ICP_config& default_value = ICP_config() )
{
    if( node.empty() )
        x = default_value;
    else
        x.read( node );
}


//==============================================================================
//      R e g i s t r a t i o n P i p e l i n e C o n f i g
//==============================================================================

/*! \brief all (and only) the settings/configuration of the \ref
 * RegistrationPipeline class.
  */
class RegistrationPipelineConfig : public configuration::BaseConfig
{
public:
    RegistrationPipelineConfig();
    //!< default construtor

    virtual
    ~RegistrationPipelineConfig();
    //!< destructor.


    typedef boost::shared_ptr<RegistrationPipelineConfig> Ptr;
    //!< shared pointer.

    typedef boost::shared_ptr<const RegistrationPipelineConfig> ConstPtr;
    //!< constant shared pointer.


    virtual
    int
    write_config_file( std::string filename ) const;

    virtual
    int
    read_config_file( std::string filename );

    virtual
    void
    display() const;

    //--------------------------------------------------------------------------
    //   p a r a m e t e r s
    //--------------------------------------------------------------------------

    DownsamplingParameters par_downsampling;
    //!< Corresponding parameters for downsampling.

    NormalComputingParameters par_normal;
    //!< Corresponding parameters for computing normal.


    //--------------------------------------------------------------------------
    //      S c a l e ( s )
    //--------------------------------------------------------------------------

    virtual
    inline
    double
    get_depth_image_scale() const;
    //!< Returns the correspoinding data member.

    virtual
    inline
    void
    set_depth_image_scale( double scale );
    //!< Sets the correspoinding data member.


    //--------------------------------------------------------------------------
    //      I n t r i n s i c s
    //--------------------------------------------------------------------------

    virtual
    hjh::Intrinsics
    get_target_intrinsics_color() const;
    //!< Returns the intrinsics of the target color (RGB) camera.

    virtual
    hjh::Intrinsics
    get_target_intrinsics_depth() const;
    //!< Returns the intrinsics of the target depth (IR) camera.


    virtual
    hjh::Intrinsics
    get_source_intrinsics_color() const;
    //!< Returns the intrinsics of the source color (RGB) camera.

    virtual
    hjh::Intrinsics
    get_source_intrinsics_depth() const;
    //!< Returns the intrinsics of the source depth (IR) camera.


    virtual
    void
    set_target_intrinsics_color( hjh::Intrinsics intr_ );
    //!< Sets the intrinsics for the target color (RGB) camera.

    virtual
    void
    set_target_intrinsics_depth( hjh::Intrinsics intr_ );
    //!< Sets the intrinsics for the target depth (IR) camera.

    virtual
    void
    set_source_intrinsics_color( hjh::Intrinsics intr_ );
    //!< Sets the intrinsics for the source color (RGB) camera.

    virtual
    void
    set_source_intrinsics_depth( hjh::Intrinsics intr_ );
    //!< Sets the intrinsics for the source depth (IR) camera.


    //--------------------------------------------------------------------------
    //      F e a t u r e   D e t e c t o r   2 D
    //--------------------------------------------------------------------------

    virtual
    void
    set_feature_detector_2D_type( std::string type_ );
    //!< Sets the corresponding data-member.

    virtual
    std::string
    get_feature_detector_2D_type() const;
    //!< Returns the corresponding data-member.

    //--------------------------------------------------------------------------
    //      F e a t u r e   D e s c r i p t o r   2 D
    //--------------------------------------------------------------------------

    virtual
    void
    set_feature_descriptor_2D_type( std::string type_ );
    //!< Sets the corresponding data-member.

    virtual
    std::string
    get_feature_descriptor_2D_type() const;
    //!< Returns the corresponding data-member.

    //--------------------------------------------------------------------------
    //      F e a t u r e   M a t c h e r   2 D
    //--------------------------------------------------------------------------

    virtual
    void
    set_descriptor_matcher_2D_type( std::string type_ );
    //!< Sets the corresponding data-member.

    virtual
    std::string
    get_descriptor_matcher_2D_type() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_knn_number_of_matches( int knn_ );
    //!< Sets the corresponding data-member.

    virtual
    int
    get_knn_number_of_matches() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_matching_filter_2D_type( std::string type_ );
    //!< Sets the corresponding data-member.

    virtual
    std::string
    get_matching_filter_2D_type() const;
    //!< Returns the corresponding data-member.

    //--------------------------------------------------------------------------
    //      I n l i e r s   2 D
    //--------------------------------------------------------------------------

    virtual
    double
    get_inliers_max_threshold() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_inliers_max_threshold( double threshold_ );
    //!< Sets the corresponding data-member.

    virtual
    std::string
    get_inliers_finding_method() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_inliers_finding_method( std::string method_ );
    //!< Sets the corresponding data-member.




    //--------------------------------------------------------------------------
    //      R i g i d   T r a n s f o r m a t i o n
    //--------------------------------------------------------------------------

    virtual
    bool
    get_flag_full_affine() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_flag_full_affine( bool flag_ );
    //!< Sets the corresponding data-member.


    //--------------------------------------------------------------------------
    //      H o m o g r a p h y   2 D
    //--------------------------------------------------------------------------

    virtual
    double
    get_ransac_reprojection_threshold_2D() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_ransac_reprojection_threshold_2D( double threshold_ );
    //!< Sets the corresponding data-member.

    virtual
    std::string
    get_homography_2D_method() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_homography_2D_method( std::string method_ );
    //!< Sets the corresponding data-member.


    //--------------------------------------------------------------------------
    //      F u n d a m e n t a l   M a t r i x   2 D
    //--------------------------------------------------------------------------

    virtual
    double
    get_ransac_max_distance_threshold_2D() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_ransac_max_distance_threshold_2D( double threshold_ );
    //!< Sets the corresponding data-member.

    virtual
    double
    get_confidence_probability() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_confidence_probability( double confidence_probability_ );
    //!< Sets the corresponding data-member.

    virtual
    std::string
    get_fundamental_matrix_2D_method() const;
    //!< Returns the corresponding data-member.

    virtual
    void
    set_fundamental_matrix_2D_method( std::string method_ );
    //!< Sets the corresponding data-member.


    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   2 D / 3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    std::string
    get_correspondence_base() const;

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondence_base( std::string base_ );


    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    float
    get_registration_distance_threshold() const;


    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_registration_distance_threshold( float threshold_ );

    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   R e j e c t i o n
    //      ( i n l i e r s    3 D )
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    double
    get_correspondences_rejection_distant_threshold() const;


    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_distant_threshold( double threshold_ );

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    double
    get_correspondences_rejection_median_factor() const;


    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_median_factor( double factor_ );


    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    float
    get_correspondences_rejection_similarity_threshold() const;

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    int
    get_correspondences_rejection_polygon_cardinality() const;

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    int
    get_correspondences_rejection_poly_iterartion() const;

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_similarity_threshold( float threshold_ );

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_polygon_cardinality( int cardinality_ );

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_poly_iterartion( int iteration_ );



    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    double
    get_correspondences_rejection_SAC_inlier_threshold() const;

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    int
    get_correspondences_rejection_SAC_max_iterations() const;

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    bool
    get_correspondences_rejection_SAC_flag_refine() const;

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_SAC_inlier_threshold( double threshold_ );

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_SAC_max_iterations( int iterations_ );

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_SAC_flag_refine( bool flag_ );



    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    float
    get_correspondences_rejection_overlap_ratio() const;

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    unsigned int
    get_correspondences_rejection_min_correspondences() const;

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_overlap_ratio( float ratio_ );

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_min_correspondences( unsigned int correspondences_ );


    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    double
    get_correspondences_rejection_min_ratio() const;

    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    double
    get_correspondences_rejection_max_ratio() const;

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_min_ratio( double ratio_ );

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_correspondences_rejection_max_ratio( double ratio_ );



    //--------------------------------------------------------------------------
    //     T r a n s f o r m a t i o n   E s t i m a t i o n   3 D
    //--------------------------------------------------------------------------


    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    std::string
    get_transformation_estimation_3D_method() const;


    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_transformation_estimation_3D_method( std::string type_ );



    /*!
     * \brief Returns the corresponding data member.
     */
    virtual
    std::string
    get_transformation_estimation_3D_correspondences_to_use() const;

    /*!
     * \brief Sets the corresponding data member.
     */
    virtual
    void
    set_transformation_estimation_3D_correspondences_to_use( std::string method_ );


    //--------------------------------------------------------------------------
    //     I C P   3 D
    //--------------------------------------------------------------------------

    virtual
    std::string
    get_icp_type() const;
    //!< Returns the corresponding data member.

    virtual
    void
    set_icp_type( std::string type_ );
    //!< Sets the corresponding data member.

    /*!
     * \brief Keeps the required settings for the \ref RegistrationPipeline::icp_ptr.
     */
    ICP_config icp_settings;

    //--------------------------------------------------------------------------
    //     L o o p - C l o s u r e
    //--------------------------------------------------------------------------

    virtual
    bool
    get_flag_loop_closure() const;
    //!< Returns the corresponding data member.

    virtual
    void
    set_flag_loop_closure( bool flag_ );
    //!< Sets the corresponding data member.

    virtual
    std::string
    get_loop_closure_type() const;
    //!< Returns the corresponding data member.

    virtual
    void
    set_loop_closure_type( std::string type_ );
    //!< Sets the corresponding data member.


protected:

    /*!
     * \brief indicates what kind of 3D data used in this object (to use/prepare
     * appropriate pointers).
     */
    DATA_TYPE_3D data_type_3d_;

    /*!
     * \brief downsampling_method_ indicates the method to downsample the data.
     */
    DOWNSAMPLING_METHOD downsampling_method_;

    /*!
     * \brief indicates which method being to find neighbours around a 3D point.
     */
    NEIGHBOUR_METHOD neighbour_method_;

    /*!
     * \brief indicates which method being to extract 3D keypoints.
     */
    KEYPOINT_3D_METHOD keypoint_3D_method_;

    /*!
     * \brief indicates which method used as a 3D descriptor.
     */
    DESCRIPTOR_3D descriptor_3D_;

    /*!
     * \brief indicates which steps involed in alignment process.
     */
    ALIGNMENT_STEPS alignment_flag_;


    //--------------------------------------------------------------------------
    //      S c a l e ( s )
    //--------------------------------------------------------------------------

    double depth_image_scale;


    //--------------------------------------------------------------------------
    //      I n t r i n s i c s
    //--------------------------------------------------------------------------

    hjh::Intrinsics target_intrinsics_color;
    //!< Keeps the intrinsics for the target color (RGB) camera.

    hjh::Intrinsics target_intrinsics_depth;
    //!< Keeps the intrinsics for the target depth (IR) camera.


    hjh::Intrinsics source_intrinsics_color;
    //!< Keeps the intrinsics for the source color (RGB) camera.

    hjh::Intrinsics source_intrinsics_depth;
    //!< Keeps the intrinsics for the source depth (IR) camera.


    //--------------------------------------------------------------------------
    //      F e a t u r e   D e t e c t o r   2 D
    //--------------------------------------------------------------------------

    std::string feature_detector_2D_type;
    //!< Determines the type for the \ref RegistrationPipeline::detector_ptr.

    //--------------------------------------------------------------------------
    //      F e a t u r e   D e s c r i p t o r   2 D
    //--------------------------------------------------------------------------

    std::string feature_descriptor_2D_type;
    //!< Determines the type for the \ref RegistrationPipeline::descriptor_ptr.

    //--------------------------------------------------------------------------
    //      F e a t u r e   M a t c h e r   2 D
    //--------------------------------------------------------------------------

    std::string descriptor_matcher_2D_type;
    //!< Determines the type for the \ref RegistrationPipeline::matcher_ptr.

    /*!
     * \brief Count of best matches found per each query descriptor or less if a
     * query descriptor has less than k possible matches in total.
     */
    int knn_number_of_matches;

    /*!
     * \brief Defines the filter type for the 2D matching process, and currently,
     * the supported matching-filters are:
     *  - <B>Simple</B>
     *  - <B>CrossCheck</B>.
     */
    std::string matching_filter_2D_type;


    //--------------------------------------------------------------------------
    //      I n l i e r s   2 D
    //--------------------------------------------------------------------------

    /*!
     * \brief inliers_max_threshold is the maximum threshold to determine inliers
     * by the \ref generate_homography_based_inliers_mask_2D
     * , \ref generate_fundamental_matrix_based_inliers_mask_2D
     * , \ref generate_perspective_based_inliers_mask_2D
     * and \ref generate_affine_based_inliers_mask_2D methods.
     */
    double inliers_max_threshold;

    /*!
     * \brief inliers_finding_method defines various methods to determine inliers
     * among matched features between the source and target entities.
     * \note Currently, it includes the followings:
     * - <B>all_as_inliers</B>: it taks all the matched features as inliers.\n
     * - <B>source_to_target</B>: it transforms the source to the target and then
     * keeps the features which are matched after transformation (single-direction).\n
     * - <B>target_to_source</B>: it transforms the target to the source (if possible)
     * and then keeps the features which are matched after transformation (single-direction).\n
     * - <B>source_AND_target</B>: the intersection of the inliers obtained by the
     * <i>source_to_target</i> and <i>target_to_source</i> methods (bidirectioin).\n
     * - <B>source_OR_target</B>: the union of the inliers obtained by the
     * <i>source_to_target</i> and <i>target_to_source</i> methods (bidirectioin).\n
     */
    std::string inliers_finding_method;



    //--------------------------------------------------------------------------
    //      R i g i d   T r a n s f o r m a t i o n
    //--------------------------------------------------------------------------

    /*!
     * \brief flag_full_affine is used in the \ref find_rigid_transformation_2D
     * as follows:
     * If true, the function finds an optimal affine transformation with no
     * additional restrictions (6 degrees of freedom); Otherwise, the class of
     * transformations to choose from is limited to combinations of translation,
     * rotation, and uniform scaling (5 degrees of freedom).
     */
    bool flag_full_affine;

    //--------------------------------------------------------------------------
    //      H o m o g r a p h y   2 D
    //--------------------------------------------------------------------------

    double ransac_reprojection_threshold_2D;
    //!< Maximum accepted reprojection error (maximum inlier distance).

    /*!
     * \brief Method used by OpenCV 'findHomography' method, and currently, the
     * supported methods are:
     *  - <B>RANSAC</B>
     *  - <B>LMEDS</B>.
     */
    std::string homography_2D_method;


    //--------------------------------------------------------------------------
    //      F u n d a m e n t a l   M a t r i x   2 D
    //--------------------------------------------------------------------------

    /*!
     * \brief ransac_max_distance_threshold_2D is the maximum distance from a
     * point to an epipolar line in pixels, beyond which the point is considered
     * an outlier and is not used for computing the final fundamental matrix
     * (maximum inlier distance) [OpenCV doc].
     */
    double ransac_max_distance_threshold_2D;

    /*!
     * \brief confidence_probability is used for the RANSAC or LMedS methods
     * only and it specifies a desirable level of confidence (probability) that
     * the estimated matrix is correct [OpenCV doc].
     */
    double confidence_probability;

    /*!
     * \brief Method used by OpenCV 'findFundamentalMat' method, and currently,
     * the supported methods are:
     *  - <B>7POINT</B>
     *  - <B>8POINT</B>
     *  - <B>RANSAC</B>
     *  - <B>LMEDS</B>.
     */
    std::string fundamental_matrix_2D_method;

    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   2 D / 3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief correspondence_base indicates which method is being used to remove
     * the outliers from 2D matches.
     * \note Currently, the followings are supported:
     * - <B>homography</B>
     * - <B>fundamental_matrix</B>
     * - <B>essential_matrix</B>
     * - <B>affine</B>
     * - <B>perspective</B>
     * - <B>rigid_transform</B>.
     * - <B>rigid_transform_direct</B> independent of the found keypoints.
     *
     * \sa \ref RegistrationPipeline::estimate_correspondences method.
     */
    std::string correspondence_base;

    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief registration_distance_threshold is the threshold for the correspondence
     * distance used in the \ref RegistrationPipeline::determine_correspondences method.
     */
    float registration_distance_threshold;

    //--------------------------------------------------------------------------
    //      C o r r e s p o n d e n c e s   R e j e c t i o n
    //      ( i n l i e r s    3 D )
    //--------------------------------------------------------------------------

    /*!
     * \brief correspondences_rejection_distant_threshold is the threshold used
     * in the outlier rejection method of
     * \ref RegistrationPipeline::reject_correspondences_distance.
     */
    double correspondences_rejection_distant_threshold;



    /*!
     * \brief correspondences_rejection_median_factor used as a factor in PCL
     * implementaions such that "Points with distance greater than median times
     * "factor" will be rejected", used in the outlier rejection method of
     * \ref RegistrationPipeline::reject_correspondences_median_distance.
     */
    double correspondences_rejection_median_factor;


    /*!
     * \brief correspondences_rejection_similarity_threshold is used as a setting
     * value in the \ref RegistrationPipeline::reject_correspondences_poly method
     * , (due to PCL documentation, it should be in range of [0, 1]).
     * \sa correspondences_rejection_polygon_cardinality and
     * correspondences_rejection_poly_iterartion.
     */
    float correspondences_rejection_similarity_threshold;

    /*!
     * \brief correspondences_rejection_polygon_cardinality is used as a setting
     * value in the \ref RegistrationPipeline::reject_correspondences_poly method.
     * \sa correspondences_rejection_similarity_threshold and
     * correspondences_rejection_poly_iterartion.
     */
    int correspondences_rejection_polygon_cardinality;

    /*!
     * \brief correspondences_rejection_poly_iterartion is used as a setting
     * value in the \ref RegistrationPipeline::reject_correspondences_poly method.
     * \sa correspondences_rejection_similarity_threshold and
     * correspondences_rejection_polygon_cardinality.
     */
    int correspondences_rejection_poly_iterartion;



    /*!
     * \brief correspondences_rejection_SAC_inlier_threshold is used as a setting value
     * in the \ref RegistrationPipeline::reject_correspondences_sample_consensus method.
     * \sa correspondences_rejection_SAC_max_iterations and
     * correspondences_rejection_SAC_flag_refine.
     */
    double correspondences_rejection_SAC_inlier_threshold;

    /*!
     * \brief correspondences_rejection_SAC_max_iterations is used as a setting value in
     * the \ref RegistrationPipeline::reject_correspondences_sample_consensus method.
     * \sa correspondences_rejection_SAC_inlier_threshold and
     * correspondences_rejection_SAC_flag_refine.
     */
    int correspondences_rejection_SAC_max_iterations;

    /*!
     * \brief correspondences_rejection_SAC_flag_refine is used as a setting value in
     * the \ref RegistrationPipeline::reject_correspondences_sample_consensus method.
     * \sa correspondences_rejection_SAC_max_iterations and
     * correspondences_rejection_SAC_inlier_threshold.
     */
    bool correspondences_rejection_SAC_flag_refine;



    /*!
     * \brief correspondences_rejection_overlap_ratio is used as a setting value in
     * the \ref RegistrationPipeline::reject_correspondences_trimmed method (in
     * range of [0, 1]).
     * \sa correspondences_rejection_minimum_correspondence.
     */
    float correspondences_rejection_overlap_ratio;

    /*!
     * \brief correspondences_rejection_min_correspondences is used as a setting value in
     * the \ref RegistrationPipeline::reject_correspondences_trimmed method.
     * \sa correspondences_rejection_overlap_ratio.
     */
    unsigned int correspondences_rejection_min_correspondences;



    /*!
     * \brief correspondences_rejection_min_ratio is used as a setting value in
     * the \ref RegistrationPipeline::reject_correspondences_via_trimmed method
     * (Due to PCL docs, it is the minimum overlap ratio between the input and
     * target clouds, in range of [0, 1]).
     * \sa correspondences_rejection_max_ratio.
     */
    double correspondences_rejection_min_ratio;

    /*!
     * \brief correspondences_rejection_min_ratio is used as a setting value in
     * the \ref RegistrationPipeline::reject_correspondences_via_trimmed method
     * (Due to PCL docs, it is the maximum overlap ratio between the input and
     * target clouds, in range of [0, 1]).
     * \sa correspondences_rejection_min_ratio.
     */
    double correspondences_rejection_max_ratio;


    //--------------------------------------------------------------------------
    //     T r a n s f o r m a t i o n   E s t i m a t i o n   3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Method used to initialize the \ref
     * RegistrationPipeline::transformation_estimator_3D, and currently, the
     * supported methods are:
     *  - <B>DualQuaternion</B>
     *  - <B>SVD</B>
     *  - <B>SVDscale</B>
     *  - <B>LM</B> (Levenberg Marquardt).
     */
    std::string transformation_estimation_3D_method;

    /*!
     * \brief transformation_estimation_3D_correspondences_to_use determines
     * which correspondes (-rejection) to be used by the
     * \ref RegistrationPipeline::estimate_transformation_3D method in order to
     * register the source and target clouds, and currently, the followings
     * are supported:
     *  - <B>no_rejection</B>: correspondences determined via
     * \ref RegistrationPipeline::estimate_correspondences method.
     *  - <B>general</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::determine_correspondences method.
     *  - <B>reciprocal</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::determine_correspondences method.
     *  - <B>distance</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_distance method.
     *  - <B>features</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_features method.
     *  - <B>median_distance</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_median_distance method.
     *  - <B>one_to_one</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_one_to_one method.
     *  - <B>organized_boundary</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_organized_boundary method.
     *  - <B>poly</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_poly method.
     *  - <B>sample_consensus</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_sample_consensus method.
     *  - <B>sample_consensus_direct</B>: rigid transformation obtained from
     * RegistrationPipeline::get_sample_consensus_best_transformation method.
     *  - <B>surface_normal</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_surface_normal method.
     *  - <B>trimmed</B>: correspondences (-rejection) determined via \ref
     * RegistrationPipeline::reject_correspondences_trimmed method.
     *  - <B>var_trimmed</B>: correspondences (-rejection) determined via
     * \ref RegistrationPipeline::reject_correspondences_var_trimmed method.
     * \sa RegistrationPipeline::estimate_transformation_3D.
     */
    std::string transformation_estimation_3D_correspondences_to_use;


    //--------------------------------------------------------------------------
    //     I C P   3 D
    //--------------------------------------------------------------------------

    /*!
     * \brief Method used to initialize the \ref icp_ptr, and currently, the
     * supported methods are:
     *  - <B>ICP</B>
     *  - <B>GICP</B>
     *  - <B>ICP_NL</B>.
     */
    std::string icp_type;

    //--------------------------------------------------------------------------
    //     L o o p - C l o s u r e
    //--------------------------------------------------------------------------

    /*!
     * \brief A flag to indicate whether perform the loop-closure or not.
     * \note There is no information about how to accomplish the loop-closure
     * associated with this flag.
     */
    bool flag_loop_closure;

    /*!
     * \brief Determines which method exploited to perform loop clousre.
     *
     * \todo 'OpenFABMAP' and 'RTABmap' are under construction.
     *
     * Currently, the supported methods are\n
     *  - <B>SimpleBackToBack</B> : assumes that the sensor has already made a loop, so performs an alignment on the first and last images.\n
     *  - <B>OpenFABMAP</B> : Fast Appearance-based Mapping, open source implementation available via <a href="http://www.swarthmore.edu/NatSci/mzucker1/opencv-2.4.10-docs/modules/contrib/doc/openfabmap.html">OpenCV</a>.\n
     *  - <B>RTABmap</B> : Real-Time Appearance-Based Mapping <a href="http://introlab.github.io/rtabmap/">here</a>.
     */
    std::string
    loop_closure_type;

};

// *****************************************************************************
// *****************************************************************************

} // end of namespace registration
} // end of namespace hjh

#include "Registration/RegistrationPipeline_config_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
