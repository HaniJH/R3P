
/*!
 * \brief This file contains all the standalone functions used in registration
 * of Boxes to Pyramids.
 * \author  Hani Javan-Hemmat.
 * \date    12 Aug 2015.
 */
#ifndef _BOX_PYRAMID_REGISTRATION_20150812_HPP_
#define _BOX_PYRAMID_REGISTRATION_20150812_HPP_

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

//#include <boost/shared_ptr.hpp>

//#include <opencv2/opencv.hpp>

//#include "boost_serialization.hpp"

// null-ptr
#include "Utilities/NullPointer.hpp"

#include "Registration/BoxPyramidPair.hpp"
#include "Registration/RegistrationPipeline.hpp"

#include "BoxFusion/RichBox.hpp"
#include "BoxFusion/BoxChain.hpp"

#include "ImgVis/ImgVis.hpp"

// general logger
#include "Logger/Logger.hpp"
using namespace hjh::log;

#include "PointCloudToImage/PointCloudToPyramidLog.hpp"
#include "PointCloudToImage/SquarePyramidPack.hpp"
#include "PointCloudToImage/PointCloudParsingConfig.hpp"

#include "string"


namespace hjh {
namespace registration {

// *****************************************************************************
// *********************     E N U E R A T I O N S   ***************************
// *****************************************************************************

/*!
 * \brief The PYRAMID_COMPONENT enum defines various components of a pyramid.
 * Could be used as a single flag or combination of flags.
 */
enum PYRAMID_COMPONENT : uint8_t
{
    PYRAMID_NO_COMPONENT = 0x00         //!< No component.
    , PYRAMID_COLOR_IMAGE = 0x01        //!< The color (RGB) image of a pyramid.
    , PYRAMID_DEPTH_IMAGE = 0x02        //!< The depth (D) image of a pyramid.
    , PYRAMID_ORIGINAL_CLOUD = 0x04     //!< The original point cloud of a pyramid.
    , PYRAMID_VOXELIZED_CLOUD = 0x08    //!< The voxelized point cloud of a pyramid.
    , PYRAMID_RGBD = 0x03   // 0x01 | 0x02
    //!< The color and depth images and also the original cloud of a pyramid.
    , PYRAMID_RGBDC = 0x07  // 0x01 | 0x02 | 0x04
    //!< The color and depth images and also the original cloud of a pyramid.
    , PYRAMID_RGBDV = 0x0B  // 0x01 | 0x02 | 0x08
    //!< The color and depth images and also the voxelized cloud of a pyramid.
    , PYRAMID_RGBDCV = 0x0F // 0x01 | 0x02 | 0x04 | 0x08
    //!< The color and depth images and the original and voxelized clouds of a pyramid.
};

//==============================================================================
//==============================================================================


// *****************************************************************************
// *********************   D E C L A R A T I O N S   ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

/*!
 * \brief load_pyramid_vector loads target pyramids from disk.
 * \param[in]   folder_name the folder containing the pyramids data.
 * \param[in]   logs        the vector of pyramid data as a log vector.
 * \param[out]  pyramids    the resulting pyramids as a std::vector.
 * \param[in]   flag        to indicate which components of pyramids be loaded.
 * \param[in]   H_FOV_start where to start to load pyramids (horizontal degree).
 * \param[in]   H_FOV_end   where to stop to load pyramids (horizontal degree).
 * \param[in]   V_FOV_start where to start to load pyramids (vertical degree).
 * \param[in]   V_FOV_end   where to stop to load pyramids (vertical degree).
 * \param[in]   external_logger_ptr to log the status of the function progress.
 */
void
load_pyramid_vector( const std::string &folder_name
                 , const std::vector<hjh::pcd2img::PointCloudToPyramidLog> &logs
                 , std::vector<hjh::pcd2img::SquarePyramidPack> &pyramids
                 , PYRAMID_COMPONENT flag = PYRAMID_RGBDC
                 , double H_FOV_start = 0
                 , double H_FOV_end = 360
                 , double V_FOV_start = -90
                 , double V_FOV_end = 90
                 , const hjh::log::Logger::Ptr &external_logger_ptr
                     = hjh::utility::null_ptr()
                 );

//==============================================================================
//==============================================================================

/*!
 * \brief register_box_to_appropriate_pyramid attempts to find the Rt matrix to
 * register a box to the best suited pyramid among a vector of pyramids.
 *
 * \note The result(s) of this function is being used in the
 * \ref addjust_box_to_model_based_on_pyramid function to adjust a box to its
 * appropriate pyramid.
 *
 * \todo Hint...
 *
 * \param[in]   box             The input box.
 * \param[in]   pyramids_vec    The available pyramids.
 * \param[in]   regpipeline_ptr The smart pointer to the registration pipeline.
 * \param[in]   box_counter_    The number of the current box.
 * \param[in]   number_of_boxes_ The total number of boxes.
 * \param[in]   loop_start_     The starting box of the loop (for logging purpose).
 * \param[in]   hint_horizontal_FOV_degree  A hint indicating the horizontal pose
 * of the box inside laser scanner framework in degrees (check the \ref
 * flag_valid_FOV_hint to know about the validity of this hint).
 * \param[in]   hint_vertical_FOV_degree    A hint indicating the vertical pose
 * of the box inside laser scanner framework in degrees (check the \ref
 * flag_valid_FOV_hint to know about the validity of this hint).
 * \param[in]   hint_horizontal_search_radius   This feature determines the radius
 * to search around a horizontal hint (not including inside the radius area).
 * \param[in]   hint_vertical_search_radius This feature determines the radius
 * to search around a vertical hint (not including inside the radius area).
 * \param[in]   cfg     Contains the required data for hint information.
 * \param[in]   flag_valid_FOV_hint A flag which determines if the FOV hints (\ref
 * hint_horizontal_FOV_degree and \ref hint_vertical_FOV_degree) are valid or not.
 * \param[in]   vis_ptr         The smart pointer to display images (should be 2x2).
 * \param[in]   delay_ms        The delay in ms between frames (no delay by default).
 * \param[in]   external_logger_ptr A pointer to an external logger.
 *
 * \return The information of box-to-pyramid registration as a \ref BoxPyramidPair
 * object.
 *
 * \sa convert_FOV_hint_to_FOV_specific.
 */

// * \param[in]   hint_pose       A hint indicating the possible pose of the box
// * in the laser scanner framework (check the 'validity' flag of hint_pose to know
// * whether it is a valid hint or not).
//        , const hjh::Pose &hint_pose = hjh::Pose( "hint", false )


hjh::registration::BoxPyramidPair
register_box_to_appropriate_pyramid( const boxfusion::RichBox &box
    , const std::vector<hjh::pcd2img::SquarePyramidPack> &pyramids_vec
    , const hjh::registration::RegistrationPipeline::Ptr &regpipeline_ptr
    , const int box_counter_
    , const int number_of_boxes_
    , const int loop_start_ = 0
    , const double hint_horizontal_FOV_degree = 0
    , const double hint_vertical_FOV_degree = 0
    , const bool flag_valid_FOV_hint = false
    , const uint16_t hint_horizontal_search_radius = 0
    , const uint16_t hint_vertical_search_radius = 0
    , const bool flag_always_check_zero_radius = false
    , const hjh::pcd2img::PointCloudParsingConfig::Ptr cfg = hjh::utility::null_ptr()
    , const hjh::vis::ImgVis::Ptr vis_ptr = hjh::utility::null_ptr()
    , const unsigned int delay_ms = 0
    , const Logger::Ptr &external_logger_ptr = hjh::utility::null_ptr() );

//==============================================================================
//==============================================================================

/*!
 * \brief addjust_box_to_model_based_on_pyramid adjusts a box to the FARO model
 * based on the corresponding pyramid.
 *
 * \note This function adjusts a box to its corresponding pyramid based on the
 * data generated by the \ref register_box_to_appropriate_pyramid function.
 *
 * this function performs the following transformations for a given box:
 *  -# inverse of box-pose,
 *  -# box: back-to-origin,
 *  -# box-to-pyramid,
 *  -# from-Kinect-to-FARO,
 *  -# inverse of pyramid-pose.
 *
 * \param[in]   box_number_         Box number in the Box-Chain (\ref box_chain_).
 * \param[in]   pyramid_number_     Pyramid number in the pyramid vector (\ref pyramid_log_vector_).
 * \param[in]   b2p_data_           Box-to-pyramid registration data.
 * \param[in]   box_chain_          The box-chain pointer.
 * \param[in]   pyramid_log_vector_ The vector containing pyramids data.
 * \param[out]  pose_               The output transformation of the input box,
 * \param[in]   external_logger_ptr A pointer to an external logger.
 * which adjusts the box into the model.
 * \return zero if everything goes well, a negative number otherwise.
 */
int
addjust_box_to_model_based_on_pyramid(
        int box_number_
        , int pyramid_number_
        , const hjh::registration::BoxPyramidPair &b2p_data_
        , const hjh::boxfusion::BoxChain::Ptr &box_chain_
        , const std::vector<hjh::pcd2img::PointCloudToPyramidLog> &pyramid_log_vector_
        , hjh::Pose &pose_
        , const Logger::Ptr &external_logger_ptr = hjh::utility::null_ptr()
        );

//==============================================================================
//==============================================================================

// BADAN ToDo
bool
extract_FOV_hint_from_box_pose( const hjh::Pose &box_pose_
                      , double &horizontal_FOV_hint
                      , double &vertical_FOV_hint );


//==============================================================================
//==============================================================================

/*!
 * \brief convert_FOV_hint_to_FOV_specific function converts a FOV hint to specific
 * FOV degrees which match the hint the best (horizontally and vertically).
 *
 * Based on the specific FOV, it is possible to match a hint to an excat pyramid
 * using the \ref register_box_to_appropriate_pyramid function.
 *
 * \note Used as a helper function in the \ref register_box_to_appropriate_pyramid
 * function.
 *
 * \param[in]   hint_horizontal_FOV_degree  Hint for horizontal FOV (in degrees).
 * \param[in]   hint_vertical_FOV_degree    Hint for vertical FOV (in degrees).
 * \param[out]  specific_horizontal_FOV_degree  The target pyramid (horizontally).
 * \param[out]  specific_vertical_FOV_degree    The target pyramid (vertically).
 * \param[in]   horizontal_FOV_degree   Horizontal FOV of pyramids (in degrees).
 * \param[in]   vertical_FOV_degree     Vertical FOV of pyramids (in degrees).
 * \param[in]   horizontal_overlap      Horizontal overlap of pyramids as percentage (should be in [0, 1]).
 * \param[in]   vertical_overlap        Vertical overlap of pyramids as percentage (should be in [0, 1]).
 * \param[in]   max_horizontal_FOV      Maximum horizontal FOV of pyramids (in degrees).
 * \param[in]   min_horizontal_FOV      Minimum horizontal FOV of pyramids (in degrees).
 * \param[in]   max_vertical_FOV        Maximum vertical FOV of pyramids (in degrees).
 * \param[in]   min_vertical_FOV        Minimum vertical FOV of pyramids (in degrees).
 *
 */
void
convert_FOV_hint_to_FOV_specific( double hint_horizontal_FOV_degree
                                  , double hint_vertical_FOV_degree
                                  , double &specific_horizontal_FOV_degree // output
                                  , double &specific_vertical_FOV_degree // output
                                  , double horizontal_FOV_degree = 58
                                  , double vertical_FOV_degree = 45
                                  , double horizontal_overlap = 0.8
                                  , double vertical_overlap = 0.8
                                  , double max_horizontal_FOV = 360
                                  , double min_horizontal_FOV = 0
                                  , double max_vertical_FOV = 90
                                  , double min_vertical_FOV = -90
                                  );
/*!
 * \brief convert_FOV_hint_to_FOV_specific function converts a FOV hint to specific
 * FOV degrees which match the hint the best (horizontally and vertically).
 *
 * \note This is an alias for the \ref convert_FOV_hint_to_FOV_specific function.
 *
 * Based on the specific FOV, it is possible to match a hint to an excat pyramid
 * using the \ref register_box_to_appropriate_pyramid function.
 *
 * \note Used as a helper function in the \ref register_box_to_appropriate_pyramid
 * function.
 *
 * \param[in]   hint_horizontal_FOV_degree  Hint for horizontal FOV (in degrees).
 * \param[in]   hint_vertical_FOV_degree    Hint for vertical FOV (in degrees).
 * \param[out]  specific_horizontal_FOV_degree  The target pyramid (horizontally).
 * \param[out]  specific_vertical_FOV_degree    The target pyramid (vertically).
 * \param[in]   cfg                         The %configuration data.
 */
void
convert_FOV_hint_to_FOV_specific( double hint_horizontal_FOV_degree
                              , double hint_vertical_FOV_degree
                              , double &specific_horizontal_FOV_degree // output
                              , double &specific_vertical_FOV_degree // output
                              , const hjh::pcd2img::PointCloudParsingConfig &cfg
                              );

//==============================================================================
//==============================================================================


// *****************************************************************************
// *****************************************************************************

} // end of namespace registration
} // end of namespace hjh

#include "Registration/BoxPyramidRegistration_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
